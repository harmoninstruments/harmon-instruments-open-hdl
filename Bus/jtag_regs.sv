// provides an interface to registers from JTAG
// bit 0: start (1)
// bit 1: rden
// bits 31:2 address // address, write command ends here, data starts
// bits 95:32 first word of write data
// or
// bits 39:32 read word count - 1 (0 is 1 word, 255 is 256 words)
// bits 47:40 read filler
// bit 48: start of read data on TDO

module jtag_regs (
   // connection to I/O
   input shift, // USERn instruction in IR, in shift-dr
   input tck,
   input tdi,
   // connection to bus
   output logic tdo = 0,
   output logic valid = 0, // high 1 cycle for each read/write
   output logic [29:0] address, // word address
   output logic rden, // 1: read 0: write
   output logic [63:0] wdata, // write data valid with rden = 0
   input [63:0] rdata, // set this to read value within 16 TCK cycles of valid
   input [31:0] initial_read // value to transmit during address phase
   );

   enum logic [1:0] {WAIT_START = 0, WAIT_READ = 1, READING = 2, WRITING = 3} state = WAIT_START;

   reg [63:0] idr, odr;
   reg [5:0] bitcount = 0;
   reg [7:0] wordcount = 0;

   always_ff @ (posedge tck) begin
      valid <= shift & (
         ((state == WAIT_START) & idr[32] & idr[33]) | // as soon as we know we have a read
         ((state == READING) & (wordcount != 0) & (bitcount == 32)) | // read continues
         ((state == WRITING) & (bitcount == 63))); // write

      if((state == WRITING) & (bitcount == 63))
         wdata <= idr;

      if(~shift)
         wordcount <= 1'b0;
      else if((state == WAIT_READ) & (bitcount == 15))
         wordcount <= idr[55:48];
      else if((state == READING) & valid & (wordcount != 0))
         wordcount <= wordcount - 1'b1;

      idr <= shift ? {tdi, idr[63:1]} : 64'h0;

      if(shift) // in shift-dr, IR=select BSCAN 
         begin
            if(((state == READING) | (state == WAIT_READ) )& (bitcount == 13))
               odr <= rdata;
            else
               odr <= odr >> 1;
            bitcount <= (state == WAIT_START) ? 1'b0: bitcount + 1'b1;
            if(state == WAIT_START) begin
               if(idr[32]) 
                  begin // we have the command
                     address <= idr[63:34];
                     rden <= idr[33];
                  end
            end else begin
               address <= address + valid;
            end
            case(state)
               WAIT_START: state <= idr[32] ? (idr[33] ? WAIT_READ : WRITING) : WAIT_START;
               WAIT_READ: state <= bitcount == 16 ? READING : WAIT_READ;
               default: state <= state;
            endcase
         end
      else // reset, capture, update, etc 
         begin
            odr <= initial_read;
            state <= WAIT_START;
            bitcount <= 1'b0;
         end
   end

   always_ff @ (negedge tck)
      tdo <= odr[0];

endmodule
