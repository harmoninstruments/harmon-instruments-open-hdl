# Test bench for gf_mul
# Copyright (C) 2023 Darrell Harmon
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

import cocotb
import cocotb.runner
from cocotb.clock import Clock
from cocotb.triggers import Timer, RisingEdge
from pathlib import Path

async def regs_impl(dut):
    reg = 0
    while True:
        await RisingEdge(dut.tck)
        valid = dut.valid.value.integer
        if valid == 0:
            continue
        rden = dut.rden.value.integer
        address = dut.address.value.integer
        if rden == 1:
            rdata = address | (0xffc0ffee << 32)
            if address == 1:
                rdata = reg

            print(f"read at {address:08x}: {rdata:016x}")
            dut.rdata.value = rdata
        else:
            wdata = dut.wdata.value.integer
            print(f"write at {address:08x}: {wdata:016x}")
            if address == 1:
                reg = wdata

async def do_write(dut, addr, data):
    dut.shift.value = 0
    dut.tdi.value = 0
    for i in range(3):
        await RisingEdge(dut.tck)
    dut.shift.value = 1
    d = 1 | (addr << 2)
    for i in range(32):
        dut.tdi.value = (d & 1)
        d = d >> 1
        await RisingEdge(dut.tck)
    for d in data:
        for i in range(64):
            dut.tdi.value = (d & 1)
            d = d >> 1
            await RisingEdge(dut.tck)
    await RisingEdge(dut.tck)

    dut.shift.value = 0
    dut.tdi.value = 0
    for i in range(3):
        await RisingEdge(dut.tck)

async def do_read(dut, addr, count):
    dut.shift.value = 0
    dut.tdi.value = 0
    for i in range(3):
        await RisingEdge(dut.tck)
    dut.shift.value = 1
    d = 3 | (addr << 2) | ((count-1)<<32)
    for i in range(48):
        dut.tdi.value = (d & 1)
        d = d >> 1
        await RisingEdge(dut.tck)
    rv = []
    for i in range(count):
        d = 0
        for j in range(64):
            d = d >> 1
            d |= (dut.tdo.value.integer << 63)
            await RisingEdge(dut.tck)
        print(f"read data {addr:08x} = {d:016x}")
        addr += 1
        rv.append(d)
    dut.shift.value = 0
    dut.tdi.value = 0
    for i in range(3):
        await RisingEdge(dut.tck)
    return rv
    
@cocotb.test()
async def run_test(dut):
    dut.shift.value = 0
    dut.tdi.value = 0
    dut.rdata.value = 0
    dut.initial_read.value = 0xcafed00d
    await Timer(1)
    cocotb.start_soon(Clock(dut.tck, 20000).start())
    cocotb.start_soon(regs_impl(dut))
    await do_write(dut, 1, [0xcafe, 0xd00d, 0x12345678ABCDEF00])
    await do_write(dut, 7, [0xdeaddeaddeadd00d])
    r = await do_read(dut, 0, 3)
    print(r)
    assert r == [0xffc0ffee00000000, 0xcafe, 0xffc0ffee00000002]
    r = await do_read(dut, 7, 1)
    print(r)
    assert r == [0xffc0ffee00000007]




def test_jtag_regs():
    sim = "icarus"
    hdl_toplevel = "jtag_regs"
    proj_path = Path(__file__).resolve().parent
    verilog_sources = [
        proj_path/"jtag_regs.sv",
        proj_path/"../dump.sv",

    ]
    runner = cocotb.runner.get_runner(sim)
    runner.build(
        verilog_sources=verilog_sources,
	    hdl_toplevel=hdl_toplevel,
        #parameters={"M":m, "POLY":poly},
        always=True,
    )
    runner.test(hdl_toplevel=hdl_toplevel, test_module="jtag_regs_test")

if __name__ == "__main__":
    test_jtag_regs()