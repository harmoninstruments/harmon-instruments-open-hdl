// bit 0: start (1)
// bit 1: rden
// bits 31:2 address // address, write command ends here, data starts
// bits 39:32 read word count - 1 (0 is 1 word, 255 is 256 words)
// bits 47:40 read filler

module jtag_bscane2 #(parameter JTAG_CHAIN=3) (
   output tck,
   output valid, // high 1 cycle for each read/write
   output [29:0] address, // word address
   output rden, // 1: read 0: write
   output [63:0] wdata, // write data valid with rden = 0
   input [63:0] rdata, // set this to read value within 16 TCK cycles of valid
   input [31:0] initial_read // value to transmit during address phase
   );

   wire shift, sel, tdi, tdo;
   jtag_regs jtag_regs(.tck, .shift(shift & sel), .tdi, .tdo, 
      .valid, .address, .rden, .wdata, .rdata, .initial_read);

   wire delay_q;

   BSCANE2 #(.JTAG_CHAIN(JTAG_CHAIN)) BSCANE2_3 (
      .CAPTURE(),
      .DRCK(), // gated TCK
      .RESET(),
      .RUNTEST(), // in RTI
      .SEL(sel), // USER<N> instruction is in IR
      .SHIFT(shift), // in shift-dr
      .TCK(tck),
      .TDI(tdi),
      .TMS(),
      .UPDATE(),
      .TDO(tdo) // has FF on negedge before pin
   );

endmodule
