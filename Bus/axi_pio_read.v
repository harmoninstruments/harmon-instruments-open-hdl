// Copyright 2014 - 2019 Harmon Instruments, LLC
`timescale 1 ps / 1 ps

module axi_pio_read
  (input c,
   // local bus
   input [31:0] 	  local_rdata,
   input 		  local_rready, // assert 1 clock when local_rdata valid
   output reg [ABITS-1:0] local_raddr,
   output reg 		  local_read = 0, // asserts 1 clock with local_raddr
   // AXI
   input [ABITS-1:0] 	  araddr,
   input [3:0] 		  arlen, // transfers minus 1
   input [11:0] 	  arid,
   input 		  arvalid,
   output 		  arready,
   input 		  rready,
   output reg [11:0] 	  rid = 0,
   output reg [31:0] 	  rdata,
   output reg 		  rlast = 0,
   output reg 		  rvalid = 0
   );

   parameter ABITS=20;

   reg [4:0] 	     state = 0;

   assign arready = (state == 0);

   always @ (posedge c) begin
      case(state)
	0: begin
	   if(arvalid) begin
              rid <= arid;
	      local_raddr <= araddr[ABITS-1:0];
	      state <= 5'd31 - arlen;
	      local_read <= 1'b1;
	   end
	end
	default: begin
           local_raddr <= local_raddr + (rready && rvalid);
	   local_read <= rready && rvalid && (state != 31);
	   state <= state + (rready && rvalid);
	   rlast <= state == 31;
	   if(local_rready)
             begin
                rdata <= local_rdata;
                rvalid <= 1'b1;
             end
	   else if(rready && rvalid)
	     rvalid <= 1'b0;
        end
      endcase
   end

`ifdef SIM
   initial
     begin
        $dumpfile("dump.vcd");
        $dumpvars(0);
     end
`endif

endmodule
