// Copyright 2014 - 2019 Harmon Instruments, LLC
`timescale 1 ps / 1 ps

module axi_pio_write
  (input c,
   // local bus
   output [31:0]          local_wdata,
   output reg [ABITS-1:0] local_waddr = 0,
   output                 local_wvalid,
   // AXI
   input [ABITS-1:0]      awaddr,
   input [11:0]           awid,
   output reg             awready = 1,
   input                  awvalid,
   output reg [11:0]      bid,
   output reg             bvalid = 0,
   input                  bready,
   output reg             wready = 0,
   input                  wvalid,
   input                  wlast,
   input [31:0]           wdata);

   parameter ABITS = 20;

   reg [4:0]              state = 0;

   assign local_wdata = wdata;
   assign local_wvalid = (wvalid && wready);

   always @ (posedge c) begin
      // write response
      if(awvalid && awready)
	bid <= awid;
      if(wvalid && wready && wlast)
	bvalid <= 1'b1;
      else if(bready)
	bvalid <= 1'b0;

      // ready for another address after the write response is accepted
      if(awvalid && awready)
	awready <= 1'b0;
      else if(bready && bvalid)
	awready <= 1'b1;

      // ready for write data when we get an address, until last is asserted
      if(awvalid && awready)
	wready <= 1'b1;
      else if(wvalid && wready && wlast)
	wready <= 1'b0;

      if(awvalid && awready)
	local_waddr <= awaddr;
      else if(wvalid && wready)
        local_waddr <= local_waddr + 1'b1;
   end

`ifdef SIM
   initial
     begin
        $dumpfile("dump.vcd");
        $dumpvars(0);
     end
`endif

endmodule
