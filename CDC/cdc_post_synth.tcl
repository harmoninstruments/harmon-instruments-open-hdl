foreach cell [get_cells -quiet -hier -filter {hi_max_delay != ""}] {
    set clock [get_clocks -of_objects \
        [all_fanin -flat -startpoints_only [get_pin $cell/D]]]
    if {[llength $clock] != 0} {
        set_max_delay -datapath_only -from $clock \
            -to [get_cells $cell] [get_property hi_max_delay $cell]
    }
}