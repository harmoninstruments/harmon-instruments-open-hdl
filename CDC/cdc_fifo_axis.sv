// a 5 bit gray up counter
module gray_counter_5 (input clock, inc, output bit [4:0] o);
    always_ff @( posedge clock) begin
        if(inc) begin
            case(o)
                5'b00000: o <= 5'b00001;
                5'b00001: o <= 5'b00011;
                5'b00011: o <= 5'b00010;
                5'b00010: o <= 5'b00110;
                5'b00110: o <= 5'b00111;
                5'b00111: o <= 5'b00101;
                5'b00101: o <= 5'b00100;
                5'b00100: o <= 5'b01100;
                5'b01100: o <= 5'b01101;
                5'b01101: o <= 5'b01111;
                5'b01111: o <= 5'b01110;
                5'b01110: o <= 5'b01010;
                5'b01010: o <= 5'b01011;
                5'b01011: o <= 5'b01001;
                5'b01001: o <= 5'b01000;
                5'b01000: o <= 5'b11000;
                5'b11000: o <= 5'b11001;
                5'b11001: o <= 5'b11011;
                5'b11011: o <= 5'b11010;
                5'b11010: o <= 5'b11110;
                5'b11110: o <= 5'b11111;
                5'b11111: o <= 5'b11101;
                5'b11101: o <= 5'b11100;
                5'b11100: o <= 5'b10100;
                5'b10100: o <= 5'b10101;
                5'b10101: o <= 5'b10111;
                5'b10111: o <= 5'b10110;
                5'b10110: o <= 5'b10010;
                5'b10010: o <= 5'b10011;
                5'b10011: o <= 5'b10001;
                5'b10001: o <= 5'b10000;
                5'b10000: o <= 5'b00000;
            endcase 
        end
    end
endmodule

// AXI stream CDC FIFO
// uses distributed RAM 
// capacity is at least 24 words
// N bits wide
module cdc_fifo_axis #(parameter N = 32) (
        input iclock,
        input [N-1:0] idata,
        input ivalid,
        output bit iready = 0,
        output ialmostfull, // if not asserted, can accept at least 3 more words
        input oclock, 
        output reg [N-1:0] odata, // AXI stream out: oclock domain
        output bit ovalid = 0, 
        input oready
        );

        // LUTs for gray code comparison
        // bits [5:3] are optr
        // bits [2:0] are iptr in these LUTs
        // see tools/gray.py for generation
        wire [63:0] lut_iready = 64'hBFFB7FDFFDF7FEEF; // iptr-optr < 7
        wire [63:0] lut_almost_full = 64'h440CC0A0030A1130; // iptr-optr > 5

        wire [4:0] iptr, optr; // gray coded input, output pointers
        wire [4:0] iptr_o; // in the opposite clock domains
        wire [2:0] optr_i; // 3 MSBs
        wire ram_read = (oready | (~ovalid)) & (optr != iptr_o);

        gray_counter_5 ptr_in  (.clock(iclock), .inc(ivalid & iready), .o(iptr));
        gray_counter_5 ptr_out (.clock(oclock), .inc(ram_read), .o(optr));
        cdc_maxdelay #(.N(5)) sync_io (.clock(oclock), .i(iptr), .o(iptr_o));
        cdc_maxdelay #(.N(3)) sync_oi (.clock(iclock), .i(optr[4:2]), .o(optr_i));

        assign ialmostfull = lut_almost_full[{optr_i,iptr[4:2]}];

        reg [31:0] ram [N-1:0];

        always @ (posedge iclock) begin
            if(iready & ivalid)
                ram[iptr] <= idata;
            iready <= lut_iready[{optr_i,iptr[4:2]}];
        end

        always @ (posedge oclock) begin
            if (ram_read)
                odata <= ram[optr];
            ovalid <= (ovalid & ~oready) | ram_read;
        end
endmodule