# emit elements of a case statement as used in async_fifo_24_axis
def gray5(x):
    return f"5'b{x ^ (x>>1):05b}"

for i in range(32):
    next = (i + 1) & 0x1F
    print(f"    {gray5(i)}: o <= {gray5(next)};")

def gray(x):
    return x ^ (x>>1)

lut_iready = 0
lut_almost_full = 0

for iptr in range(8):
    for optr in range(8):
        fill = (iptr - optr) & 7
        idx = gray(iptr) | (gray(optr) << 3)
        bit = 0
        if fill < 7:
            bit = 1
            lut_iready |= (1 << idx)
        if fill > 5:
            bit = 1
            lut_almost_full |= (1 << idx)
        print(iptr, optr, fill, idx, bit)

print(f"wire [63:0] lut_iready = 64'h{lut_iready:016X};")
print(f"wire [63:0] lut_almost_full = 64'h{lut_almost_full:016X};")

