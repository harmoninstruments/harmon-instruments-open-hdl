
module binary_to_gray #(parameter N = 5) (input [N-1:0] i, output [N-1:0] o);
    assign o = i ^ (i >> 1);
endmodule

module gray_to_binary #(parameter N = 5) (input [N-1:0] i, output [N-1:0] o);
    generate genvar j;
        for(j=0; j<N; j++) begin
            assign o[j] = ^(i >> j);
        end
    endgenerate
endmodule

// synchronize a signal from iclock to oclock
module sync_gray #(parameter N = 5) (
        input iclock, oclock, input [N-1:0] i, output [N-1:0] o);
        wire [N-1:0] i_gray;
        binary_to_gray #(.N(N)) bin2gray (.i, .o(i_gray));
        reg [N-1:0] i_gray_q = 0;
        always @ (posedge iclock)
            i_gray_q <= i_gray;
        wire [N-1:0] o_gray;
        sync_maxdelay #(.N(N)) sync_i (.clock(oclock), .i(i_gray_q), .o(o_gray));
        gray_to_binary #(.N(N)) gray2bin (.i(o_gray), .o);
endmodule