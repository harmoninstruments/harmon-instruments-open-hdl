// a 2 FF synchronizer with a false path constraint
module cdc_falsepath #(parameter N = 1, parameter RESET=0) (
    input clock, input [N-1:0] i, output [N-1:0] o);
    (* \hi_false_path = "TRUE", ASYNC_REG="TRUE" *) reg [N-1:0] r0 = RESET;
    (* ASYNC_REG="TRUE" *) reg [N-1:0] r1 = RESET;
    always @ (posedge clock)
        begin
            r0 <= i;
            r1 <= r0;
        end
    assign o = r1;
endmodule