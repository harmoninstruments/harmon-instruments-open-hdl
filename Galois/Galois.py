class Galois():
    def __init__(self, prim=0x409, m=10, g=2):
        self.prim = prim
        self.m = m
        self.g = g
        # 2**i
        self.exptable = [0]*2**self.m
        self.logtable = [0]*2**self.m
        self.exptable[0] = 1
        for i in range(1,2**self.m):
            v = self.mul(self.exptable[i-1], g)
            self.exptable[i] = v

        for i in range(2**self.m-1):
            expi = self.exptable[i]
            if expi == 0:
                print("expi", i)
            self.logtable[self.exptable[i]] = i

    # x*y
    def mul(self, x, y):
        r=0
        while y:
            if y & 1:
                r = r ^ x
            y = y >> 1
            x = x << 1
            if x & 2**self.m:
                x = x ^ self.prim
        return r

    # 2**x
    def exp(self, x):
        return self.exptable[x%(2**self.m-1)]

    # log2(x)
    def log(self, x):
        return self.logtable[x%((2**self.m)-1)]

    # x**y
    def pow(self, x, y):
        return self.exp(self.log(x)*y)

    # x / y
    def div(self, x, y):
        if y == 0:
            print("divide by 0")
            return 0
        return self.exp(self.log(x)-self.log(y))

    # 1 / x
    def inverse(self, x):
        if x == 0:
            return 0
        rv = self.exptable[(2**self.m-1)-self.logtable[x]]
        assert self.mul(rv, x) == 1
        return rv

    def generate_mul_verilog(self, fn):
        matrix = [0]*self.m
        for i in range(self.m): # matrix row, a = 2**i
            for j in range(self.m): # matrix column, b = 2**i
                matrix[i] ^= self.mul(2**i, 2**j)<<(self.m*j)
        from optimize import OptimizeMatrix
        o = OptimizeMatrix(matrix, self.m**2)
        o.usage()
        o.print()
        o.optimize(0)
        o.optimize(0)
        o.print()
        o.usage()
        o.write_verilog(fn, print_levels=False)

    def poly_eval(self, poly, x):
        xm = 1
        rv = 0
        for v in poly:
            rv ^= self.mul(v,xm)
            xm = self.mul(xm,x)
        return rv

    def poly_mul(self, x, y):
        rv = [0] * (len(x) + len(y) - 1)
        for i in range(len(x)):
            for j in range(len(y)):
                rv[i+j] ^= self.mul(x[i], y[j])
        return rv

    def generator_poly(self, nsym):
        g = [1] # generator*x + 1
        for i in range(nsym): # multiply by generator*x + 1
            g = self.poly_mul(g, [1,self.pow(self.g,i)])
            print(g)
        return g

if __name__ == "__main__":
    gf = Galois()
    gf5 = Galois(m=5, prim=0x25)
    poly = [1,0,0,0,0,0,0,1,0,0,1][::-1]
    for alpha in range(1024):
        e = gf.poly_eval(poly, alpha)
        if e != 0:
            continue
        print("root", alpha, e)
    poly5 = [1, 0, 0, 1, 0, 1]
    for alpha in range(32):
        e = gf5.poly_eval(poly5, alpha)
        if e != 0:
            continue
        print("root", alpha, e)
    print(gf5.exptable)
    #gf10 = GF(prim=0x409, m=10)
    #gf10.generate_mul_verilog("gfmul10_409_constants")
    #gf8 = GF(prim=0x11D, m=8)
    #gf8.generate_mul_verilog("gfmul8_11D_constants")
