/*
 AES 256 key schedule, increment 1 round
 Copyright (C) 2023 Darrell Harmon
 SPDX-License-Identifier: BSD-2-Clause-NetBSD

 This does 4 iterations of the key expansion loop
 producing one round key.

 */

module AES256_key_schedule
  (
   input logic [255:0]  kin, // raw key or from previous round
   input                do_rotword,
   input logic [6:0]    rcon, // Rcon[i]
   output logic [255:0] kout); // low 128 are round key, all 256 go to next round

   // RotWord with bypass for if even round, no rotation if odd   w[i-1]
   wire [31:0]          rotword_opt = do_rotword ? {kin[224+:8],kin[232 +:24]} : kin[224 +:32];
   wire [31:0]          subword;

   for(genvar i=0; i<4; i++) begin
      AES_sbox sb (.c(rotword_opt[i*8 +:8]), .s(subword[i*8 +:8]));
   end

   wire [31:0] o128 = subword ^ rcon ^ kin[0 +:32];

   assign kout[0  +:128] = kin[128 +:128];
   assign kout[128 +:128] = {4{o128}} ^
                            {{3{kin[32 +:32]}}, 32'h0} ^
                            {{2{kin[64 +:32]}}, 64'h0} ^
                            {{1{kin[96 +:32]}}, 96'h0};

endmodule
