/*
 AES256 test for FPGA stability
 Does (2**L2SIZE)/16 rounds of AES-CBC with a zero key, zero data, outputs low 64 bits of result
 Copyright (C) 2023-2024 Darrell Harmon
 SPDX-License-Identifier: BSD-2-Clause-NetBSD
 */

module AES256_stability_test #(parameter L2SIZE=10) 
   (
    input               clock,
    input               enable,
    output logic [63:0] out
   );

   wire [127:0] aes_out;
   reg reset;
   reg [L2SIZE-1:0] state = 0;
   reg start = 0;

   always_ff @ (posedge clock) begin
      start <= (state[3:0] == 0) & enable;
      reset <= (state == 0) | (~enable);
      state <= enable ? state + 1'b1 : 1'b0;
      if(state == 0)
         out <= aes_out[63:0];
   end

   AES256_iterative AES(.clock, .start, .key(256'b0), .din(reset ? 128'b0 : aes_out), 
      .ovalid(), .out(aes_out));

endmodule
