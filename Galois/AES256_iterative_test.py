# Test bench for AES256 iterative
# Copyright (C) 2023-2024 Darrell Harmon
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

import cocotb
import cocotb.runner
from cocotb.clock import Clock
from cocotb.triggers import Timer, RisingEdge
from pathlib import Path
from struct import unpack

from AES_util import tvec_key_256, tvec_din_256, tvec_out_256, py_encrypt_256, randomint

keys = [tvec_key_256]
data = [tvec_din_256]

for i in range(200):
    keys.append(randomint(256))
    data.append(randomint(128))

async def do_input(dut):
    for i in range(len(keys)):
        dut.start.value = 1
        dut.key.value = keys[i]
        dut.din.value = data[i]
        await Timer(1)
        for i in range(36 if i == 3 else 14):
            await RisingEdge(dut.clock)
            dut.start.value = 0

@cocotb.test()
async def run_test(dut):
    dut.start.value = 0
    cocotb.start_soon(Clock(dut.clock, 2000).start())
    await Timer(1)
    await RisingEdge(dut.clock)
    cocotb.start_soon(do_input(dut))
    await Timer(1)
    await RisingEdge(dut.clock)
    for i in range(len(keys)):
        while dut.ovalid.value.integer == 0:
            await Timer(1)
            await RisingEdge(dut.clock)
        result_exp = py_encrypt_256(data[i], keys[i])
        result = dut.out.value.integer
        print(f"expected 0x{result_exp:032x}")
        print(f"result   0x{result:032x}, {result==result_exp}")
        assert result==result_exp
        await Timer(1)
        await RisingEdge(dut.clock)

def test_AES256_iterative():
    sim = "icarus"
    hdl_toplevel = "AES256_iterative"
    proj_path = Path(__file__).resolve().parent
    verilog_sources = [
        proj_path/"AES256_iterative.sv",
        proj_path/"AES_sbox.sv",
        proj_path/"AES_MixColumns.sv",
        proj_path/"../dump.sv",
    ]
    runner = cocotb.runner.get_runner(sim)
    runner.build(
        verilog_sources=verilog_sources,
	hdl_toplevel=hdl_toplevel,
        always=True,
    )
    runner.test(hdl_toplevel=hdl_toplevel, test_module=hdl_toplevel + "_test")

if __name__ == "__main__":
    test_AES256_iterative()
