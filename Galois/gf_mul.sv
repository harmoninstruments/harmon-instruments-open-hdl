// Copyright (C) 2023 Darrell Harmon
// SPDX-License-Identifier: BSD-2-Clause-NetBSD
// multiplier in GF(2^M)

module gf_mul #(parameter POLY=9'h11D, parameter M=$clog2(POLY)-1)
   (
    input [M-1:0]  a, b,
    output [M-1:0] p
    );
   generate
      for(genvar i=0; i<M; i++) begin : scale
         wire [M-1:0] m; // a * 2**i
         wire [M-1:0] p_i; // product through this iteration
         wire [M-1:0] a_bi = ({M{b[i]}} & m); // a * b[i]
         if(i==0) begin
            assign m = a;
            assign p_i = a_bi;
         end else begin
            assign m = {scale[i-1].m[M-2:0],1'b0} ^
                       ({M{scale[i-1].m[M-1]}} & POLY[M-1:0]); // m = m_prev * 2
            assign p_i = scale[i-1].p_i ^ a_bi; // p_i = p_i_prev + a_bi
         end
      end
   endgenerate
   assign p = scale[M-1].p_i;
endmodule
