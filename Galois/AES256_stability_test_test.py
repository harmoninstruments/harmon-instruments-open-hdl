# Test bench for AES256 iterative
# Copyright (C) 2023-2024 Darrell Harmon
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

import cocotb
import cocotb.runner
from cocotb.clock import Clock
from cocotb.triggers import Timer, RisingEdge
from pathlib import Path
from struct import unpack

@cocotb.test()
async def run_test(dut):
    dut.enable.value = 0
    cocotb.start_soon(Clock(dut.clock, 2000).start())
    await Timer(1)
    for i in range(10):
        await RisingEdge(dut.clock)
    dut.enable.value = 1
    await Timer(1)
    for i in range(1100):
        await RisingEdge(dut.clock)
    result = dut.out.value.integer
    print(f"result = 0x{result:08x}")

def test_AES256_iterative():
    sim = "icarus"
    hdl_toplevel = "AES256_stability_test"
    proj_path = Path(__file__).resolve().parent
    verilog_sources = [
        proj_path/"AES256_stability_test.sv",
        proj_path/"AES256_iterative.sv",
        proj_path/"AES_sbox.sv",
        proj_path/"AES_MixColumns.sv",
        proj_path/"../dump.sv",
    ]
    runner = cocotb.runner.get_runner(sim)
    runner.build(
        verilog_sources=verilog_sources,
    	hdl_toplevel=hdl_toplevel,
        always=True,
    )
    runner.test(hdl_toplevel=hdl_toplevel, test_module=hdl_toplevel + "_test")

if __name__ == "__main__":
    test_AES256_iterative()
