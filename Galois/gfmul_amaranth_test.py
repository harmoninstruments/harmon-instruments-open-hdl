# Copyright 2023 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth import *
from gfmul import *
from amaranth.sim import Simulator, Settle
import Galois

def do_gfmul(m=8, poly=0x11D):
    gf = Galois.Galois(prim=poly, m=m)
    a = Signal(m)
    b = Signal(m)
    dut = GFMul(a,b,m,poly)
    sim = Simulator(dut)
    def test_proc():
        for av in range(2**m):
            for bv in range(2**m):
                yield a.eq(av)
                yield b.eq(bv)
                yield Settle()
                p = yield dut.o
                p_expected = gf.mul(av,bv)
                if p != p_expected:
                    print(f"a={av},b={bv},p={p},e={p_expected}")
                assert p == p_expected
    sim.add_process(test_proc)
    sim.run()

def test_gfmul_8():
    do_gfmul()
