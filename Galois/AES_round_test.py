# Test bench for AESRound
# Copyright (C) 2023 Darrell Harmon
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

import cocotb
import cocotb.runner
from cocotb.clock import Clock
from cocotb.triggers import Timer, RisingEdge
from pathlib import Path
from struct import unpack

def tvec(x):
    s = bytes.fromhex(x)
    (r1,r2) = unpack("QQ", s)
    return r1 | (r2 << 64)

# from FIPS-197, these have LS byte on the left, need to run though tvec()
testcases = [
    ['00102030405060708090a0b0c0d0e0f0', '5f72641557f5bc92f7be3b291db9f91a'],
    ['4f63760643e0aa85efa7213201a4e705', 'bd2a395d2b6ac438d192443e615da195'],
    ['1859fbc28a1c00a078ed8aadc42f6109', '810dce0cc9db8172b3678c1e88a1b5bd'],
    ['975c66c1cb9f3fa8a93a28df8ee10f63', 'b2822d81abe6fb275faf103a078c0033'],
    ['1c05f271a417e04ff921c5c104701554', 'aeb65ba974e0f822d73f567bdb64c877'],
    ['c357aae11b45b7b0a2c7bd28a8dc99fa', 'b951c33c02e9bd29ae25cdb1efa08cc7'],
    ['7f074143cb4e243ec10c815d8375d54c', 'ebb19e1c3ee7c9e87d7535e9ed6b9144'],
    ['d653a4696ca0bc0f5acaab5db96c5e7d', '5174c8669da98435a8b3e62ca974a5ea'],
    ['5aa858395fd28d7d05e1a38868f3b9c5', '0f77ee31d2ccadc05430a83f4ef96ac3'],
    ['4a824851c57e7e47643de50c2af3e8c9', 'bd86f0ea748fc4f4630f11c1e9331233'],
    ['c14907f6ca3b3aa070e9aa313b52b5ec', 'af8690415d6e1dd387e5fbedd5c89013'],
    ['5f9c6abfbac634aa50409fa766677653', '7427fae4d8a695269ce83d315be0392b'],
    ['516604954353950314fb86e401922521', '2c21a820306f154ab712c75eee0da04f'],
]

@cocotb.test()
async def run_test(dut):
    for case in testcases:
        dut.din.value = tvec(case[0])
        await Timer(1)
        result = dut.out.value.integer
        expected = tvec(case[1])
        print(f"output   0x{result:032x}")
        print(f"expected 0x{expected:032x}")
        assert result == expected

def test_AESRound():
    sim = "icarus"
    hdl_toplevel = "AES_round"
    proj_path = Path(__file__).resolve().parent
    verilog_sources = [
        proj_path/"AES_round.sv",
        proj_path/"AES_sbox.sv",
        proj_path/"AES_MixColumns.sv",
    ]
    runner = cocotb.runner.get_runner(sim)
    runner.build(
        verilog_sources=verilog_sources,
	hdl_toplevel=hdl_toplevel,
        always=True,
    )
    runner.test(hdl_toplevel=hdl_toplevel, test_module=hdl_toplevel + "_test")

if __name__ == "__main__":
    test_AESRound()
