from optimize import OptimizeMatrix
import reedsolo as rs # used for comparison purposes only
import Galois

# references:
# notes7.pdf: https://web.archive.org/web/20140630172526/http://web.stanford.edu/class/ee387/handouts/notes7.pdf
# LinCostello: Lin and Costello "Error Control Coding"

class RSFEC():
    def __init__(self, n=528, k=514, t=7, m=10, prim=0x409):
        self.prim = prim#rs.find_prime_polys(c_exp=m, fast_primes=True, single=True)[0]
        self.gf = Galois.Galois(prim=prim, m=m)
        rs.init_tables(c_exp=m, prim=self.prim)
        self.g = self.gf.generator_poly(2*t)[::-1]
        self.n = n
        self.k = k
        self.m = m
        self.t = t
        self.size = n-k
        self.parity = [0]*self.size
        self.syndrome = [0]*self.size

    def print_g(self):
        rv = "{"
        for gv in self.g:
            rv += f"{self.m}'d{gv}, "
        rv += "};"
        print(rv)

    def reset(self):
        self.parity = [0]*self.size
        self.syndrome = [0]*self.size
    def get_parity(self):
        rv = 0
        for i in range(2*self.t):
            rv |= (self.parity[i] << (self.m*i))
        return rv
    def get_syndrome(self):
        rv = 0
        for i in range(2*self.t):
            rv |= (self.syndrome[i] << (self.m*i))
        return rv
    def encode(self, x): # append a symbol
        px = self.parity[self.n-(self.k+1)] ^ x
        for i in range(1,self.size)[::-1]:
            self.parity[i] = self.gf.mul(px, self.g[i]) ^ self.parity[i-1]
        self.parity[0] = self.gf.mul(px, self.g[0])
    def check_sym(self, x):
        # synd[i] = gf_poly_eval(msg, gf_pow(2,i)), x is element of msg
        for i in range(0,self.size)[::-1]:
            self.syndrome[i] = self.gf.mul(self.syndrome[i], self.gf.exptable[i]) ^ x

    # one iteration of Berlekamp-Massey
    def BerlekampMasseyRound(
            self,
            Lambda, # error locator polynomial, maximum degree is t
            L, # degree of Lambda
            T, # correction polynomial, maximum degree is t
            synd, # syndromes, rotated by k
            k # iteration number 1 .. 2*t
    ):
        assert Lambda[0] == 1 # we will optimize these out in the HDL
        assert T[0] == 0
        delta = synd[0]
        for i in range(1,L+1):
            delta ^= self.gf.mul(Lambda[i],synd[2*self.t-i])
        inv_delta = self.gf.inverse(delta)
        Tprev = T.copy()
        if delta != 0:
            if k > (2*L):
                L = k-L
                T[0] = inv_delta # optimization for i=0 in polynomial scaling
                for i in range(1,self.t):
                    T[i] = self.gf.mul(Lambda[i], inv_delta)

        for i in range(1,self.t+1): # optimization to skip T[0] which is 0
            Lambda[i] ^= self.gf.mul(delta, Tprev[i])

        T = [0] + T[:self.t] # shift correction poly T(x) = xT(x)
        print(f"k={k}, Lambda={Lambda}, L={L}, T={T}, delta={delta}")
        synd = synd[1:] + synd[:1]
        return Lambda, L, T, synd

    def BerlekampMassey(self):
        synd = self.syndrome.copy()
        # notes7 page 66
        print("In BerlekampMassey")

        if True:#self.t == 1:
            print("In BerlekampMassey, t=1")
            if synd[0] == 0:
                assert synd[1] == 0 # uncorrectable if this is not the case
                return []
            ep_exp = self.gf.div(synd[0], synd[1])
            ep = self.gf.log(ep_exp)
            print(ep)
            ep = (2**self.m-1)-self.gf.log(synd[1]) + self.gf.log(synd[0])
            print(ep)
            # L is degree of Lambda
            # T is correction poly, T[0] is always 0
            # optimized first iteration, (1 inv)
            Lambda = [1, synd[0]] # error locator poly, max degree t, Lambda[1] is always 1
            T1 = self.gf.inverse(synd[0])
            print(f"k={1}, Lambda={Lambda}")
            delta = synd[1] ^ self.gf.mul(synd[0],synd[0])
            if delta != 0:
                Lambda = [1, synd[0] ^ self.gf.mul(delta, T1)]
            print(f"k={2}, Lambda={Lambda}, T1={T1}, delta={delta}")
            return Lambda

        # L is degree of Lambda
        # T is correction poly, T[0] is always 0
        # optimized first iteration, (1 inv)
        Lambda = [1, synd[0]] + [0]*(self.t-2) # error locator poly, max degree t, Lambda[1] is always 1
        L = 0
        T = [0,0,1]
        if synd[0] != 0:
            L = 1
            T[1] = self.gf.inverse(synd[0])
            T[2] = 0

        print(f"k={1}, Lambda={Lambda}, L={L}, T={T}, delta={synd[0]}")

        # optimized 2nd iteration (2 mul, 1 inv)
        delta = synd[1] ^ self.gf.mul(synd[0],synd[0])
        inv_delta = self.gf.inverse(delta) # may be 0 but we won't use it if that's the case so OK
        if delta != 0:
            Lambda = [1, synd[0] ^ self.gf.mul(delta, T[1]), delta if T[2] else 0] + [0]*(self.t-3)
            if L==0:
                L = 2
                T = [inv_delta, self.gf.mul(synd[0], inv_delta), 0]
        T = [0] + T # shift correction poly T(x) = xT(x)
        print(f"k={2}, Lambda={Lambda}, L={L}, T={T}, delta={delta}")

        Lambda += [0]*(self.t+1-len(Lambda))
        T += [0]*(self.t+1-len(T))

        syndrot = synd[2:] + synd[:2] # rotate syndrome so we don't have to have muxes in the HDL

        for k in range(3,2*self.t+1):
            Lambda, L, T, syndrot = self.BerlekampMasseyRound(Lambda, L, T, syndrot, k)

        assert L <= self.t
        return Lambda[:L+1]

    # Chien Search
    # inputs are error locator poly, outputs are hit signals
    def Chien(self, err_loc):
        if len(err_loc) == 2:
            print("Chien 1 error case")
        ep_exp = self.gf.inverse(err_loc[1])
        ep = self.gf.log(ep_exp)
        return [ep]

        # XOR only core
        # (t) 10 bit inputs (err_loc),
        # (t) 10 bit outputs (feedback)
        # N 10 bit outputs
        # ==1, (N) 10 bit in, N 1 bit out
        # fine to have 0s in the error locator poly
        errors = len(err_loc) - 1
        err_pos = []
        err_loc = err_loc[1:] + [0]*(8-len(err_loc))
        multipliers = [0]*len(err_loc)
        for i in range(0, len(multipliers)):
            multipliers[i] = self.gf.exp(i+1)
        fb = err_loc.copy() # feedback loop
        # rather than search the first part which is guaranteed to be 0, skip it
        for j in range(len(fb)):
            fb[j] = self.gf.mul(fb[j], self.gf.exp((j+1) * 496))
        for i in range(496,1024): # only check the 528 that matter
            s = 0
            r = fb.copy()
            #print(i,r)
            for j in range(len(fb)):
                s ^= fb[j]
                fb[j] = self.gf.mul(fb[j], multipliers[j])
            if s == 1:
                err_pos.append(i)
        print("Chien", err_loc, err_pos)
        assert len(err_pos) == errors
        return err_pos

    # ref: notes7.pdf
    def Forney(self, synd, err_pos, Lambda): # err_pos is a list of the positions of the errors
        print(f"in Forney, synd={synd}, err_pos={err_pos}, Lambda={Lambda}")

        L = len(Lambda)-1 # degree of Lambda, number of errors to correct

        Omega = [0]*L # Omega error evaluator in  notes7 page 49 21 GF Mul for 7 errors corrected
        assert 1 == Lambda[0]
        for i in range(L):
            Omega[i] = synd[i]
            for j in range(i):
                Omega[i] ^= self.gf.mul(synd[j], Lambda[i-j]) # some terms from Berlekamp
        print(f"Omega = {Omega} (error evaluator)")

        # formal derivative of Lambda, no actual math
        Lambda_prime = [1]*L
        for i in range(0,L):
            Lambda_prime[i] = Lambda[i+1] if (i%2==0) else 0
        print("Lambda_prime", Lambda_prime)

        cor = []

        for i in range(L):
            Xi = self.gf.exp(-err_pos[i]) # root of Lambda
            Xi_inv = self.gf.inverse(Xi) # can do table of err_pos to save latency
            num = self.gf.poly_eval(Omega, Xi_inv) # 7 mul
            num = self.gf.mul(num, Xi) # 1 mul
            den = self.gf.poly_eval(Lambda_prime, Xi_inv) # 3 mul, 1 square
            magnitude = self.gf.div(num, den)
            print(f"E[{err_pos[i]-496}] = {magnitude}")
            cor += [[err_pos[i] - 496, magnitude]]
        return cor

    def Correct(self, codeword):
        self.reset()
        for x in codeword:
            self.check_sym(x)

        print("syndrome", max(self.syndrome), self.syndrome)

        print(list(rs.rs_calc_syndromes(codeword, self.t*2))[1:])
        loc = list(rs.rs_find_error_locator(rs528_514.syndrome, 14))[::-1]
        loc2 = self.BerlekampMassey()
        assert loc2 == loc

        errs_orig = self.Chien(loc)
        errs = errs_orig.copy()
        for i in range(len(errs)):
            errs[i] -= 496
        print("errs", errs, "errs_orig", errs_orig)

        print(self.syndrome)

        cor = self.Forney(self.syndrome, errs_orig, loc)
        data_corrected = codeword.copy()
        for c in cor:
            data_corrected[c[0]] ^= c[1]
        print(data_corrected == data+parity_expected)

rs544_514 = RSFEC(n=544, k=514, t=15, m=10)
# assert that it matches IEEE 802.3 Table 91-1
print(rs544_514.g)
assert rs544_514.g == [523, 834, 128, 158, 185, 127, 392, 193, 610, 788, 361, 883, 503, 942, 385, 495, 720, 94, 132, 593, 249, 282, 565, 108, 1, 552, 230, 187, 552, 575, 1]

rs528_514 = RSFEC()
# assert that it matches IEEE 802.3 Table 91-1
assert rs528_514.g == [432, 290, 945, 265, 592, 391, 614, 900, 925, 656, 32, 701, 6, 904, 1]
rs528_514.print_g()
# example packet from https://grouper.ieee.org/groups/802/3/bj/public/may12/langhammer_01_0512.pdf
data = list(range(510, 1024)[::-1])
parity_expected = [451, 952, 674, 140, 539, 287, 460, 438, 559, 883, 542, 885, 930, 191]
for x in data:
    rs528_514.encode(x)
assert rs528_514.parity[::-1] == parity_expected
rs528_514.reset()
for x in data[:10]:
    rs528_514.encode(x)
    print("p2", rs528_514.parity)

rs528_514.reset()
dp = data + parity_expected
for x in dp:
    rs528_514.check_sym(x)

#print(max(rs528_514.syndrome), rs528_514.syndrome)
assert max(rs528_514.syndrome) == 0

rs528_514.reset()
#dp[31] ^= 1
dp[30] ^= 0x2
#dp[1] ^= 2 #^ 1007
#dp[0] ^=2 # test 2, 2
#dp[527] ^=27
#dp[5] ^= 4
#dp[6] ^= 1023
#dp[7] = 0
#dp[8] = 0

rs528_514.Correct(dp)

# helper functions to convert arrays of 10 bit values to single integer and back
def tentoint(x):
    rv = 0
    for i in range(len(x)):
        rv |= x[i] << 10*i
    return rv

def inttoten(x, n):
    rv = [0] * n
    for i in range(n):
        rv[i] = x & 0x3FF
        x = x >> 10
    return rv

assert parity_expected == inttoten(tentoint(parity_expected),14)

class FECMatrix():
    def __init__(self, dbits, fec):
        self.fec = fec
        self.dbits = dbits
        self.pbits = self.fec.t * 2 * self.fec.m
        assert dbits % self.fec.m == 0
        # generate the parity matrix
        self.parity = [0] * self.pbits
        for i in range(self.pbits):
            fec.reset()
            fec.parity[i//self.fec.m] = 1 << (i%self.fec.m)
            for j in range(dbits//self.fec.m):
                fec.encode(0)
            self.parity[i] = fec.get_parity()
        # generate the data matrix
        self.data = [0] * dbits
        for i in range(dbits):
            fec.reset()
            for j in range(dbits//self.fec.m):
                fec.encode(1<<(i%self.fec.m) if (i//self.fec.m == j) else 0)
            self.data[i] = fec.get_parity()
        # generate the syndrome feedback matrix
        self.syndrome_fb = [0]*self.pbits
        for i in range(self.pbits):
            fec.reset()
            fec.syndrome[i//self.fec.m] = 1 << (i%self.fec.m)
            for j in range(dbits//self.fec.m):
                fec.check_sym(0)
            self.syndrome_fb[i] = fec.get_syndrome()
        # generate the syndrome data matrix
        self.syndrome = [0]*dbits
        for i in range(dbits):
            fec.reset()
            for j in range(dbits//self.fec.m):
                fec.check_sym(1<<(i%self.fec.m) if (i//self.fec.m == j) else 0)
            self.syndrome[i] = fec.get_syndrome()

    # only used for assertion to validate matrix
    def compute(self, data, n):
        parity = 0
        for i in range((n*self.fec.m)//self.dbits):
            parity_next = 0
            # advance parity
            for j in range(self.pbits):
                if parity & (1<<j):
                    parity_next ^= self.parity[j]
            for j in range(self.dbits):
                if data & (1<<(j+self.dbits*i)):
                    parity_next ^= self.data[j]
            parity = parity_next
        return parity

def FECMatrix_test():
    matrix330 = FECMatrix(330, rs528_514)
    testcaseint = tentoint(data)
    assert parity_expected == inttoten(matrix330.compute(testcaseint<<140, 528),14)[::-1]
