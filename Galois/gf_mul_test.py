# Test bench for gf_mul
# Copyright (C) 2023 Darrell Harmon
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

import cocotb
import cocotb.runner
from cocotb.clock import Clock
from cocotb.triggers import Timer, RisingEdge
from pathlib import Path
import Galois

@cocotb.test()
async def run_test(dut):
    m = int(dut.M.value)
    poly = int(dut.POLY.value)
    print(f"GF(2^{m}), poly={poly:X}")
    gf = Galois.Galois(prim=poly, m=m)
    for a in range(2**m):
        for b in range(2**m):
            dut.a.value = a
            dut.b.value = b
            await Timer(1)
            p = dut.p.value.integer
            p_expected = gf.mul(a,b)
            if p != p_expected:
                print(f"a={a},b={b},p={p},e={p_expected}")
            assert p == p_expected

def test_gf_mul(m=8, poly="9'h11D"):
    sim = "icarus"
    hdl_toplevel = "gf_mul"
    proj_path = Path(__file__).resolve().parent
    verilog_sources = [
        proj_path/"gf_mul.sv",
    ]
    runner = cocotb.runner.get_runner(sim)
    runner.build(
        verilog_sources=verilog_sources,
	hdl_toplevel=hdl_toplevel,
        parameters={"M":m, "POLY":poly},
        always=True,
    )
    runner.test(hdl_toplevel=hdl_toplevel, test_module="gf_mul_test")

if __name__ == "__main__":
    test_gf_mul(m=8, poly="9'h11D")
    #test_gf_mul(m=10, poly="11'h409") # takes 20 seconds, uncomment to run
