// Copyright (C) 2024 Darrell Harmon
// SPDX-License-Identifier: BSD-2-Clause-NetBSD
// test wrapper for Reed Solomon n=72, k=70, t=1, m=8, poly=0x11D, generator=2

module rs72_70_test
  (
   input [559:0]  i,
   input [575:0]  error,
   output [559:0] out
   );

   wire [575:0]   encoded;
   assign encoded[559:0] = i;

   rs72_70_encode encode(.i(i), .o({encoded[560+:8], encoded[568+:8]}));
   rs72_70_decode decode(.in(encoded^error), .out(out));

endmodule
