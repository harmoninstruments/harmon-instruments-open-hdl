/*
 AES256, one block in 14 clock cycles
 Copyright (C) 2023-2024 Darrell Harmon
 SPDX-License-Identifier: BSD-2-Clause-NetBSD

 */

module AES256_iterative
   (
    input                clock,
    input                start, // assert at most every 14th cycle with key and din
    input logic [255:0]  key, // must be valid on the cycle start is asserted
    input logic [127:0]  din, // must be valid on the cycle start is asserted
    output logic         ovalid = 0, // 16 cycles after start, OK to start another before
    output logic [127:0] out // valid with ovalid
   );

   logic [127:0]         round0, round_dout;
   logic [255:0]         keysched = 0;
   logic [3:0]           state = 0;
   logic                 start_1 = 0;
   logic                 ce = 0;
   logic                 ceo = 0;

   always_ff @ (posedge clock) begin
      start_1 <= start;
      if(start)
        round0 <= din ^ key[127:0];
      ce <= start | ((state != 0) & (state < 13));
      ceo <= state == 13;
      ovalid <= ceo;
      state <= start ? 1'b1 : state + (state != 0);

   end

   // AES round
   logic [127:0] shiftrows;
   wire [127:0] sdin = start_1 ? round0 : round_dout;

   // s-box and shift rows
   for(genvar i=0; i<16; i++) begin : sboxes
      AES_sbox sb (.c(sdin[((i + (i%4)*20)%16)*8 +:8]), .s(shiftrows[i*8 +:8]));
   end

   // MixColumns
   logic [127:0] mixc;
   for(genvar i=0; i<4; i++) begin : mixcol
      AES_MixColumns MixColumns (.b(shiftrows[32*i +:32]), .d(mixc[32*i +:32]));
   end

   wire [127:0] roundkey = keysched[128 +:128];
   always_ff @ (posedge clock)
     begin
        if(ce)
          round_dout <= mixc ^ roundkey;
        if(ceo)
          out <= shiftrows ^ roundkey;
     end

   // the key schedule
   // RotWord with bypass for if even round, no rotation if odd   w[i-1]
   wire [31:0]          rotword_opt = state[0] ? {keysched[224+:8],keysched[232 +:24]} : keysched[224 +:32];
   wire [31:0]          subword;
   logic [6:0]          rcon = 0;
   logic [5:0]          rcon_background = 0;

   for(genvar i=0; i<4; i++) begin
      AES_sbox sb (.c(rotword_opt[i*8 +:8]), .s(subword[i*8 +:8]));
   end

   wire [31:0] o128 = subword ^ rcon ^ keysched[0 +:32];

   always_ff @ (posedge clock) begin
      // round constant rcon is 1, 0, 2, 0, 4, 0, 8, ...
      rcon <= start ? 1'b1 : {rcon_background,1'b0};
      rcon_background <= rcon;

      if(start)
        keysched <= key;
      else if(ce)
        keysched <= {{4{o128}} ^ {{3{keysched[32 +:32]}}, 32'h0} ^ {{2{keysched[64 +:32]}}, 64'h0} ^
                           {{keysched[96 +:32]}, 96'h0}, keysched[128 +:128]};
   end
endmodule
