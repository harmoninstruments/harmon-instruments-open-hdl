# Test bench for rs72_70
# Copyright (C) 2024 Darrell Harmon
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

import cocotb
import cocotb.runner
from cocotb.clock import Clock
from cocotb.triggers import Timer, RisingEdge
from pathlib import Path

@cocotb.test()
async def run_test(dut):

    # all correctables
    for i in range(0,72):
        for j in range(0, 256):
            dut.i.value = 1
            dut.error.value = j<<(8*i)
            await Timer(1)
            #print("i", dut.i.value)
            #print("error", dut.error.value)
            #print("encoded", dut.encoded.value)
            print("par0", dut.encode.o.value.integer & 0xFF)
            print("par1", dut.encode.o.value.integer >> 8)
            print("synd0", dut.decode.synd0.value.integer)
            print("synd1", dut.decode.synd1.value.integer)
            print("errmag", dut.decode.error_magnitude.value.integer)
            print("errpos", dut.decode.error_position.value.integer)
            print("epraw", dut.decode.ep_raw.value.integer)
            cerr = dut.decode.correctable_error.value.integer
            uerr = dut.decode.uncorrectable_error.value.integer
            print("cerr", cerr)
            assert cerr == (1 if j != 0 else 0)
            print("uerr", uerr)
            assert uerr == 0
            print("errf", dut.decode.errorflags.value)
            p = dut.out.value.integer
            assert p == 1

    dut.i.value = 1

    for i in range(0,72):
        for j in range(0, 72):
            if i == j:
                continue
            for k in range(0,8):
                for l in range(0,8):
                    dut.error.value = k<<(8*i) | l<<(8*j)
                    await Timer(1)
                    print("uncorrectables", i, j, k, l)
                    #print("i", dut.i.value)
                    #print("error", dut.error.value)
                    #print("encoded", dut.encoded.value)
                    print("par0", dut.encode.o.value.integer & 0xFF)
                    print("par1", dut.encode.o.value.integer >> 8)
                    print("synd0", dut.decode.synd0.value.integer)
                    print("synd1", dut.decode.synd1.value.integer)
                    print("errmag", dut.decode.error_magnitude.value.integer)
                    print("errpos", dut.decode.error_position.value.integer)
                    print("epraw", dut.decode.ep_raw.value.integer)
                    cerr = dut.decode.correctable_error.value.integer
                    uerr = dut.decode.uncorrectable_error.value.integer
                    print("cerr", cerr)
                    assert cerr == 0
                    print("uerr", uerr)
                    assert uerr == 1
                    print("errf", dut.decode.errorflags.value)
                    print("out", dut.out.value)
                    #p = dut.out.value.integer
                    #assert p == 1

def test_rs72_70():
    sim = "icarus"
    hdl_toplevel = "rs72_70_test"
    proj_path = Path(__file__).resolve().parent
    verilog_sources = [
        proj_path/"rs72_70_test.sv",
        proj_path/"rs72_70_encode.sv",
        proj_path/"rs72_70_syndrome.sv",
        proj_path/"rs72_70_decode.sv",
        proj_path/"gf_log_11d.sv",
    ]
    runner = cocotb.runner.get_runner(sim)
    runner.build(
        verilog_sources=verilog_sources,
	hdl_toplevel=hdl_toplevel,
        always=True,
    )
    runner.test(hdl_toplevel=hdl_toplevel, test_module="rs72_70_test")

if __name__ == "__main__":
    test_rs72_70()
