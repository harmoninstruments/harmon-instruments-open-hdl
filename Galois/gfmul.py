# Copyright 2023 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth import *
class GFMul(Elaboratable):
    def __init__(self, a, b, m=8, poly=0x11D):
        self.a = a
        self.b = b
        self.m = m
        self.poly = poly
        self.o = Signal(m)
    def elaborate(self, platform):
        m = Module()
        am = Array(Signal(self.m, name="am{}".format(a)) for a in range(self.m))
        m.d.comb += am[0].eq(self.a)
        for i in range(1, self.m):
            m.d.comb += am[i].eq((am[i-1] << 1) ^ Mux(am[i-1][-1], self.poly, 0))
        for i in range(self.m):
            sigs_to_xor = []
            for j in range(self.m):
                sigs_to_xor.append(self.b[j] & am[j][i])
            m.d.comb += self.o[i].eq(Cat(sigs_to_xor).xor())
        return m

if __name__ == "__main__":
    from amaranth.cli import main
    a = Signal(8)
    b = Signal(8)
    mul = GFMul(a,b)
    main(mul, ports=[a, b, mul.o])
