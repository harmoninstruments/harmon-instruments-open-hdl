# utilities for AES test benches and generators
# Copyright (C) 2023-2024 Darrell Harmon
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from struct import unpack
from Crypto.Cipher import AES
from Crypto.Random import get_random_bytes

def tvec256(x):
    s = bytes.fromhex(x)
    (r1,r2,r3,r4) = unpack("QQQQ", s)
    return r1 | (r2 << 64) | (r3 << 128) | (r4 << 192)

def tvec128(x):
    s = bytes.fromhex(x)
    (r1,r2) = unpack("QQ", s)
    return r1 | (r2 << 64)

# from FIPS-197, these have LS byte on the left, need to run though tvec256()
roundkeys_hex = [
    '000102030405060708090a0b0c0d0e0f', # round 0 key
    '101112131415161718191a1b1c1d1e1f', # round 1 key
    'a573c29fa176c498a97fce93a572c09c',
    '1651a8cd0244beda1a5da4c10640bade',
    'ae87dff00ff11b68a68ed5fb03fc1567',
    '6de1f1486fa54f9275f8eb5373b8518d',
    'c656827fc9a799176f294cec6cd5598b',
    '3de23a75524775e727bf9eb45407cf39',
    '0bdc905fc27b0948ad5245a4c1871c2f',
    '45f5a66017b2d387300d4d33640a820a',
    '7ccff71cbeb4fe5413e6bbf0d261a7df',
    'f01afafee7a82979d7a5644ab3afe640',
    '2541fe719bf500258813bbd55a721c0a',
    '4e5a6699a9f24fe07e572baacdf8cdea',
    '24fc79ccbf0979e9371ac23c6d68de36', # round 14
]

tvec_roundkeys = []
for k in roundkeys_hex:
    tvec_roundkeys.append(tvec128(k))

tvec_key_256 = tvec256('000102030405060708090a0b0c0d0e0f101112131415161718191a1b1c1d1e1f')
tvec_din_256 = tvec128('00112233445566778899aabbccddeeff')
tvec_out_256 = tvec128('8ea2b7ca516745bfeafc49904b496089')

def py_encrypt_256(x, key):
    data = x.to_bytes(16, byteorder='little')
    key = key.to_bytes(32, byteorder='little')
    cipher = AES.new(key, AES.MODE_ECB)
    ct_bytes = cipher.encrypt(data)
    return int.from_bytes(ct_bytes, byteorder='little')

assert py_encrypt_256(tvec_din_256, tvec_key_256) == tvec_out_256

def randomint(bits):
    return int.from_bytes(get_random_bytes(bits//8), byteorder='little')
