/*
 One round of AES
 Copyright (C) 2023 Darrell Harmon
 SPDX-License-Identifier: BSD-2-Clause-NetBSD

 bit assignments to rows and columns
 row 0 [ 7: 0] [39:32] [71:64] [103: 96]
 row 1 [15: 8] [47:40] [79:72] [111:104]
 row 2 [23:16] [55:48] [87:80] [119:112]
 row 3 [31:24] [63:56] [95:88] [127:120]

 the shiftrows output is prior to MixColumns,
 this is taken as the output of the last round

 */

module AES_round (input logic [127:0]  din, output logic [127:0] shiftrows, out);

   // s-box and sub_bytes
   for(genvar i=0; i<16; i++) begin : sboxes
      AES_sbox sb (.c(din[((i + (i%4)*20)%16)*8 +:8]), .s(shiftrows[i*8 +:8]));
   end

   for(genvar i=0; i<4; i++) begin : mixcol
      AES_MixColumns mixc (.b(shiftrows[32*i +:32]), .d(out[32*i +:32]));
   end
endmodule
