// Copyright (C) 2024 Darrell Harmon
// SPDX-License-Identifier: BSD-2-Clause-NetBSD
// decoder for Reed Solomon n=72, k=70, t=1, m=8, poly=0x11D, generator=2

module rs72_70_decode
  (
   input [575:0]  in,
   output [559:0] out,
   output         uncorrectable_error,
   output         correctable_error,
   output [7:0]   error_magnitude,
   output [7:0]   error_position
   );

   logic [7:0] synd0, synd1, synd0_log, synd1_log;

   wire        error_exists = (synd0 != 0) | (synd1 != 0);
   assign correctable_error = error_exists & (synd1 != 0) & (error_position < 72);

   // if synd0 is 0, synd1 is not 0, we have at least 2 errors as synd0 is the sum of magnitude errors
   // if error_posistion > 71 this indicates the error is in a symbol outside bounds,
   // implies a multiple symbol error
   // this code doesn't have strong error detection
   assign uncorrectable_error = error_exists & ((synd1 == 0) || (error_position > 71));

   rs72_70_syndrome syndrome_calc(.i(in), .o({synd1,synd0}));

   // error magnitude is simply synd0 as synd0 is the sum of magnitude errors and we are only
   // correcting one error
   assign error_magnitude = synd0;

   // error location is gf_log(synd0/synd1)
   // push the log to before division so it's subtraction instead
   gf_log_11d log_s0(.x(synd0), .y(synd0_log));
   gf_log_11d log_s1(.x(synd1), .y(synd1_log));
   // the substraction of the logs is almost normal integer but modulo 255 to be correct
   // TODO: explain this, why the mod255, why not GF math, break this operation up
   // the -184 is due to the fact that this is really a truncated RS(255,253) code.
   // TODO: explain that better
   wire [8:0] ep_raw = synd0_log + 8'd255 - synd1_log;
   assign error_position = (ep_raw[8] ? ep_raw - 8'd255 : ep_raw) - 8'd184;

   // if bit n is 1, we have an error in byte n, XOR with error_magnitude to correct
   wire [69:0] errorflags;

   generate
      for(genvar i=0; i<70; i++) begin : correct_byte
         assign errorflags[i] = correctable_error && (error_position == i);
         assign out[8*i +: 8] = in[8*i +: 8] ^ (errorflags[i] ? error_magnitude : 1'b0);
      end
   endgenerate

endmodule
