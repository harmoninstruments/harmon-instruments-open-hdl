# Test bench for AES256 key schedule
# Copyright (C) 2023 Darrell Harmon
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

import cocotb
import cocotb.runner
from cocotb.clock import Clock
from cocotb.triggers import Timer, RisingEdge
from pathlib import Path
from struct import unpack

from AES_util import tvec_roundkeys, tvec_key_256

roundkeys = tvec_roundkeys[1:] # the round 0 key is taken directly from the input so skip

@cocotb.test()
async def run_test(dut):
    dut.kin.value = tvec_key_256
    dut.rcon.value = 1
    dut.do_rotword.value = 1
    for i in range(len(roundkeys)):
        await Timer(1)
        roundkey = roundkeys[i]
        result = dut.kout.value.integer
        dut.kin.value = result
        dut.do_rotword.value = 1 if i&1 else 0
        dut.rcon.value = [0,2,0,4,0,8,0,16,0,32,0,64,0,0][i]
        result &= (2**128) -1
        print(f"expected 0x{roundkey:032x}")
        print(f"result   0x{result:032x}")
        assert result == roundkey

def test_AESRound():
    sim = "icarus"
    hdl_toplevel = "AES256_key_schedule"
    proj_path = Path(__file__).resolve().parent
    verilog_sources = [
        proj_path/"AES256_key_schedule.sv",
        proj_path/"AES_sbox.sv",
        proj_path/"AES_MixColumns.sv",
    ]
    runner = cocotb.runner.get_runner(sim)
    runner.build(
        verilog_sources=verilog_sources,
	hdl_toplevel=hdl_toplevel,
        always=True,
    )
    runner.test(hdl_toplevel=hdl_toplevel, test_module=hdl_toplevel + "_test")

if __name__ == "__main__":
    test_AESRound()
