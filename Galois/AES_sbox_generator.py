#!/usr/bin/env python3
# Copyright 2023 Darrell Harmon
# SPDX-License-Identifier: BSD-2-Clause-NetBSD")

import Galois

def rotl8(x, n):
    for i in range(n):
        x = x << 1
        if (x & 0x100):
            x |= 1
            x ^= 0x100
    return x

gf = Galois.Galois(0x11B, 8, 3)
print("// generated by AES_sbox_generator.py")
print("// AES S-box table generator")
print("// SPDX-License-Identifier: BSD-2-Clause-NetBSD")
print("")
print("module AES_sbox (")
print("   input logic [7:0] c,")
print("   output logic [7:0] s")
print( "   );")
print("")
print("   logic [7:0] rom [0:255];")
print("")
print("   assign s = rom[c];")
print("")
print("   initial begin")
for c in range(0, 256):
    b = gf.inverse(c)
    s = b ^ rotl8(b, 1) ^ rotl8(b, 2) ^ rotl8(b, 3) ^ rotl8(b,4) ^ 0x63
    print(f"      rom[{c}] = 8'h{s:02x};")
print("   end")
print("endmodule")
