/*
 AES256, one block per clock cycle
 Copyright (C) 2023-2024 Darrell Harmon
 SPDX-License-Identifier: BSD-2-Clause-NetBSD

 */

// key schedule for even numbered rounds
module AES256_key_schedule0
  (
   input                clock, ce,
   input logic [255:0]  kin, // raw key or from previous round
   input logic [6:0]    rcon, // Rcon[i]
   output logic [255:0] o); // low 128 are next round key, all 256 go to next round

   wire [31:0]          rotword = {kin[224+:8],kin[232 +:24]};
   wire [31:0]          subword;

   for(genvar i=0; i<4; i++) begin
      AES_sbox sb (.c(rotword[i*8 +:8]), .s(subword[i*8 +:8]));
   end

   logic [255:0] kout_next;

   assign kout_next[0  +:128] = kin[128 +:128];
   assign kout_next[128 +:32] = subword ^ rcon ^ kin[0 +:32]; // one special case key expansion word
   assign kout_next[160 +:96] = kout_next[128 +:96] ^ kin[32 +:96]; // three normal key expansion words

   always_ff @ (posedge clock)
     begin
        if(ce)
          o <= kout_next;
     end
endmodule

// key schedule for odd numbered rounds
module AES256_key_schedule1
  (
   input                clock, ce,
   input logic [255:0]  kin, // key out of previous round of key schedule
   output logic [255:0] o); // low 128 are next round key, all 256 go to next round

   wire [31:0]          subword;

   for(genvar i=0; i<4; i++) begin
      AES_sbox sb (.c(kin[224+i*8 +:8]), .s(subword[i*8 +:8]));
   end

   logic [255:0] kout_next;

   assign kout_next[0  +:128] = kin[128 +:128];
   assign kout_next[128 +:32] = subword ^ kin[0 +:32]; // one special case key expansion word
   assign kout_next[160 +:96] = kout_next[128 +:96] ^ kin[32 +:96]; // three normal key expansion words

   always_ff @ (posedge clock)
     begin
        if(ce)
          o <= kout_next;
     end
endmodule

module AES_round_reg (input clock, ce, input logic [127:0]  din, kin, output logic [127:0] out);
   logic [127:0] shiftrows;
   // s-box and sub_bytes
   for(genvar i=0; i<16; i++) begin : sboxes
      AES_sbox sb (.c(din[((i + (i%4)*20)%16)*8 +:8]), .s(shiftrows[i*8 +:8]));
   end

   logic [127:0] mixc;
   for(genvar i=0; i<4; i++) begin : mixcol
      AES_MixColumns MixColumns (.b(shiftrows[32*i +:32]), .d(mixc[32*i +:32]));
   end

   always_ff @ (posedge clock)
     begin
        if(ce)
          out <= mixc ^ kin;
     end
endmodule

module AES_round_final_reg // final round has no MixColumns
  (input clock, ce, input logic [127:0]  din, kin, output logic [127:0] out);
   logic [127:0] shiftrows;
   // s-box and sub_bytes
   for(genvar i=0; i<16; i++) begin : sboxes
      AES_sbox sb (.c(din[((i + (i%4)*20)%16)*8 +:8]), .s(shiftrows[i*8 +:8]));
   end

   always_ff @ (posedge clock)
     begin
        if(ce)
          out <= shiftrows ^ kin;
     end
endmodule

module AES256_unrolled
  (
   input                clock,
   input                dvalid, kvalid, // clock enables for din and key
   input logic [255:0]  key, // can rekey with no gap if key and data change together
   input logic [127:0]  din,
   output logic         ovalid = 0, // 16 stage pipeline from din/dvalid
   output logic [127:0] out // valid with ovalid
   );

   logic [14:0]         ce_data = 0;
   logic [14:0]         ce_key = 0;
   logic [127:0]        din_buf;
   logic [255:0]        key_buf;

   generate
      for(genvar i=0; i<13; i++) begin : keyschedule // keys for 13, 14 are separate
         logic [255:0] o;
         if(i == 0) begin
            AES256_key_schedule0 ks_0 (.clock, .ce(ce_key[0]), .kin(key_buf), .rcon(7'd1), .o);
         end else if(i&1) begin
            AES256_key_schedule1 ks_i (.clock, .ce(ce_key[i]), .kin(keyschedule[i-1].o), .o);
         end else begin
            wire [6:0] rcon = 1 << (i>>1);
            AES256_key_schedule0 ks_i (.clock, .ce(ce_key[i]), .kin(keyschedule[i-1].o), .rcon, .o);
         end
      end
   endgenerate

   logic [127:0] rk14;
   always_ff @ (posedge clock) begin
      if(ce_key[13])
        rk14 <= keyschedule[12].o[128 +:128];

      ce_data <= {ce_data[13:0], dvalid};
      ce_key <=  {ce_key[13:0], kvalid};
      if(dvalid)
        din_buf <= din;
      if(kvalid)
        key_buf <= key;
   end

   logic [127:0] r0, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13;
   always_ff @ (posedge clock) begin // round 0 is just a XOR
      if(ce_data[0])
        r0 <= din_buf ^ key_buf[0 +:128];
   end
   AES_round_reg round_1 (.clock, .ce(ce_data[ 1]), .din(r0 ), .kin(keyschedule[0].o[0 +:128]), .out(r1));
   AES_round_reg round_2 (.clock, .ce(ce_data[ 2]), .din(r1 ), .kin(keyschedule[1].o[0 +:128]), .out(r2));
   AES_round_reg round_3 (.clock, .ce(ce_data[ 3]), .din(r2 ), .kin(keyschedule[2].o[0 +:128]), .out(r3));
   AES_round_reg round_4 (.clock, .ce(ce_data[ 4]), .din(r3 ), .kin(keyschedule[3].o[0 +:128]), .out(r4));
   AES_round_reg round_5 (.clock, .ce(ce_data[ 5]), .din(r4 ), .kin(keyschedule[4].o[0 +:128]), .out(r5));
   AES_round_reg round_6 (.clock, .ce(ce_data[ 6]), .din(r5 ), .kin(keyschedule[5].o[0 +:128]), .out(r6));
   AES_round_reg round_7 (.clock, .ce(ce_data[ 7]), .din(r6 ), .kin(keyschedule[6].o[0 +:128]), .out(r7));
   AES_round_reg round_8 (.clock, .ce(ce_data[ 8]), .din(r7 ), .kin(keyschedule[7].o[0 +:128]), .out(r8));
   AES_round_reg round_9 (.clock, .ce(ce_data[ 9]), .din(r8 ), .kin(keyschedule[8].o[0 +:128]), .out(r9));
   AES_round_reg round_10(.clock, .ce(ce_data[10]), .din(r9 ), .kin(keyschedule[9].o[0 +:128]), .out(r10));
   AES_round_reg round_11(.clock, .ce(ce_data[11]), .din(r10), .kin(keyschedule[10].o[0 +:128]), .out(r11));
   AES_round_reg round_12(.clock, .ce(ce_data[12]), .din(r11), .kin(keyschedule[11].o[0 +:128]), .out(r12));
   AES_round_reg round_13(.clock, .ce(ce_data[13]), .din(r12), .kin(keyschedule[12].o[0 +:128]), .out(r13));
   AES_round_final_reg round_14(.clock, .ce(ce_data[14]), .din(r13), .kin(rk14), .out);

   always_ff @ (posedge clock) begin
      ovalid <= ce_data[14];
   end

endmodule
