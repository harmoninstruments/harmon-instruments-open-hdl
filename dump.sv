// Copyright (C) 2023 Darrell Harmon
// SPDX-License-Identifier: BSD-2-Clause-NetBSD

`timescale 1 ns / 1 ps

module dump();
  initial begin
     $dumpfile ("dump.vcd"); // Change filename as appropriate.
     $dumpvars();
  end
endmodule
