# Copyright (C) 2020 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth import *
from amaranth.sim import Simulator, Passive

def test_cdc_gray():
    from CDC.CDC import CDC_Gray
    i = Signal(8)
    dut = CDC_Gray(i=i)
    sim = Simulator(dut)
    sim.add_clock(8e-9)
    def sim_proc():
        for j in range(30):
            yield i.eq(j)
            yield
            yield
            yield
            yield
            assert (yield dut.o) == j
    sim.add_sync_process(sim_proc)
    with sim.write_vcd("dump.vcd"):
        sim.run()
