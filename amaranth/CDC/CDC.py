# Copyright 2020 Harmon Instruments
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth import *
from amaranth.lib.cdc import FFSynchronizer

# clock domain crossing for a single cycle pulse
# pulse repetition rate < cout rate

class CDC_pulse(Elaboratable):
    def __init__(self, i, o, cin='sync', cout='sync', stages=2, max_input_delay = None):
        self.i = i
        self.o = o
        self.cin = cin
        self.cout = cout
        self.stages = stages
        self.max_input_delay = max_input_delay

    def elaborate(self, platform):
        m = Module()

        # t toggles when I is asserted
        t = Signal()
        m.d[self.cin] += t.eq(t ^ self.i)

        t_oclk = Signal()
        m.submodules.mreg = FFSynchronizer(
            i=t,
            o=t_oclk,
            o_domain=self.cout,
            stages=self.stages,
            max_input_delay = self.max_input_delay
        )

        t_prev = Signal()

        m.d[self.cout] += [
            t_prev.eq(t_oclk),
            self.o.eq(t_oclk^t_prev),
        ]

        return m

# clock domain crossing for a bus with clock enable
# pulse repetion rate < cout/4
# latch the data on cei

class CDC_CE_bus(Elaboratable):
    def __init__(self, i, cei, o, ceo=None, cin='sync', cout='sync', max_input_delay = None):
        self.i = i
        self.cei = cei
        self.o = o
        self.ceo = ceo
        self.cin = cin
        self.cout = cout
        self.max_input_delay = max_input_delay

    def elaborate(self, platform):
        m = Module()

        ce_oclk = Signal()
        m.submodules.syncce = CDC_pulse(
            i = self.cei,
            o = ce_oclk,
            cin = self.cin,
            cout = self.cout,
            stages = 4,
            max_input_delay = self.max_input_delay)

        ibuf = Signal(len(self.i))

        with m.If(self.cei):
            m.d[self.cin] += ibuf.eq(self.i)

        d_oclk = Signal(len(self.i))

        m.submodules.mreg = FFSynchronizer(
            i=ibuf,
            o=d_oclk,
            o_domain=self.cout,
            stages=2,
            max_input_delay=self.max_input_delay
        )

        with m.If(ce_oclk):
            m.d[self.cout] += self.o.eq(d_oclk)

        if not (self.ceo is None):
            m.d[self.cout] += self.ceo.eq(ce_oclk)

        return m

# clock domain crossing accepting an async pulse, outputting a single pulse
# corresponding to the rising edge

class CDC_oneshot(Elaboratable):
    def __init__(self, i, o, cout='sync', stages=2, max_input_delay = None):
        self.i = i
        self.o = o
        self.cout = cout
        self.stages = stages
        self.max_input_delay = max_input_delay

    def elaborate(self, platform):
        m = Module()

        isync = Signal()
        m.submodules.mreg = FFSynchronizer(
            i=self.i,
            o=isync,
            o_domain=self.cout,
            stages=self.stages,
            max_input_delay=self.max_input_delay
        )

        i_prev = Signal()

        m.d[self.cout] += [
            i_prev.eq(isync),
            self.o.eq(isync & ~i_prev),
        ]

        return m

class CDC_Gray(Elaboratable):
    def __init__(self, i, cin='sync', cout='sync', stages=2, max_input_delay = None):
        self.i = i
        self.o = Signal(len(i))
        self.cin = cin
        self.cout = cout
        self.stages = stages
        self.max_input_delay = max_input_delay

    def elaborate(self, platform):
        m = Module()
        encoded = Signal(len(self.i))
        m.d[self.cin] += encoded.eq(self.i ^ self.i[1:])
        encoded_cout = Signal(len(self.i))
        m.submodules.mreg = FFSynchronizer(
            i=encoded,
            o=encoded_cout,
            o_domain=self.cout,
            stages=self.stages,
            max_input_delay=self.max_input_delay
        )
        m.d.comb += self.o[-1].eq(encoded_cout[-1])
        for i in reversed(range(len(self.i) - 1)):
            m.d.comb += self.o[i].eq(self.o[i + 1] ^ encoded_cout[i])
        return m
