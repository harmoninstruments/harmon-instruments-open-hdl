# Test bench for cdc_fifo
# Copyright (C) 2023-2025 Darrell Harmon
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

import cocotb
import cocotb.runner
from cocotb.clock import Clock
from cocotb.triggers import Timer, RisingEdge, ReadOnly
from pathlib import Path
from struct import unpack

@cocotb.test()
async def run_test(dut):
    dut.ivalid.value = 0
    dut.oready.value = 0
    cocotb.start_soon(Clock(dut.iclock, 2000).start())
    cocotb.start_soon(Clock(dut.oclock, 2100).start())
    await Timer(1)
    await RisingEdge(dut.iclock)
    count = 0
    count_almost = 0
    count_ovalid = 0
    while dut.iready.value.integer == 1:
        dut.ivalid.value = 1
        dut.idata.value = count
        count += 1
        if dut.ialmostfull.value.integer == 0:
            count_almost += 1
        if dut.ovalid.value.integer == 0: # not really in the right clock domain
            count_ovalid += 1
        await RisingEdge(dut.iclock)
    count -= 1
    dut.ivalid.value = 0
    print(f"initial fill {count} almost full {count_almost} ovalid {count_ovalid}")
    assert count < 32
    assert count > 24
    await RisingEdge(dut.oclock)
    await RisingEdge(dut.oclock)
    dut.oready.value = 1
    for i in range(count):
        await ReadOnly()
        print(i, dut.odata.value.integer, dut.ovalid.value.integer)# == i
        assert dut.odata.value.integer == i
        assert dut.ovalid.value.integer == 1
        await RisingEdge(dut.oclock)
    await RisingEdge(dut.oclock)
    await ReadOnly()
    assert dut.ovalid.value.integer == 0
    await RisingEdge(dut.iclock)
    dut.ivalid.value = 1
    await RisingEdge(dut.iclock)
    dut.ivalid.value = 0
    while True:
        await RisingEdge(dut.oclock)
        await ReadOnly()
        if dut.ovalid.value.integer == 1:
            assert dut.odata.value.integer == count
            break
    await RisingEdge(dut.oclock)
    await ReadOnly()
    assert dut.ovalid.value.integer == 0
    await RisingEdge(dut.oclock)
    await RisingEdge(dut.oclock)

def test_cdc_fifo_axis():
    sim = "icarus"
    hdl_toplevel = "cdc_fifo_axis"
    proj_path = Path(__file__).resolve().parent
    verilog_sources = [
        proj_path/"cdc_fifo_axis.sv",
        proj_path/"cdc_maxdelay.sv",
        proj_path/"../dump.sv",
    ]
    runner = cocotb.runner.get_runner(sim)
    runner.build(
        verilog_sources=verilog_sources,
	    hdl_toplevel=hdl_toplevel,
        always=True,
    )
    runner.test(hdl_toplevel=hdl_toplevel, test_module=hdl_toplevel + "_test")

if __name__ == "__main__":
    test_cdc_fifo_axis()
