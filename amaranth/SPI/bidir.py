# Copyright (C) 2020 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

# Bidirectional SPI with shared SDIO
# 1 byte per transaction
# to read, write 0x200 to port
# to write, write data byte to port
# to terminate (set CS), write 0x100 to port
# read data bit 8 indicates busy

# used for AD9628, AD9735

from amaranth import *
from SPI.flash import SPI_flash

class SPI_bidir(Elaboratable):
    def __init__(self, pins, pio, addr):
        self.pins = pins
        self.pio = pio
        self.wv = pio.get_wvalid(addr)
        self.spi = SPI_flash(
            sck=pins.sck.o,
            copi=pins.sdio.o,
            cipo=pins.sdio.i,
            cs=pins.cs.o,
            wv=self.wv,
            wdata=pio.wdata[:9],
	)
        pio.add_ro_reg(addr, self.spi.rdata)
    def elaborate(self, platform):
        m = Module()
        m.submodules.spi_core = self.spi
        with m.If(self.wv):
            m.d.sync += self.pins.sdio.oe.eq(~self.pio.wdata[9])
        return m
