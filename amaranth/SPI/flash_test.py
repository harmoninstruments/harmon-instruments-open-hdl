# Copyright (C) 2018-2020 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth import *
from SPI.flash import *

def test_spi_flash():
    from amaranth.sim import Simulator
    sck = Signal()
    copi = Signal()
    cipo = Signal()
    cs = Signal(reset=1)
    wv = Signal()
    wdata = Signal(9)
    dut = SPI_flash(sck, copi, cipo, cs, wv, wdata)
    def write(x):
        yield wdata.eq(x)
        yield wv.eq(1)
        yield
        yield wv.eq(0)
        for i in range(130):
            yield

    def test_proc():
        yield
        yield from write(0x155)
        yield from write(0x055)
        rdata = yield dut.rdata
        print(rdata)

    sim = Simulator(dut)#, vcd_file=open("dump.vcd", "w"))
    sim.add_clock(8e-9)
    sim.add_sync_process(test_proc)
    with sim.write_vcd("dump.vcd", "dump.gtkw", traces=dut.ports):
        sim.run()
