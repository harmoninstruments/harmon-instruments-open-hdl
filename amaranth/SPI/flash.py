# Copyright (C) 2018-2020 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD
# write 0x100 to set CS and terminate
from amaranth import *

class SPI_flash(Elaboratable):
    def __init__(self, sck, copi, cipo, cs, wv, wdata):
        self.args = (sck, copi, cipo, cs, wv, wdata)
        self.dout = Signal(8)
        self.busy = Signal()
        self.rdata = Cat(self.dout, self.busy)
        self.ports = [sck, copi, cipo, cs, wv, wdata, self.dout, self.busy]

    def elaborate(self, platform):
        (sck, copi, cipo, cs, wv, wdata) = self.args
        m = Module()
        cipoq = Signal()
        state = Signal(7)

        with m.If(wv):
            m.d.sync += cs.eq(wdata[8])

        with m.If(wv & ~wdata[8]):
            m.d.sync += [
                self.dout.eq(wdata),
                state.eq(1),
                self.busy.eq(1),
            ]
        with m.Elif(state != 0):
            m.d.sync += state.eq(state + 1)
            with m.If(state[:4] == 15):
                m.d.sync += self.dout.eq(Cat(cipoq, self.dout[:7]))
        with m.Elif(state == 0):
            m.d.sync += self.busy.eq(0)

        m.d.sync += [
            sck.eq(state[3]),
            copi.eq(self.dout[7]),
            cipoq.eq(cipo),
        ]

        return m

# A wrapper for Xilinx 7 series and CPU bus
# sck should be from usrcclk from a STARTUPE2 instance
class SPI_flash_XC7(Elaboratable):
    def __init__(self, pins, sck, pio, addr):
        self.pins = pins
        self.sck = sck
        self.pio = pio
        self.addr = addr
    def elaborate(self, platform):
        m = Module()
        flash_cs = Signal(reset=1)
        m.d.comb += self.pins.cs.o.eq(flash_cs)
        flash = m.submodules.flash = SPI_flash(
            sck=self.sck,
            copi=self.pins.copi.o,
            cipo=self.pins.cipo.i,
            cs=flash_cs,
            wv=self.pio.get_wvalid(self.addr),
            wdata=self.pio.wdata[:9],
	)
        self.pio.add_ro_reg(self.addr, flash.rdata)
        return m

# A wrapper for Lattice ECP5 and CPU bus
class SPI_flash_ECP5(Elaboratable):
    def __init__(self, pins, pio, addr):
        self.pins = pins
        self.pio = pio
        self.addr = addr
    def elaborate(self, platform):
        m = Module()
        sck = Signal()
        m.submodules.usrmclk = Instance("USRMCLK", i_USRMCLKI=sck, i_USRMCLKTS=C(0))
        flash_cs = Signal(reset=1)
        m.d.comb += self.pins.cs.o.eq(flash_cs)
        flash = m.submodules.flash = SPI_flash(
            sck=sck,
            copi=self.pins.copi.o,
            cipo=self.pins.cipo.i,
            cs=flash_cs,
            wv=self.pio.get_wvalid(self.addr),
            wdata=self.pio.wdata[:9],
	)
        self.pio.add_ro_reg(self.addr, flash.rdata)
        return m
