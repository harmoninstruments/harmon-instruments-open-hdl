# Cosine generator
# Copyright 2015 - 2020 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth import *
import numpy as np
from XC7.DSP import DSP48E1

# rom is an instance of CosROMPort ie CosROM.port[n]
# a is the angle, 26 bits, 0 = 0, 2^26 = 2*pi
# o is the output, up to 23 bits
# clk is the clock domain (optional)
# a to o is a 6 stage pipeline

class Cosine(Elaboratable):
    def __init__(self, rom, a=None, o=None, clk='sync'):
        self.rom = rom
        self.a = Signal(signed(26)) if a is None else a
        self.o = Signal(signed(26)) if o is None else o
        self.clk = clk
        self.pipedelay = 6

    def elaborate(self, platform):
        m = Module()
        a0 = Signal(24)
        s = Signal(4)
        a1 = Signal(14)
        coarse_2 = Signal(22)
        m.d[self.clk] += [
            a0.eq(Repl(self.a[24], 24) ^ self.a[:24]),
            s.eq(Cat(~self.a[25] ^ self.a[24], s[:3])),
            a1.eq(a0[:14]),
            coarse_2.eq(self.rom.o[13:35]),
        ]

        m.d.comb += self.rom.a.eq(a0[14:24])

        dsp = m.submodules.dsp = DSP48E1(
            clk = self.clk,
            AREG=2,
            BREG=2, # 2nd is bypassed by inmode[4]
            alumode = Cat(C(1), s[3], C(0, 2)),
            inmode = C(0b10100, 5),
            opmode = C(0b0110101, 7),
            a = Cat(Repl(0,10), a1, Repl(0,6)), # 4 regs to outa
            b = Cat(C(0), self.rom.o[:13], Repl(0,4)), # 3 regs to out
            c = Cat(Repl(1, 25), coarse_2, C(0)) # 2 regs to out
        )

        m.d.comb += self.o.eq(dsp.o[48-len(self.o):48])

        return m

def gen_cosrom_contents():
    abits = 10
    shift = 2
    dbits = 22

    derivbits = dbits-9
    rbits = dbits + derivbits
    npoints = 2**abits

    n = np.arange(npoints + 1)
    d = np.cos(2.0*np.pi*n/(2**(abits+shift))) * ((2**(dbits)) - 1.0)
    d = np.round(d)
    d = d.astype(int)
    deriv = d[:-1] - d[1:]
    d <<= derivbits
    d = d[:2**abits]
    d |= deriv
    return d

class CosROMPort():
    def __init__(self, clk):
        self.a = Signal(10)
        self.o = Signal(35)
        self.clk = clk

class CosROM(Elaboratable):
    def __init__(self, clk='sync'):
        self.port = [CosROMPort(clk), CosROMPort(clk)]

    def elaborate(self, platform):
        m = Module()

        init = {} # block ram initialization

        d = gen_cosrom_contents()

        for i in range(128):
            v = 0
            for j in range(8):
                v1 = int(d[8*i+j]) & 0xFFFFFFFF
                v |= v1 << (32*j)
            init["p_INIT_{:02X}".format(i)] = C(v,256)
        for i in range(16):
            v = 0
            for j in range(64):
                v1 = (int(d[64*i+j]) >> 32) & 0x7
                v |= v1 << (4*j)
            init["p_INITP_{:02X}".format(i)] = C(v,256)

        filler=Signal(2)

        m.submodules.rom = Instance(
            "RAMB36E1",
            p_DOA_REG=1,
            p_DOB_REG=1,
            p_INIT_A=C(0,36),
            p_INIT_B=C(0,36),
            p_RAM_MODE="TDP",
            p_READ_WIDTH_A=36,
            p_READ_WIDTH_B=36,
            p_WRITE_WIDTH_A=36,
            p_WRITE_WIDTH_B=36,
            p_SIM_DEVICE="7SERIES",
            **init,
            o_CASCADEOUTA=Signal(),
            o_CASCADEOUTB=Signal(),
            o_DBITERR=Signal(),
            o_ECCPARITY=Signal(8),
            o_RDADDRECC=Signal(9),
            o_SBITERR=Signal(),
            # out
            o_DOADO=self.port[0].o[:32],
            o_DOPADOP=Cat(self.port[0].o[32:],filler[0]),
            o_DOBDO=self.port[1].o[:32],
            o_DOPBDOP=Cat(self.port[1].o[32:],filler[1]),
            # unused inputs
            i_CASCADEINA=C(0),
            i_CASCADEINB=C(0),
            i_INJECTDBITERR=C(0),
            i_INJECTSBITERR=C(0),
            # port A inputs
            i_ADDRARDADDR=Cat(C(0,5), self.port[0].a, C(0)),
            i_CLKARDCLK=ClockSignal(self.port[0].clk),
            i_ENARDEN=C(1),
            i_REGCEAREGCE=C(1),
            i_RSTRAMARSTRAM=C(0),
            i_RSTREGARSTREG=C(0),
            i_WEA=C(0,4),
            i_DIADI=C(0,32),
            i_DIPADIP=C(0,4),
            # port B inputs
            i_ADDRBWRADDR=Cat(C(0,5), self.port[1].a, C(0)),
            i_CLKBWRCLK=ClockSignal(self.port[1].clk),
            i_ENBWREN=C(1),
            i_REGCEB=C(1),
            i_RSTRAMB=C(0),
            i_RSTREGB=C(0),
            i_WEBWE=C(0,8),
            i_DIBDI=C(0,32),
            i_DIPBDIP=C(0,4),
        )
        return m
