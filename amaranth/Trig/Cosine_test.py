# Copyright (C) 2014-2020 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

import os.path
import numpy as np
np.random.seed(seed=0)
from amaranth_cocotb import run
from amaranth import *

import cocotb
from cocotb.clock import Clock
from cocotb.triggers import Timer, RisingEdge, ReadOnly, Event
from cocotb.result import ReturnValue

def test_cosine():
    from Trig.Cosine import Cosine, CosROM
    m = Module()
    a = Signal(26)
    o_cos = Signal(signed(25))
    o_sin = Signal(signed(25))
    sync = m.domains.sync = ClockDomain("sync", reset_less=True)
    rom = m.submodules.rom = CosROM()
    m.submodules.cos = Cosine(rom=rom.port[0], a=a, o=o_cos)
    m.submodules.sin = Cosine(rom=rom.port[1], a=(1<<24)+ ~a, o=o_sin)
    run(m,
        'Cosine_test',
        ports=[sync.clk, a, o_cos, o_sin],
        vcd_file='dump.vcd',
        verilog_sources=[
            os.path.expandvars("$XPRIMS/RAMB36E1.v"),
            os.path.expandvars("$XPRIMS/DSP48E1.v"),
        ]
    )

pipestages = 6
count = 10000

@cocotb.coroutine
def do_angle(dut, angle, dmult, amult):
    yield RisingEdge(dut.clk)
    in_scaled = np.round(angle*amult)
    dut.a = int(in_scaled)
    yield ReadOnly() # Wait until all events have executed for this timestep
    result_re = int(dut.o_cos.value.signed_integer)*1.0/dmult
    result_im = int(dut.o_sin.value.signed_integer)*1.0/dmult
    result = result_re + 1j* result_im
    raise ReturnValue(result)

@cocotb.test()
def run_test(dut):
    "Test complex exponential generator"
    nbits_a = len(dut.a)
    nbits_d = len(dut.o_cos)
    amult = 2**nbits_a/(2.0*np.pi)
    dmult = 2**(nbits_d-1)-1.0
    print("using {} bits for angle, {} bits for output".format(
        nbits_a, nbits_d))
    dut.a = 0
    a = cocotb.fork(Clock(dut.clk, 2500).start())
    angles = np.random.random(count) * 2.0 * np.pi
    expected = np.exp(1j*angles)
    expected_mag = np.abs(expected)
    expected_angle = np.angle(expected)
    result = np.zeros(count, dtype=complex)
    for i in range(10):
        yield RisingEdge(dut.clk)
    for i in range(count+pipestages):
        if i < count:
            v = yield do_angle(dut, angles[i], dmult, amult)
        else:
            v = yield do_angle(dut, 0.0, dmult, amult)
        if i >= pipestages:
            result[i-pipestages] = v

    result_angle = np.angle(result)
    result_mag = np.abs(result)

    error_angle = result_angle - expected_angle
    error_mag = result_mag - expected_mag

    msum = np.sum(error_mag)/count
    asum = np.sum(error_angle)/count
    mrms = np.sqrt(np.sum(np.square(error_mag))/count)
    arms = np.sqrt(np.sum(np.square(error_angle))/count)

    print("maximum magnitude error =", np.max(np.abs(error_mag)))
    print("maximum magnitude error (LSB) =", np.max(np.abs(error_mag)) * dmult)
    print("maximum angle error (radian) =", np.max(np.abs(error_angle)))
    print("maximum angle error (LSB) =", np.max(np.abs(error_angle)) * amult)
    print("mean magnitude error =", msum)
    print("mean magnitude error (LSB) =", msum * dmult)
    print("mean angle error (radian) =", asum)
    print("mean angle error (LSB) =", asum * amult)
    print("RMS magnitude error =", mrms)
    print("RMS magnitude error (LSB) =", mrms * dmult)
    print("RMS angle error (radian) =", arms)
    print("RMS angle error (LSB) =", arms * amult)

    for i in range(count):
        if np.abs(error_angle[i]) > 0.1 or np.abs(error_mag[i]) > 0.1:
            print(i, angles[i], expected[i], result[i])

    if np.max(np.abs(error_angle)) > 1e-4:
        dut.log.error("FAIL")
