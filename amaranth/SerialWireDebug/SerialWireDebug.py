# Serial Wire Debug host
# Copyright (C) 2014-2019 Harmon Instruments, LLC
# License: GPLv3

from amaranth import *

class SerialWireDebug(Elaboratable):
    def __init__(self, pins, pio, addr):
        self.pins = pins
        self.pio = pio
        self.wv = Signal(name='swd_wv')
        self.rdata = Signal(32)
        self.status = Signal(4)
        pio.add_ro_reg(addr, self.rdata, lsb=4)
        pio.add_ro_reg(addr+0x40, self.status, lsb=4)
        pio.add_wvalid(addr, self.wv, lsb=5)

    def elaborate(self, platform):
        m = Module()
        m.submodules.swd = Instance(
            "swd_host",
            i_clock=ClockSignal("sync"),
            o_swdio_oe=self.pins.io.oe,
            i_swdio_i=self.pins.io.i,
            o_swdio_o=self.pins.io.o,
            o_swclk=self.pins.clk,
            i_wvalid=self.wv,
            i_wdata=Cat(self.pio.wdata[:32],self.pio.waddr[:5]),
            o_rdata=self.rdata,
            o_status=self.status,
        )

        return m
