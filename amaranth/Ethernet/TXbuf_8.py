# Copyright (C) 2020 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth import *
import sys
from AXI.Stream import AXIStream

# Transmit buffer, holds a single packet
# packet is written by uC (buffer is write only)
# TX buffer starts at 0x2
# A write of a byte count + 2 at 0x4000 starts transmission process
# busy goes high until complete

class TXbuf_8(Elaboratable):
    def __init__(self, wv, wbe, wdata, waddr):
        self.ostream = AXIStream('txstream', u=[('last', 1), ('data', 8)])
        self.args = (wv, wbe, wdata, waddr)
        self.busy = Signal()
    def elaborate(self, platform):
        (wv, wbe, wdata, waddr) = self.args
        m = Module()
        ram = Memory(width=32, depth=1024)
        wrport = m.submodules.wrport = ram.write_port(domain="sync", granularity=8)
        rdport = m.submodules.rdport = ram.read_port(domain="sync", transparent=False)
        # data into buffer
        m.d.sync += [
            wrport.en.eq(Mux(wv & (waddr[10] == 0), wbe, 0)),
            wrport.addr.eq(waddr[:10]),
            wrport.data.eq(wdata),
        ]
        # unload buffer
        bytes_remaining = Signal(11)
        ram_ptr_valid = Signal()
        ram_out_valid = Signal()
        raddr = Signal(11)
        addrlow = Signal(2)
        with m.If(wv & (waddr == 1024)):
            m.d.sync += bytes_remaining.eq(wdata[:11])
            m.d.sync += ram_ptr_valid.eq(1)
            m.d.sync += raddr.eq(2)
            m.d.sync += self.busy.eq(1)
        with m.Elif(bytes_remaining != 0):
            with m.If(self.ostream.ready | ~self.ostream.valid | ~ram_out_valid | ~ram_ptr_valid):
                m.d.sync += raddr.eq(raddr + 1)
                m.d.sync += ram_ptr_valid.eq(bytes_remaining > 3)
                m.d.sync += bytes_remaining.eq(
                    Mux(bytes_remaining == 0, 0, bytes_remaining-1))
            m.d.sync += self.ostream.last.eq(bytes_remaining == 2)
        with m.Else():
            m.d.sync += ram_ptr_valid.eq(0)
            m.d.sync += self.busy.eq(0)
        # ram read
        with m.If(self.ostream.ready | ~self.ostream.valid | ~ram_out_valid):
            m.d.sync += ram_out_valid.eq(ram_ptr_valid)

        m.d.comb += rdport.en.eq(ram_ptr_valid & (self.ostream.ready | ~self.ostream.valid | ~ram_out_valid))
        m.d.comb += rdport.addr.eq(raddr[2:])
        with m.If(rdport.en):
            m.d.sync += addrlow.eq(raddr[:2])

        # ram out to out
        with m.If(self.ostream.ready | ~self.ostream.valid):
            m.d.sync += self.ostream.data.eq(Mux(addrlow[1], Mux(addrlow[0], rdport.data[24:32], rdport.data[16:24]), Mux(addrlow[0], rdport.data[8:16], rdport.data[0:8])))
            m.d.sync += self.ostream.valid.eq(ram_out_valid)

        return m
