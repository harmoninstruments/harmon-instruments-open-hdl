# Copyright (C) 2020 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth import *
from Ethernet.RXbuf_32 import RXbuf_32

# See RXbuf_32 for docs
# this is just a wrapper expanding the input stream from 8 to 32 bits

class RXbuf_8(Elaboratable):
    def __init__(
            self,
            din,
            rvalid,
            raddr,
            wvalid,
            wdata,
            clk_in='sync',
            primitive='XC7', # or generic
            clockname_in = None, # Vivado name of input clock
    ):
        self.din = din # Record
        self.clk_in = clk_in
        self.din32 = Record([("data", 32), ("size", 2), ("last", 1), ("error", 1), ("valid", 1)])
        self.buf32 = RXbuf_32(
            din=self.din32,
            raddr=raddr,
            rvalid=rvalid,
            wvalid=wvalid,
            wdata=wdata,
            clk_in=clk_in,
            primitive=primitive,
            clockname_in = clockname_in,
        )
        self.rdata = self.buf32.rdata

    def elaborate(self, platform):
        m = Module()
        wsync = m.d[self.clk_in]
        din32 = self.din32
        m.submodules.buf32 = self.buf32
        with m.If(self.din.valid):
            for i in range(4):
                with m.If(din32.size == i):
                    wsync += din32.data[8*i:8*i+8].eq(self.din.data)
        wsync += [
            din32.valid.eq(self.din.valid & (self.din.last | (din32.size == 3))),
            din32.last.eq(self.din.valid & self.din.last),
            din32.error.eq(Mux(din32.last, 0, din32.error | (self.din.error & self.din.valid))),
            din32.size.eq(Mux(din32.last, 0, din32.size + self.din.valid))
        ]

        return m
