# Copyright (C) 2020 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth import *
from struct import unpack
import zlib

def test_TX_10GBASER():
    from amaranth.sim import Simulator, Passive
    from Ethernet.TX_10GBASER import TX_10GBASER
    idata = Signal(32)
    istart = Signal()
    ilast = Signal()
    isize = Signal(2)
    olast = Signal()
    dut = TX_10GBASER(idata=idata, istart=istart, ilast=ilast, isize=isize, olast=olast)

    packets = []
    maxpacket = bytearray(65)
    maxpacket[0] = 0x55
    for i in range(1,65):
        maxpacket[i] = i
    for i in range(12,32):
        packet = maxpacket[:i]
        crc = zlib.crc32(packet)
        packets.append((packet, crc))
    for i in range(8):
        packet = maxpacket[:12]
        crc = zlib.crc32(packet)
        packets.append((packet, crc))

    def send_packet(data, expected_crc, startpos=None):
        while startpos is not None:
            ol = yield olast
            if ol == startpos:
                break
            yield
        yield istart.eq(1)
        tail = len(data) & 0x3
        yield isize.eq(tail)
        data32 = []
        data = data + bytes(4-tail if tail != 0 else 0)
        for i in range(len(data)//4):
            d = 0
            for j in range(4):
                d |= data[4*i+j] << (8*j)
            data32.append(d)
        for word in data32[:-1]:
            yield idata.eq(word)
            yield
            yield istart.eq(0)
        yield ilast.eq(1)
        yield idata.eq(data32[-1])
        yield
        yield idata.eq(0xFFFFFFFF)
        yield ilast.eq(0)
        yield isize.eq(1)
        for i in range(7):
            yield

    data = []
    control = []

    def drive_olast():
        yield Passive()
        while True:
            yield
            yield olast.eq(~olast)

    def capture():
        yield Passive()
        for i in range(5):
            yield
        while True:
            dl = yield dut.odata
            cl = yield dut.ocontrol
            ll = yield dut.olast
            yield
            if ll == 1:
                continue
            dh = yield dut.odata
            ch = yield dut.ocontrol
            lh = yield dut.olast
            yield
            d = dl | (dh<<32)
            data.append(d)
            assert lh == 1
            assert cl == ch
            control.append(cl)
            print(cl, hex(d))
            if cl:
                db = bin((d >> 4) & 0xF)
                if db.count("1") & 1:
                    assert d&0xF != (d>>4) & 0xF
                else:
                    assert d&0xF == (d>>4) & 0xF

    def test_proc():
        for i in range(11):
            yield
        for startpos in [None, 0, 1]:
            for packet in packets:
                yield from send_packet(packet[0], packet[1], startpos=startpos)
        for i in range(16):
            yield

    sim = Simulator(dut)
    sim.add_clock(3.1e-9)
    sim.add_sync_process(test_proc)
    sim.add_sync_process(capture)
    sim.add_sync_process(drive_olast)
    with sim.write_vcd("dump.vcd"):
        sim.run()
    data = data[1:]
    control = control[1:]
    # count frames
    sofs = []
    for i in range(len(control)):
        if (control[i] == 1) and (data[i] == 0xD555555555555578):
            sofs.append((i,0))
        if (control[i] == 1) and (data[i] == 0x5555550000000033):
            sofs.append((i,4))
    expected_packets = packets * 3
    assert len(sofs) == len(expected_packets)
    rxpackets = []
    for (sof, skip) in sofs:
        ilen = 0
        pdata = bytearray()
        for i in range(sof + 1, len(data)):
            if control[i] == 1:
                assert data[i] & 0xF0 >= 0x80 # terminate must come after start
                size = 0x7 & (data[i] >> 4)
                for j in range(size):
                    pdata.append(0xFF & (data[i] >> (8*(j+1))))
                assert (data[i] >> (8 + 8*size)) == 0 # control bytes must be 0
                break
            else:
                for j in range(8):
                    pdata.append(0xFF & (data[i] >> (8*j)))
        rxpackets.append(pdata[skip:])
    # check the packets
    for i in range(len(rxpackets)):
        print("checking packet ", i)
        (expected_data, expected_crc) = expected_packets[i]
        (actual_crc,) = unpack("I", rxpackets[i][-4:])
        assert rxpackets[i][:-4] == expected_data
        assert actual_crc == expected_crc
        if rxpackets[i][:-4] != expected_data:
            print("fail data", i)
        if actual_crc != expected_crc:
            print("fail crc", i)
    # check interframe lengths
    interframe = []
    for (sof, skip) in sofs[1:]:
        idles = skip
        for i in range(0,sof)[::-1]:
            assert control[i] == 1
            if data[i] & 0x80 == 0x80: # terminate
                idles += 8 - ((data[i] >> 4) & 0x7)
                break
            assert data[i] == 0x1E
            idles += 8
        interframe.append(idles)
    print("interframe", interframe)
    dic = 0
    for gap in interframe:
        dic = dic + (12 - gap)
        assert dic < 4
        if dic < 0:
            dic = 0
