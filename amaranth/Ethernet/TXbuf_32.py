# Copyright (C) 2020 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth import *
import sys
from AXI.Stream import AXIStream
from CDC.CDC import CDC_CE_bus, CDC_pulse

# Transmit buffer, holds a single packet
# packet is written by uC (buffer is write only)
# TX buffer starts at 0x2
# A write of a byte count + 2 at 0x4000 starts transmission process
# busy goes high until complete

class TXbuf_32(Elaboratable):
    def __init__(self, wv, wbe, wdata, waddr, wrclk='sync', rdclk='sync'):
        self.ostream = AXIStream('txstream', u=[('last', 1), ('data', 32), ('size', 2)])
        self.args = (wv, wbe, wdata, waddr)
        self.busy = Signal()
        self.wrclk = wrclk
        self.rdclk = rdclk
    def elaborate(self, platform):
        (wv, wbe, wdata, waddr) = self.args
        max_input_delay = None
        if platform:
            if 'xc7' in platform.device:
                max_input_delay = 5e-9
        m = Module()
        syncw = m.d[self.wrclk]
        syncr = m.d[self.rdclk]
        ram = Memory(width=32, depth=512)
        wrport = m.submodules.wrport = ram.write_port(domain=self.wrclk, granularity=8)
        rdport = m.submodules.rdport = ram.read_port(domain=self.rdclk, transparent=False)
        # data into buffer
        syncw += [
            wrport.en.eq(Mux(wv & (waddr[10] == 0), wbe, 0)),
            wrport.addr.eq(waddr[:10]),
            wrport.data.eq(wdata),
        ]
        #
        done = Signal()
        start = Signal()
        m.d.comb += start.eq(wv & (waddr == 1024))
        with m.If(start):
            syncw += self.busy.eq(1)
        with m.Elif(done):
            syncw += self.busy.eq(0)
        # sync start command
        start_tx = Signal()
        start_del = Signal(3)
        size = Signal(11)
        running = Signal()
        m.submodules.cdc_bus = CDC_CE_bus(
            i=wdata[:11], cei=start, o=size, ceo=start_tx,
            cin=self.wrclk, cout=self.rdclk,
            max_input_delay=max_input_delay)
        # unload buffer
        syncr += start_del.eq(Cat(start_tx, start_del[:-1]))
        last_word = Signal(9)

        with m.If(start_tx):
            syncr += running.eq(1)
        with m.Elif(self.ostream.last):
            syncr += running.eq(0)
        with m.If(start_tx):
            syncr += last_word.eq(size[2:] + Mux(size[:2] == 3, 2, 1))
            syncr += rdport.addr.eq(0)
            syncr += self.ostream.size.eq(size + 2)
        with m.Elif((start_del != 0) | (self.ostream.ready & running)):
            syncr += rdport.addr.eq(rdport.addr + 1)
            syncr += self.ostream.last.eq(last_word == rdport.addr)

        m.d.comb += rdport.en.eq((self.ostream.ready & running)|(start_del != 0))

        rdata_prev = Signal(16)
        # ram out to out
        with m.If(self.ostream.ready | ~self.ostream.valid):
            syncr += rdata_prev.eq(rdport.data[16:])
            syncr += self.ostream.data.eq(Cat(rdata_prev, rdport.data[:16]))

        with m.If(start_del[-1]):
            syncr += self.ostream.valid.eq(1)
        with m.Elif(self.ostream.last):
            syncr += self.ostream.valid.eq(0)


        m.submodules.cdc_done = CDC_pulse(
            i=self.ostream.last, o=done, cin=self.rdclk, cout=self.wrclk,
            max_input_delay = max_input_delay)

        return m
