# Copyright (C) 2020 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth import *
from CDC.CDC import CDC_CE_bus
from Memory.SDP import Memory_SDP
from Memory.FIFO import FIFO
from AXI.Stream import AXIStream

# Receive buffer, holds up to 4 kiB of packets
# packet is read by uC (buffer is RO)
# Two ring buffers
#  1: Data, mapped twice so wrap around just works
#  2: Packet length, source, start pointer
# packets start with a 2 byte offset from a 32 bit boundary
# packet info FIFO at offset 0x2000/4 (1<<11)
# [:12] = start address
# [12:24] = packet length (does not include filler)
# reads as 0 if empty
# write optr as the last byte the CPU is done with
# din is an AXI stream

class RXbuf_32(Elaboratable):
    def __init__(self, din, rvalid, raddr, wvalid, wdata, clk_in='sync', primitive='XC7',
                 clockname_in = None):
        self.din = din # AXIStream or Record
        self.clk_in = clk_in
        self.rvalid = rvalid
        self.raddr = raddr
        self.rdata = Signal(32)
        self.wvalid = wvalid
        self.wdata = wdata
        self.primitive = primitive # generic or XC7
        self.clockname_in = clockname_in
    def elaborate(self, platform):
        m = Module()
        max_delay = 5e-9 if self.primitive == 'XC7' else None
        wsync = m.d[self.clk_in]
        ram = m.submodules.ram = Memory_SDP(
            wbits=32, rbits=32, wclk=self.clk_in, rclk='sync',
            primitive='RAMB36E1' if self.primitive == 'XC7' else 'generic',
            reset_out=True, reg=False)
        waddr = Signal(10, reset=0)
        optr = Signal(8)
        stopptr = Signal(8)
        m.submodules.sync_optr = CDC_CE_bus(
            i=self.wdata[4:12], cei=self.wvalid, o=optr, cout=self.clk_in,
            max_input_delay=max_delay)
        waddr_start = Signal(8, reset=0)
        waddr_start_next = Signal(8)
        packetsize = Signal(10, reset=0)
        overflow = Signal()
        error_latch = Signal()
        fifo_i = AXIStream('fifo_i', [('waddr_start', 8), ('packetsize', 12)])
        fifo = m.submodules.fifo = FIFO(
            i=fifo_i,
            iclk=self.clk_in,
            oclk='sync',
            depth=32,
            primitive='generic',
            max_delay=max_delay,
            clockname_in = self.clockname_in,
        )
        rfifo = self.raddr[11] & self.rvalid
        rdata_fifo = Signal(20)
        m.d.sync += rdata_fifo.eq(Mux(rfifo & fifo.o.valid, Cat(fifo.o.waddr_start, fifo.o.packetsize), 0))
        wsync += [
            stopptr.eq(optr-1),
        ]
        dprev = Signal(16)
        last_prev = Signal()
        m.d.comb += [
            overflow.eq(waddr[2:] == stopptr),
            waddr_start_next.eq(waddr[2:] + 1),
            self.rdata.eq(Cat(C(0,4),rdata_fifo) | ram.rdata),
            fifo.o.ready.eq(rfifo),
            ram.wdata.eq(Cat(dprev, self.din.data[0:16])),
            ram.wvalid.eq(self.din.valid | last_prev),
            ram.waddr.eq(waddr),
            ram.rvalid.eq(self.rvalid & ~self.raddr[11]),
            ram.raddr.eq(self.raddr[:10]),
        ]
        wsync += [
            fifo_i.valid.eq(self.din.last & ~self.din.error & ~overflow & ~error_latch),
            fifo_i.waddr_start.eq(waddr_start),
            fifo_i.packetsize[2:].eq(packetsize + (self.din.size == 0)),
            fifo_i.packetsize[:2].eq(self.din.size),
            last_prev.eq(self.din.valid & self.din.last),
        ]
        with m.If(self.din.valid):
            wsync += dprev.eq(self.din.data[16:])
        with m.If(last_prev):
            wsync += [
                packetsize.eq(0),
                error_latch.eq(0),
            ]
            with m.If(self.din.error | overflow | error_latch):
                wsync += waddr.eq(Cat(C(0,2), waddr_start))
            with m.Else():
                wsync += [
                    waddr_start.eq(waddr_start_next),
                    waddr.eq(Cat(C(0,2), waddr_start_next)),
                ]
        with m.Elif(self.din.valid):
            with m.If(overflow | self.din.error):
                wsync += error_latch.eq(1)
            with m.Else():
                wsync += [
                    waddr.eq(waddr + ~error_latch),
                    packetsize.eq(packetsize + 1),
                ]

        return m
