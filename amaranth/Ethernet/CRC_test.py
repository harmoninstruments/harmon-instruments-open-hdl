# Copyright 2020 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth import *
from Ethernet.CRC import *
from amaranth.sim import Simulator, Settle
import zlib, struct, random

# a known good ethernet packet with CRC
packet=b'\xff\xff\xff\xff\xff\xff\x00\xde\xad\xbe\xef\x00\x08\x06\x00\x01\x08\x00\x06\x04\x00\x01\x00\xde\xad\xbe\xef\x00\xc0\xa8\x01!\x00\x00\x00\x00\x00\x00\xc0\xa8\x01\xdb\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x13\xae{\xbd'

(samplepacketcrc,)=struct.unpack("I", packet[-4:])

# packet with negated CRC for check
packet_comp = bytearray(packet)
for i in range(4):
    packet_comp[-(i+1)] ^= 0xFF

# a simplified model of what our CRC intends to compute
def crc32_model(crc, dbits, d, poly=0xedb88320):
    for i in range(dbits):
        dbit = 1 & ((d>>i) ^ crc)
        crc = crc >> 1
        if dbit:
            crc ^= poly
    return crc

# verify the CRC on the sample packet matches zlib
def test_verify_zlib():
    zlibcrc = zlib.crc32(packet[:-4])
    assert zlibcrc == samplepacketcrc

# verify our model of next crc state generates the correct CRC for the sample packet
def test_verify_model():
    crc = 0xFFFFFFFF
    for byte in packet[:-4]:
        crc = crc32_model(crc, 8, int(byte))
    crc ^= 0xFFFFFFFF
    assert crc == samplepacketcrc

def do_crc(nb):
    din = Signal(nb)
    crc = Signal(32)
    dut = CRC(crc, din)
    sim = Simulator(dut)
    random.seed(1)
    def test_proc():
        for i in range(40):
            di = random.randint(0, 2**nb-1)
            crcinitial = random.randint(0, 2**32-1)
            yield din.eq(di)
            yield crc.eq(crcinitial)
            yield Settle()
            crc_dut = yield dut.o
            expected = crc32_model(crcinitial,nb,di)
            print("val{} = {:016x} {:08x} {:08x}".format(
                i, di, crc_dut, expected))
            assert crc_dut == expected
    sim.add_process(test_proc)
    sim.run()

def test_crc_1():
    do_crc(1)
def test_crc_8():
    do_crc(8)
def test_crc_32():
    do_crc(32)
def test_crc_65():
    do_crc(65)
