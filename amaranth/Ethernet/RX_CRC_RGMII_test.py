# Copyright (C) 2020 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth import *
from amaranth.sim import Simulator, Passive

# test packet with preamble, CRC
packet=b'UUUUUUU\xd5\xff\xff\xff\xff\xff\xff\x00\xde\xad\xbe\xef\x00\x08\x06\x00\x01\x08\x00\x06\x04\x00\x01\x00\xde\xad\xbe\xef\x00\xc0\xa8\x01!\x00\x00\x00\x00\x00\x00\xc0\xa8\x01\xdb\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x13\xae{\xbd'

def test_rx_crc():
    from Ethernet.RX_CRC_RGMII import RX_CRC_RGMII, RGMII_Status
    dut = RX_CRC_RGMII()
    def send_packet(packet, fail_crc=False, err=False):
        d = bytearray(packet)
        if fail_crc:
            d[-2] ^= 1
        for i in range(12):
            yield
        err_count = 0
        for byte in d:
            yield dut.rxd.eq(byte)
            err_count += 1
            if err_count == 13 and err:
                yield dut.rxc.eq(RGMII_Status.ERROR)
            else:
                yield dut.rxc.eq(RGMII_Status.DATA)
            yield
        yield dut.rxc.eq(RGMII_Status.IDLE)
        yield dut.rxd.eq(0)
        for i in range(12):
            yield

    rx_packets = []

    # receive the output stream
    def rx_proc():
        yield Passive()
        while True:
            data = []
            bad = False
            while True:
                yield
                ov = yield dut.o.valid
                if ov != 0:
                    o = yield dut.o.data
                    last = yield dut.o.last
                    err = yield dut.o.error
                    data.append(o)
                    if err != 0:
                        bad = True
                    if last !=0:
                        rx_packets.append((data, bad))
                        break

    def sim_proc():
        yield dut.rxd.eq(0)
        yield dut.rxc.eq(RGMII_Status.IDLE)
        yield from send_packet(packet)
        yield from send_packet(packet, fail_crc=True)
        yield from send_packet(packet, err=True)
        yield from send_packet(packet)
        assert len(rx_packets) == 4
        assert rx_packets[0][1] == False
        assert bytes(rx_packets[0][0]) == packet[8:-4]
        assert rx_packets[1][1] == True
        assert rx_packets[2][1] == True
        assert rx_packets[3][1] == False
        assert bytes(rx_packets[3][0]) == packet[8:-4]

    sim = Simulator(dut)
    sim.add_clock(8e-9)
    sim.add_sync_process(sim_proc)
    sim.add_sync_process(rx_proc)
    with sim.write_vcd("dump.vcd"):
        sim.run()
