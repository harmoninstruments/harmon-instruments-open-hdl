# Copyright (C) 2020 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth import *
import zlib

def test_CRC_Stream_128():
    from amaranth.sim import Simulator
    from Ethernet.CRC_Stream_128 import CRC_Stream_128
    idata = Signal(128)
    istart = Signal()
    ilast = Signal()
    isize = Signal(4)
    dut = CRC_Stream_128(idata=idata, istart=istart, ilast=ilast, isize=isize)

    packets = []
    maxpacket = bytearray(65)
    maxpacket[0] = 0x55
    for i in range(1,65):
        maxpacket[i] = i
    for i in range(32,65):
        packet = maxpacket[:i]
        crc = zlib.crc32(packet)
        packets.append((packet, crc))

    def send_packet(data, expected_crc):
        yield istart.eq(1)
        tail = len(data) & 0xF
        yield isize.eq(tail)
        data128 = []
        if tail != 0:
            data += bytes(16-tail)
        for i in range(len(data)//16):
            d = 0
            for j in range(16):
                d |= data[16*i+j] << (8*j)
            data128.append(d)
        for word in data128[:-1]:
            yield idata.eq(word)
            yield
            yield istart.eq(0)
        yield ilast.eq(1)
        yield idata.eq(data128[-1])
        yield
        yield idata.eq(0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)
        yield ilast.eq(0)
        yield # latency is 4 clocks
        yield
        yield
        yield
        assert (yield dut.crc) == expected_crc

    def test_proc():
        for packet in packets:
            yield from send_packet(packet[0], packet[1])
            print(hex(packet[1]), len(packet[0]))

    sim = Simulator(dut)
    sim.add_clock(3.1e-9)
    sim.add_sync_process(test_proc)
    with sim.write_vcd("dump.vcd"):
        sim.run()
