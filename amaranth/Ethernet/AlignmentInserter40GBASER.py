# Copyright 2020 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD
# IEEE 802.3 82.2.7 alignment marker inserter
# The high 32 bits of the marker are the inverse of the low 32

from amaranth import *

# 1 cycle latency
class AlignmentInserter(Elaboratable):
    def __init__(self, i, c, insert, n, ce=C(1), clk='sync', out32=False):
        self.i = i # 64 bits data
        self.c = c # 1 bits 1: control, 0: data
        self.insert = insert # 1: insert a marker, assert every 16384 cycles
        self.n = n # channel number, 0-3
        self.ce = ce
        self.od = Signal(64)
        self.oc = Signal()
        self.clk = clk
        self.out32 = out32
    def elaborate(self, platform):
        m = Module()
        bip = Signal(8)
        bip_next = Signal(8)
        markervals = [0x477690, 0xE6C4F0, 0x9B65C5, 0x3D79A2]
        markerval = C(markervals[self.n],24) # M0, M1, M2
        m.d.comb += bip_next.eq(
            Cat(
                self.od[0::8].xor(),
                self.od[1::8].xor(),
                self.od[2::8].xor(),
                self.od[3::8].xor() ^ self.oc,
                self.od[4::8].xor() ^ ~self.oc,
                self.od[5::8].xor(),
                self.od[6::8].xor(),
                self.od[7::8].xor(),
            ) ^ bip
        )
        with m.If(self.ce):
            m.d[self.clk] += [
                self.od.eq(Mux(self.insert, Cat(markerval, bip_next, ~markerval, ~bip_next), self.i)),
                self.oc.eq(Mux(self.insert, 1, self.c)),
                bip.eq(Mux(self.insert, 0, bip_next)),
            ]
        if self.out32:
            with m.Else():
                m.d[self.clk] += [
                    self.od[0:32].eq(self.od[32:64]),
                ]
        return m
