# Copyright (C) 2020 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth import *

bip = -1

def test_AlignmentInserter40GBASER():
    from amaranth.sim import Simulator, Settle
    from Ethernet.AlignmentInserter40GBASER import AlignmentInserter

    idata = Signal(64)
    ic = Signal()
    insert = Signal()
    dut = AlignmentInserter(i=idata, c=ic, insert=insert, n=2)
    bip_next_expected = Cat(
        dut.od[0::8].xor(),
        dut.od[1::8].xor(),
        dut.od[2::8].xor(),
        dut.od[3::8].xor() ^ dut.oc,
        dut.od[4::8].xor() ^ ~dut.oc,
        dut.od[5::8].xor(),
        dut.od[6::8].xor(),
        dut.od[7::8].xor(),
    )

    def marker(check = True):
        global bip
        yield insert.eq(1)
        yield
        yield insert.eq(0)
        yield Settle()
        if check:
            od_expected = 0x9B65C5 | (bip << 24)
            od_expected |= (od_expected ^ 0xFFFFFFFF) << 32
            assert (yield dut.od) == od_expected
            assert (yield dut.oc) == 1
        # reset BIP to that of just this marker
        bip = (yield bip_next_expected)

    def data(d, c):
        yield idata.eq(d)
        yield ic.eq(c)
        yield
        yield Settle()
        # append this data to BIP
        global bip
        bip ^= (yield bip_next_expected)
        assert (yield dut.od) == d
        assert (yield dut.oc) == c

    def test_proc():
        yield from marker(check = False)
        yield from data(1, 0)
        yield from data(0xDEADBEEFCAFED00D, 1)
        yield from data(3, 0)
        yield from marker(check = True)
        yield from data(0xABCDEF0123456789, 1)
        yield from data(0xCAFE, 0)
        yield from marker(check = True)
        yield from marker(check = True)

    sim = Simulator(dut)
    sim.add_clock(6.4e-9)
    sim.add_sync_process(test_proc)
    with sim.write_vcd("dump.vcd"):
        sim.run()
