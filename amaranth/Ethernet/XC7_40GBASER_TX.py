# Copyright 2020 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth import *
from Ethernet.AlignmentInserter40GBASER import *
from Ethernet.Scrambler10GBASER import *

class XC7_40GBASER_TX(Elaboratable):
    def __init__(self, idata, icontrol, clk='sync', gtx=None):
        self.idata = idata # 128 bits
        self.icontrol = icontrol # 2 bits
        self.clk = clk
        self.iready = Signal()
        self.odata = Array(Signal(32, name='odata{}'.format(a)) for a in range(4))
        self.ocontrol = Signal(4) # 1 if control
        self.oseq = Signal(6)
        self.gtx = gtx
        self.sim = False # scrambler bypass and increased alignment marker frequency
    def elaborate(self, platform):
        m = Module()

        # stage 0: sequence counter
        seq0 = Signal(7)
        aicount0 = Signal(1 if self.sim else 9)
        insert0 = Signal() # insert alignment marker
        ireadyn1 = Signal() # 1 clock early
        seq0_is_65 = Signal()
        m.d[self.clk] += [
            seq0_is_65.eq(~seq0[0] & seq0[6]),
            aicount0.eq(aicount0 + seq0_is_65),
            seq0.eq(Mux(seq0_is_65, 0, seq0 + 1)),
            insert0.eq(seq0_is_65 & (~aicount0 == 0)),
            ireadyn1.eq((seq0[1:] != 31) & ~(seq0[-1] & (~aicount0 == 0))),
            self.iready.eq(ireadyn1),
        ]

        c1 = Signal(2)
        scramble = m.submodules.scramble = Scrambler(
            i=self.idata, en=self.iready, clk=self.clk, sim_bypass = self.sim)
        with m.If(self.iready):
            m.d[self.clk] += c1.eq(self.icontrol)

        # stage 1
        seq1 = Signal(6)
        insert1 = Signal()
        insert2 = Signal()
        ce641 = Signal()

        m.d[self.clk] += [
            insert1.eq(insert0),
            ce641.eq(~seq0[6] & ~seq0[0]),
            seq1.eq(seq0[1:]),
        ]

        aligninc = Array(Signal(1, name='aligninc{}'.format(a)) for a in range(4))
        alignind = Array(Signal(64, name='alignind{}'.format(a)) for a in range(4))

        with m.If(ce641):
            m.d[self.clk] += [
                aligninc[0].eq(aligninc[2]),
                aligninc[1].eq(aligninc[3]),
                alignind[0].eq(alignind[2]),
                alignind[1].eq(alignind[3]),
            ]

        # these are valid on ce642
        m.d.comb += [
            aligninc[2].eq(c1[0]),
            aligninc[3].eq(c1[1]),
            alignind[2].eq(scramble.o[0:64]),
            alignind[3].eq(scramble.o[64:128]),
        ]

        # stage 2
        seq2 = Signal(6)
        ce642 = Signal()
        insert2 = Signal()

        m.d[self.clk] += [
            insert2.eq(insert1),
            seq2.eq(seq1),
            ce642.eq(ce641),
        ]

        for i in range(4):
            inst = m.submodules['align{}'.format(i)] = AlignmentInserter(
                i=alignind[i],
                c=aligninc[i],
                insert=insert2,
                n=i,
                ce=ce642,
                clk=self.clk,
                out32=True)
            m.d.comb += [
                self.ocontrol[i].eq(inst.oc),
                self.odata[i].eq(inst.od[:32]),
            ]
            m.d[self.clk] += self.oseq.eq(seq2)

        if self.gtx is not None:
            for i in range(4):
                m.d.comb += [
                    # gtx transmits header and data MSB first, enet is LSB, so reverse
                    self.gtx[i].txheader.eq(Cat(~self.ocontrol[i],self.ocontrol[i],C(0))),
                    self.gtx[i].txdata.eq(self.odata[i][::-1]),
                    self.gtx[i].txsequence.eq(self.oseq),
                ]

        return m
