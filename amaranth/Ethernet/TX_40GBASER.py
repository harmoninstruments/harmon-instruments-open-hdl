# Copyright (C) 2020 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth import *
from enum import Enum, unique
from Ethernet.CRC_Stream_128 import *

@unique
class BASER(Enum):
    IDLE = 0
    START = 1
    TERMINATE = 2
    DATA = 3

class TX_40GBASER(Elaboratable):
    def __init__(self, idata, istart, ilast, isize, clk='sync'):
        self.idata = idata
        self.istart = istart # assert with start of packet data
        self.ilast = ilast # assert with last of packet
        self.isize = isize # packet size modulo 16, must be valid with istart
        self.odata = Signal(128) # output is always valid
        self.ocontrol = Signal(2)
        self.clk = clk
    def elaborate(self, platform):
        m = Module()

        # 4 cycle pipe
        crc = m.submodules.crc = CRC_Stream_128(
            idata=self.idata, istart=self.istart, ilast=self.ilast, isize=self.isize, clk=self.clk)

        # stage 1
        start1 = Signal()
        last1 = Signal()
        d1 = Signal(128)
        size1 = Signal(4)
        m.d[self.clk] += [
            # stage 1
            start1.eq(self.istart),
            d1.eq(self.idata),
            size1.eq(self.isize),
            last1.eq(self.ilast),
        ]

        # stage 2
        start2 = Signal()
        last2 = Signal()
        d2 = Signal(128)
        maskbits2 = Signal(5) # 0-16
        size2 = Signal(4)
        m.d[self.clk] += [
            d2.eq(d1),
            start2.eq(start1),
            size2.eq(size1),
            last2.eq(last1),
            maskbits2.eq(Mux(last1, Mux(size1 == 0, 16, size1), Mux(last2, 0, 16))),
        ]

        # stage 3
        start3 = Signal()
        last3 = Signal()
        d3 = Signal(128)
        size3 = Signal(4)
        mask3 = Signal(16)
        terminatecmd3 = Signal(3)
        termlow3 = Signal()
        termhigh3 = Signal()
        idle3 = Signal()
        m.d[self.clk] += [
            d3.eq(d2),
            start3.eq(start2),
            size3.eq(size2),
            last3.eq(last2),
            termlow3.eq((last2 & (size2 < 4) & (size2 != 0)) |
                        (last3 & ((size3 > 11) | (size3 == 0)))),
            termhigh3.eq(last2 & (size2 < 12) & (size2 > 3)),
            idle3.eq(termlow3 | termhigh3),
            terminatecmd3.eq(size2 + 4),
        ]
        with m.Switch(maskbits2):
            for i in range(0,17):
                with m.Case(i):
                    m.d[self.clk] += mask3.eq(2**i-1)
        # stage 4
        d4 = Signal(128)
        d4_crc = Signal(128)
        size4 = Signal(2)
        low4 = Signal(BASER)
        high4 = Signal(BASER)
        crcmask4 = Signal(20)
        terminatecmd4 = Signal(7)
        terminatecmd = Signal(8)
        crc_rotated = Signal(32)
        m.d[self.clk] += [
             size4.eq(size3[:2]),
        ]

        with m.If(last3):
            m.d[self.clk] += terminatecmd4.eq(
                Cat(Repl(~terminatecmd3.xor(),4) ^ Cat(terminatecmd3, C(1)), terminatecmd3))
        m.d.comb += terminatecmd.eq(Cat(terminatecmd4,C(1)))

        for i in range(16):
            m.d[self.clk] += d4[i*8:i*8+8].eq(Mux(mask3[i], d3[i*8:i*8+8], 0))
        with m.If(last3):
            with m.Switch(size3):
                with m.Case(0):
                    m.d[self.clk] += crcmask4.eq(0xF0000)
                for i in range(1,16):
                    with m.Case(i):
                        m.d[self.clk] += crcmask4.eq(0xF << i)
        with m.Else():
            m.d[self.clk] += crcmask4.eq(crcmask4[16:])

        with m.If(start2): # the clock before we get our data
            m.d[self.clk] += [
                low4.eq(BASER.IDLE),
                high4.eq(BASER.START),
            ]
        with m.Elif(start3):
            m.d[self.clk] += [
                low4.eq(BASER.DATA),
                high4.eq(BASER.DATA),
            ]
        with m.Elif(termlow3):
            m.d[self.clk] += [
                low4.eq(BASER.TERMINATE),
                high4.eq(BASER.IDLE),
            ]
        with m.Elif(termhigh3):
            m.d[self.clk] += [
                low4.eq(BASER.DATA),
                high4.eq(BASER.TERMINATE),
            ]
        with m.Elif(idle3):
            m.d[self.clk] += [
                low4.eq(BASER.IDLE),
                high4.eq(BASER.IDLE),
            ]

        # stage 5: output data
        with m.Switch(low4):
            with m.Case(BASER.IDLE):
                m.d[self.clk] += self.odata[:64].eq(0x1E)
            with m.Case(BASER.START):
                m.d[self.clk] += self.odata[:64].eq(0xD555555555555578)
            with m.Case(BASER.TERMINATE):
                m.d[self.clk] += self.odata[:64].eq(Cat(terminatecmd, d4_crc[0:56]))
            with m.Case(BASER.DATA):
                m.d[self.clk] += self.odata[:64].eq(d4_crc[:64])
        m.d[self.clk] += self.ocontrol[0].eq(low4 != BASER.DATA)

        with m.Switch(high4):
            with m.Case(BASER.IDLE):
                m.d[self.clk] += self.odata[64:].eq(0x1E)
            with m.Case(BASER.START):
                m.d[self.clk] += self.odata[64:].eq(0xD555555555555578)
            with m.Case(BASER.TERMINATE):
                m.d[self.clk] += self.odata[64:].eq(Cat(terminatecmd, d4_crc[64:120]))
            with m.Case(BASER.DATA): # data
                m.d[self.clk] += self.odata[64:].eq(d4_crc[64:])
        m.d[self.clk] += self.ocontrol[1].eq(high4 != BASER.DATA)

        # rotate the CRC
        with m.Switch(size4):
            with m.Case(0):
                m.d.comb += crc_rotated.eq(crc.crc)
            with m.Case(1):
                m.d.comb += crc_rotated.eq(Cat(crc.crc[24:], crc.crc[:24]))
            with m.Case(2):
                m.d.comb += crc_rotated.eq(Cat(crc.crc[16:], crc.crc[:16]))
            with m.Case(3):
                m.d.comb += crc_rotated.eq(Cat(crc.crc[8:], crc.crc[:8]))

        for i in range(16):
            im = i&3
            m.d.comb += d4_crc[8*i:8*i+8].eq(
                d4[8*i:8*i+8] | Mux(crcmask4[i], crc_rotated[8*im:8*im+8], 0))

        return m

if __name__ == "__main__":
    from amaranth.cli import main
    idata = Signal(128)
    istart = Signal()
    ilast = Signal()
    isize = Signal()
    m = TX_40GBASER(idata=idata, istart=istart, ilast=ilast, isize=isize)
    main(m, ports=[idata, istart, ilast, isize])
