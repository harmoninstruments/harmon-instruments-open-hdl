# Copyright (C) 2020 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth import *
from Ethernet.CRC import CRC

# Calculate a CRC32 of a 32 bit data stream
# by negating the first 32 bits rather than initializing to all 1's
# leading zeros don't matter. The data is shifted so that the last
# CRC has 32 bits of data.
# CRC is valid 3 clocks after ilast

class CRC_Stream_32(Elaboratable):
    def __init__(self, idata, istart, ilast, isize, clk='sync'):
        self.idata = idata
        self.istart = istart # assert with start of packet data
        self.ilast = ilast # assert with last of packet
        self.isize = isize # packet size modulo 4, must be valid with istart
        self.crc = Signal(32)
        self.clk = clk
    def elaborate(self, platform):
        m = Module()
        start1 = Signal()
        last1 = Signal()
        last2 = Signal()
        d1 = Signal(56)
        d2 = Signal(32)
        size = Signal(2)
        crc_prev = Signal(32)
        mux1 = Signal(32)

        crc = m.submodules.crc_comb = CRC(crc_prev=crc_prev, i=d2)
        crcn = Signal(32)
        m.d.comb += crcn.eq(~crc.o)

        with m.If(self.istart):
            m.d[self.clk] += size.eq(self.isize - 1)

        m.d[self.clk] += [
            # stage 1: 3 bytes of previous word, current word
            start1.eq(self.istart),
            d1[:24].eq(Mux(self.istart, 0, d1[32:])),
            d1[24:].eq(Mux(self.istart, ~self.idata, self.idata)),
            last1.eq(self.ilast),

            # stage 2: shift 0, 1, 2 or 3 bytes
            d2.eq(mux1),
            last2.eq(last1),
            crc_prev.eq(Mux(start1, 0, crc.o)),
        ]

        # stage 3: output flops
        with m.If(last2):
            m.d[self.clk] += self.crc.eq(crcn)

        # stage 1 mux
        with m.Switch(size):
            with m.Case(0):
                m.d.comb += mux1.eq(d1[0:32])
            with m.Case(1):
                m.d.comb += mux1.eq(d1[8:40])
            with m.Case(2):
                m.d.comb += mux1.eq(d1[16:48])
            with m.Case(3):
                m.d.comb += mux1.eq(d1[24:56])

        return m
