# Copyright (C) 2020 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth import *

# test packet with SOF and CRC
packet_sof_crc=b'UUUUUUU\xd5\xff\xff\xff\xff\xff\xff\x00\xde\xad\xbe\xef\x00\x08\x06\x00\x01\x08\x00\x06\x04\x00\x01\x00\xde\xad\xbe\xef\x00\xc0\xa8\x01!\x00\x00\x00\x00\x00\x00\xc0\xa8\x01\xdb\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x13\xae{\xbd'

packet=packet_sof_crc[8:-4]

def test_ethernet_txbuf():
    from amaranth.sim import Simulator
    from Ethernet.TX_CRC_RGMII import TX_CRC_RGMII
    from Ethernet.TXbuf_8 import TXbuf_8

    wv = Signal()
    wbe = Signal(4)
    wdata = Signal(32)
    waddr = Signal(30)
    dut = TXbuf_8(wv=wv, wbe=wbe, wdata=wdata, waddr=waddr)
    tx = TX_CRC_RGMII(dut.ostream)
    m = Module()
    m.submodules.tx = tx
    m.submodules.dut = dut
    def write(addr, data):
        yield wv.eq(1)
        yield waddr.eq(addr)
        yield wbe.eq(0xF)
        yield wdata.eq(data)
        yield
        yield wv.eq(0)
    def test_proc():
        d = bytes(2) + packet + bytes(8)
        for i in range((len(d)-2)//4):
            yield from write(i, d[0+4*i] | (d[1+4*i] << 8) | (d[2+4*i] << 16) | (d[3+4*i] << 24))
        yield from write(1024, len(packet)+2)
        while True:
            yield
            txc = yield tx.txc
            if txc != 0:
                break
        rxdata = []
        while True:
            txc = yield tx.txc
            txd = yield tx.txd
            if txc == 0:
                break
            rxdata.append(txd)
            yield
        assert len(rxdata) == len(packet_sof_crc)
        assert bytes(rxdata) == packet_sof_crc

    sim = Simulator(m)
    sim.add_clock(8e-9)
    sim.add_sync_process(test_proc)
    with sim.write_vcd("dump.vcd"):
        sim.run()
