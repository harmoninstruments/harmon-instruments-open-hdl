# Copyright (C) 2020 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth import *
from enum import Enum, unique
from Ethernet.CRC import CRC

@unique
class RXState(Enum):
    WAIT_DATA = 0
    WAIT_SOF = 1
    WAIT_END = 2

@unique
class RGMII_Status(Enum):
    IDLE = 0
    ERROR = 1
    EXTEND = 2
    DATA = 3

class RX_CRC_RGMII(Elaboratable):
    def __init__(self):
        self.rxd = Signal(8)
        self.rxc = Signal(RGMII_Status)
        self.o = Record([('last', 1), ('data', 8), ('error', 1), ("valid", 1)])
        self.state = Signal(RXState)
        self.crc = Signal(32)
        self.link_status = Signal(4) # 1101 for 1 Gb/s full duplex
        self.link_valid = Signal()
    def elaborate(self, platform):
        m = Module()
        dv_prev = Signal()
        fcs_ok = Signal()
        error = Signal()
        dv = self.rxc != RGMII_Status.IDLE
        m.d.sync += dv_prev.eq(self.rxc != RGMII_Status.IDLE)
        crcreset = Signal()
        m.d.comb += crcreset.eq(self.state == RXState.WAIT_SOF)
        crc = m.submodules.crc = CRC(crc_prev=self.crc, i=self.rxd)

        with m.If(self.state == RXState.WAIT_DATA):
            m.d.sync += error.eq(0)
            with m.If(dv):
                m.d.sync += self.state.eq(RXState.WAIT_SOF)
        with m.Elif(self.state == RXState.WAIT_SOF):
            m.d.sync += error.eq(error | (self.rxc != RGMII_Status.DATA))
            with m.If(self.rxd == 0xD5):
                m.d.sync += self.state.eq(RXState.WAIT_END)
        with m.Elif(self.state == RXState.WAIT_END):
            m.d.sync += error.eq(error |
                                 (self.rxc == RGMII_Status.ERROR) |
                                 (self.rxc == RGMII_Status.EXTEND))
            with m.If(self.rxc == RGMII_Status.IDLE):
                m.d.sync += self.state.eq(RXState.WAIT_DATA)
        with m.If(self.rxc == RGMII_Status.IDLE):
            m.d.sync += self.link_status.eq(self.rxd[:4])
        m.d.sync += self.link_valid.eq(self.link_status == 0b1101)

        dbuf0 = Signal(8)
        dbuf1 = Signal(8)
        dbuf2 = Signal(8)
        dbuf3 = Signal(8)
        dbuf4 = Signal(8)
        ov_delay = Signal(6)
        m.d.sync += [
            fcs_ok.eq(dv & (crc.o == 0xDEBB20E3)),
            self.crc.eq(Mux(crcreset, 0xFFFFFFFF, crc.o)),
            ov_delay.eq(
                Mux(dv_prev,
                    Cat((self.state == RXState.WAIT_END) & dv, ov_delay),
                    0)),
            dbuf0.eq(self.rxd),
            dbuf1.eq(dbuf0),
            dbuf2.eq(dbuf1),
            dbuf3.eq(dbuf2),
            dbuf4.eq(dbuf3),
            self.o.data.eq(dbuf4),
            self.o.last.eq(dv_prev & ~dv),
            self.o.error.eq((dv_prev & error) | (dv_prev & ~dv & ~fcs_ok)),
        ]
        m.d.comb += self.o.valid.eq(ov_delay[-1])
        return m
