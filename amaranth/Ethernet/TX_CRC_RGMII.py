# Copyright (C) 2020 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth import *
from enum import Enum, unique
from Ethernet.CRC import CRC
@unique
class TXState(Enum):
    INTERFRAME = 0
    PREAMBLE = 1
    DATA = 2
    CRC = 3

@unique
class RGMII_Status(Enum):
    IDLE = 0
    ERROR = 1
    EXTEND = 2
    DATA = 3

# takes an AXI stream of packets
# prepends 802.3 preamble, start of frame, appends CRC
# expands packets with zeros to minimum length
# deviations from AXI stream standard:
# AXI must deliver packet with no gaps
# AXI valid must assert before ready
# output requires ODDRs
# txc[0] and txd[0:4] go on the rising edge
# txc[1] and txd[4:8] on the falling

class TX_CRC_RGMII(Elaboratable):
    def __init__(self, stream):
        self.stream = stream
        self.txc = Signal(RGMII_Status)
        self.txd = Signal(8)
    def elaborate(self, platform):
        m = Module()
        state = Signal(TXState)
        counter = Signal(4) # interframe, preamble
        crc = Signal(32)
        crc_mod = m.submodules.crc = CRC(crc_prev=crc, i=self.stream.data)
        start_preamble = (counter == 11) & self.stream.valid
        with m.Switch(state):
            with m.Case(TXState.INTERFRAME):
                m.d.sync += [
                    state.eq(Mux(start_preamble, TXState.PREAMBLE, TXState.INTERFRAME)),
                    counter.eq(Mux(start_preamble, 0, counter + (counter < 11))),
                    self.txd.eq(0),
                ]
            with m.Case(TXState.PREAMBLE):
                m.d.sync += [
                    self.txd.eq(Mux(counter<7, 0x55, 0xD5)),
                    counter.eq(counter + 1),
                    state.eq(Mux(counter == 7, TXState.DATA, TXState.PREAMBLE)),
                ]
            with m.Case(TXState.DATA):
                m.d.sync += [
                    self.txd.eq(self.stream.data),
                    counter.eq(0),
                    state.eq(Mux(self.stream.last, TXState.CRC, TXState.DATA)),
                ]
            with m.Case(TXState.CRC):
                m.d.sync += [
                    self.txd.eq(~crc[:8]),
                    counter.eq((counter + 1) & 0x3),
                    state.eq(Mux(counter == 3, TXState.INTERFRAME, TXState.CRC)),
                ]
        m.d.sync += [
            self.txc.eq(Mux(state == TXState.INTERFRAME, RGMII_Status.IDLE, RGMII_Status.DATA)),
            crc.eq(Mux(state == TXState.DATA, crc_mod.o, Cat(crc[8:],C(0xFF,8)))),
        ]
        m.d.comb += self.stream.ready.eq(state == TXState.DATA)
        return m
