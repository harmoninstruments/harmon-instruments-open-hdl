# Copyright (C) 2020 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth import *
from struct import unpack
import zlib

def test_TX_40GBASER():
    from amaranth.sim import Simulator, Passive
    from Ethernet.TX_40GBASER import TX_40GBASER
    idata = Signal(128)
    istart = Signal()
    ilast = Signal()
    isize = Signal(4)
    dut = TX_40GBASER(idata=idata, istart=istart, ilast=ilast, isize=isize)

    packets = []
    maxpacket = bytearray(65)
    maxpacket[0] = 0x55
    for i in range(1,65):
        maxpacket[i] = i
    for i in range(32,65):
        packet = maxpacket[:i]
        crc = zlib.crc32(packet)
        packets.append((packet, crc))

    def send_packet(data, expected_crc):
        yield istart.eq(1)
        tail = len(data) & 0xF
        yield isize.eq(tail)
        data128 = []
        data = data + bytes(16-tail if tail != 0 else 0)
        for i in range(len(data)//16):
            d = 0
            for j in range(16):
                d |= data[16*i+j] << (8*j)
            data128.append(d)
        for word in data128[:-1]:
            yield idata.eq(word)
            yield
            yield istart.eq(0)
        yield ilast.eq(1)
        yield idata.eq(data128[-1])
        yield
        yield idata.eq(0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)
        yield ilast.eq(0)
        yield
        yield

    data = []
    control = []

    def capture():
        yield Passive()
        while True:
            d = yield dut.odata
            c = yield dut.ocontrol
            data.append(d & ((1<<64) - 1))
            data.append(d >> 64)
            control.append(c & 1)
            control.append(c >> 1)
            yield

    def test_proc():
        for packet in packets:
            yield from send_packet(packet[0], packet[1])
        for i in range(6):
            yield

    sim = Simulator(dut)
    sim.add_clock(3.1e-9)
    sim.add_sync_process(test_proc)
    sim.add_sync_process(capture)
    with sim.write_vcd("dump.vcd"):
        sim.run()
    data = data[1:]
    control = control[1:]
    # count frames
    sofs = []
    for i in range(len(control)):
        if (control[i] == 1) and (data[i] == 0xD555555555555578):
            sofs.append(i)
    assert len(sofs) == len(packets)
    rxpackets = []
    for sof in sofs:
        ilen = 0
        pdata = bytearray()
        for i in range(sof + 1, len(data)):
            if control[i] == 1:
                assert data[i] & 0xF0 >= 0x80 # terminate must come after start
                for j in range(0x7 & (data[i] >> 4)):
                    pdata.append(0xFF & (data[i] >> (8*(j+1))))
                break
            else:
                for j in range(8):
                    pdata.append(0xFF & (data[i] >> (8*j)))
        rxpackets.append(pdata)
    # check the packets
    for i in range(len(rxpackets)):
        (expected_data, expected_crc) = packets[i]
        (actual_crc,) = unpack("I", rxpackets[i][-4:])
        assert rxpackets[i][:-4] == expected_data
        assert actual_crc == expected_crc
    # check interframe lengths
    interframe = []
    for sof in sofs[1:]:
        idles = 0
        for i in range(0,sof)[::-1]:
            assert control[i] == 1
            if data[i] & 0x80 == 0x80:
                idles += (data[i] >> 4) & 0x7
                break
            assert data[i] == 0x1E
            idles += 8
        interframe.append(idles)
    print(interframe)
    dic = 0
    for gap in interframe:
        dic = dic + (12 - gap)
        assert dic < 8
        if dic < 0:
            dic = 0
