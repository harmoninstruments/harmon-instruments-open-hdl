# Copyright 2020 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth import *
from Ethernet.Scrambler10GBASER import Scrambler
from Ethernet.TX_10GBASER import TX_10GBASER

# generate sequence for GTX, CE for gated clock
# all 4 GTX in a quad can share this part
class XC7_10GBASER_TX_Common(Elaboratable):
    def __init__(self, clk='sync'):
        self.clk = clk
        self.seq = Signal(7)
        self.ce = Signal() # tie to CE input of BUFHCE or BUFGCE
    def elaborate(self, platform):
        m = Module()
        seq0 = Signal(7)
        seq0_is_65 = Signal()
        m.d[self.clk] += [
            seq0_is_65.eq(~seq0[0] & seq0[6]),
            seq0.eq(Mux(seq0_is_65, 0, seq0 + 1)),
            self.seq.eq(seq0),
            self.ce.eq(~seq0[6]),
        ]

        return m

class XC7_10GBASER_TX(Elaboratable):
    def __init__(self, idata, istart, ilast, isize, common, clk='sync', gtx=None):
        self.idata = idata # 32 bits
        self.istart = istart
        self.ilast = ilast
        self.isize = isize
        self.clk = clk # gated
        self.odata = Signal(32)
        self.ocontrol = Signal() # 1 if control
        self.common = common # a XC7_10GBASER_TX_Common instance
        self.gtx = gtx # the target GTX instance
        self.byp = False
    def elaborate(self, platform):
        m = Module()

        txmac = m.submodules.txmac = TX_10GBASER(
            idata=self.idata,
            istart=self.istart,
            ilast=self.ilast,
            isize=self.isize,
            olast=~self.common.seq[0],
            clk=self.clk
        )

        # scrambler and matching pipe stage for control
        scramble = m.submodules.scramble = Scrambler(i=txmac.odata, clk=self.clk, sim_bypass=self.byp)
        m.d[self.clk] += [
            self.ocontrol.eq(txmac.ocontrol),
        ]

        m.d.comb += [
            self.odata.eq(scramble.o),
        ]

        if self.gtx is not None:
            m.d.comb += [
                # gtx transmits header and data MSB first, enet is LSB, so reverse
                self.gtx.txheader.eq(Cat(~self.ocontrol,self.ocontrol,C(0))),
                self.gtx.txdata.eq(Cat(self.odata[::-1],C(0,32))),
                self.gtx.txsequence.eq(Cat(self.common.seq[1:],C(0))),
            ]

        return m
