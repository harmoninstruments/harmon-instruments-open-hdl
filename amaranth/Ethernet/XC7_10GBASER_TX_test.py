# Copyright (C) 2014-2020 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

import os.path
from amaranth_cocotb import run
from amaranth import *
import cocotb
from cocotb.clock import Clock
from cocotb.triggers import Timer, RisingEdge, ReadOnly, Event
from cocotb.result import ReturnValue
from Ethernet.XC7_10GBASER_TX import *

import struct
our_mac = bytes((0x02,0xDA,0xAD,0xBE,0xEF,0x00))
our_ip = bytes((10,42,1,10))

# https://tools.ietf.org/html/rfc1071
def csum16(data):
    csum = 0
    for i in range(len(data)//2):
        csum += struct.unpack(">H",data[2*i:2*(i+1)])[0]
    while (csum >> 16) > 0:
        csum = (csum & 0xFFFF) + (csum >> 16)
    print('csum', csum)
    return csum ^ 0xFFFF

def ipv4(dest_mac, dest_ip, proto, data):
    rv = bytearray()
    rv += dest_mac + our_mac
    rv += bytes((0x08, 0x00)) # ethertype
    rv += bytes((0x45, 0x00)) # ip hdr len
    rv += struct.pack(">H", len(data) + 20) # total length
    rv += bytes((0x00, 0x00, 0x00, 0x00)) # id, frag off
    rv += bytes((255, proto)) # ttl, protocol
    rv += bytes((0x00, 0x00)) # checksum
    rv += our_ip
    rv += dest_ip
    ip_hdr = rv[14:]
    csum = csum16(ip_hdr)
    rv[14+11] = csum & 0xFF
    rv[14+10] = csum >> 8
    assert len(rv) == 34
    rv += data
    return rv

def udp(dest_mac, dest_ip, source_port, dest_port, data):
    hdr = struct.pack(">HHHH", source_port, dest_port, len(data) + 8, 0)
    return ipv4(dest_mac, dest_ip, 17, hdr+data)

pkt10 = udp(
    dest_mac = bytes((0xFF,0xFF,0xFF,0xFF,0xFF,0xFF)),
    dest_ip = bytes((10,42,0,1)),
    source_port = 40000,
    dest_port = 40002,
    data = b"hello world10\n"# + bytes(1400)
)
print(pkt10)

def test_XC7_10GBASER_TX_Common():
    m = Module()
    sync = m.domains.sync = ClockDomain("sync", reset_less=True)
    syncg = m.domains.syncg = ClockDomain("syncg", reset_less=True)
    common = m.submodules.common = XC7_10GBASER_TX_Common(clk='sync')
    m.submodules.bufce = Instance(
        "BUFHCE", i_I=ClockSignal('sync'), i_CE=common.ce, o_O=ClockSignal('syncg'))
    init = []
    for i in range((len(pkt10)+3)//4):
        b = pkt10[4*i:4*(i+1)]
        d = 0
        for a in b[::-1]:
            d <<= 8
            d |= a
        init.append(d)
    for x in init:
        print(hex(x))
    ram = Memory(width=32, depth=32, init=init)
    rdport = m.submodules.rdport = ram.read_port(domain="syncg", transparent=False)
    ra = Signal(5)
    m.d.comb += [
        rdport.en.eq(1),
        rdport.addr.eq(ra),
    ]

    start10 = Signal()
    last10 = Signal()
    size10 = Signal(2)

    tx10 = m.submodules.tx10 = XC7_10GBASER_TX(
        idata=rdport.data,
        istart=start10, ilast=last10, isize=size10,
        common=common,
        clk='syncg',
        gtx=None)
    tx10.byp = True

    m.d.syncg += [
        ra.eq(Mux(ra == 26, 0, ra+1)),#Mux(ra == 385, 0, ra + 1)),
        start10.eq(ra == 0),
        last10.eq(ra == 20),
        size10.eq(0),
    ]

    run(m,
        'Ethernet.XC7_10GBASER_TX_test',
        ports=[sync.clk, syncg.clk, tx10.odata, tx10.ocontrol, tx10.common.seq],
        vcd_file='dump.vcd',
        verilog_sources=[
            os.path.expandvars("$PWD/XC7/BUFHCE.v"),
        ],
    )

@cocotb.test()
def run_test(dut):
    cocotb.fork(Clock(dut.clk, 3000).start())
    for i in range(140):
        yield RisingEdge(dut.clk)
    """for i in range(len(a)):
        dut.a = int(a[i])
        dut.b = int(b[i] & (2**35-1))
        dut.c = int(c[i] & (2**48-1))
        yield RisingEdge(dut.clk)
        if(i > 4):
            result.append(dut.p.value.signed_integer)
            expected = a[i-4] * b [i-4] + c[i-3]
            print(a[i-4], b[i-4], c[i-3], result[-1], expected)
            assert expected == result[-1]
"""
