# Copyright (C) 2020 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth import *

def test_XC7_40GBASER_TX():
    from amaranth.sim import Simulator, Settle
    from Ethernet.XC7_40GBASER_TX import XC7_40GBASER_TX

    idata = Signal(128)
    ic = Signal(2)
    dut = XC7_40GBASER_TX(idata=idata, icontrol=ic)
    dut.sim=True

    def get():
        yield Settle()
        if (yield dut.oseq) > 31:
            yield
            yield
            yield Settle()
        od = []
        oc = []
        for i in range(4):
            od.append((yield dut.odata[i]))
            od.append(0)
            oc.append((yield dut.ocontrol[i]))
        yield
        yield Settle()
        for i in range(4):
            od[1+i*2] = (yield dut.odata[i])
            assert oc[i] == (yield dut.ocontrol[i])
        yield
        return (oc, od)

    def checker():
        for i in range(16):
            yield Settle()
            od1 = yield dut.odata[1]
            if od1 != 0:
                break
            assert i != 15 # no non zero data in 16 clocks
            yield
        for i in range(68):
            (oc,od) = yield from get()
            if i == 63:
                print ('marker', od, oc)
                for j in range(4):
                    assert od[j*2] == od[j*2+1] ^ 0xFFFFFFFF
                (oc,od) = yield from get()

            print (i, od, oc)
            for j in range(8):
                assert od[j] == j + i*8
            if i&1:
                assert oc == [0,1,1,1]
            else:
                assert oc == [0,0,1,0]

    def test_proc():
        for i in range(140):
            yield dut.idata.eq(4*i | ((4*i+1)<<32) | ((4*i+2) << 64) | ((4*i+3) << 96) )
            yield dut.icontrol.eq(i & 3)
            yield Settle()
            while not (yield dut.iready):
                yield
                yield Settle()
            yield
        return

    sim = Simulator(dut)
    sim.add_clock(3.1e-9)
    sim.add_sync_process(test_proc)
    sim.add_sync_process(checker)
    with sim.write_vcd("dump.vcd"):
        sim.run()
