# Copyright (C) 2020 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth import *
from enum import Enum, unique
from Ethernet.CRC_Stream_32 import *

@unique
class BASER(Enum):
    IDLE = 0
    START0 = 1
    START4 = 2
    TERMINATE = 3
    DATA = 4
    PREDATA = 5 # final 4 bytes of preamble followed by data

class TX_10GBASER(Elaboratable):
    def __init__(self, idata, istart, ilast, isize, olast, clk='sync'):
        self.idata = idata
        self.istart = istart # assert with start of packet data
        self.ilast = ilast # assert with last of packet
        self.isize = isize # packet size modulo 16, must be valid with istart
        self.olast = olast # high 32 bits of packet with this high
        self.odata = Signal(32) # output is always valid
        self.ocontrol = Signal()
        self.clk = clk
    def elaborate(self, platform):
        m = Module()

        # 3 cycle pipe
        crc = m.submodules.crc = CRC_Stream_32(
            idata=self.idata, istart=self.istart, ilast=self.ilast, isize=self.isize, clk=self.clk)

        low = Signal(reset=0) # processing low half of word
        lown = Signal(reset=1)
        start = Signal()
        last = Signal(5)
        d1 = Signal(32)
        d2 = Signal(32)
        d3 = Signal(32)
        d4 = Signal(32)
        size = Signal(2)
        terminate_bytes = Signal(3)
        terminatecmd = Signal(8)
        sel_crc3 = Signal(4)
        crc_rotated = Signal(32)
        cmd4 = Signal(BASER, reset=BASER.IDLE)
        terminate = Signal()
        terminatebyte = Signal(8) # terminate command or previous high byte

        with m.If(self.istart):
            m.d[self.clk] += size.eq(self.isize),
        with m.If(self.ilast):
            m.d[self.clk] += terminate_bytes.eq(Mux(size == 0, Cat(size,low),Cat(size,~low)))

        m.d[self.clk] += [
            start.eq(self.istart),
            last.eq(Cat(self.ilast,last[:-1])),
            d1.eq(self.idata),
            d2.eq(d1),
            d3.eq(Mux(last[2] | last[3] | last[4], 0, d2)),
            low.eq(self.olast),
            lown.eq(low),
            terminate.eq(Mux(size == 0, last[2]|last[3], last[1]|last[2])),
            terminatebyte.eq(Mux(low, terminatecmd, d4[24:])),
        ]

        for i in range(4):
            m.d[self.clk] += d4[i*8:i*8+8].eq(Mux(sel_crc3[i], crc_rotated[i*8:i*8+8], d3[i*8:i*8+8]))

        m.d.comb += [
            terminatecmd[0:4].eq(Mux(terminatecmd[4:7].xor(), terminatecmd[4:], ~terminatecmd[4:])),
            terminatecmd[4:7].eq(terminate_bytes),
            terminatecmd[7].eq(1),
        ]

        with m.If(low):
            with m.Switch(cmd4):
                with m.Case(BASER.TERMINATE):
                    m.d[self.clk] += cmd4.eq(BASER.IDLE)
                with m.Case(BASER.START0):
                    m.d[self.clk] += cmd4.eq(BASER.DATA)
                with m.Case(BASER.START4):
                    m.d[self.clk] += cmd4.eq(BASER.PREDATA)
                with m.Case(BASER.PREDATA):
                    m.d[self.clk] += cmd4.eq(BASER.DATA)
                with m.Case(BASER.DATA):
                    m.d[self.clk] += cmd4.eq(Mux(terminate, BASER.TERMINATE, BASER.DATA))
                with m.Case(): # BASER.IDLE
                    with m.If(self.istart):
                        m.d[self.clk] += cmd4.eq(BASER.START4)
                    with m.Elif(start):
                        m.d[self.clk] += cmd4.eq(BASER.START0)
                    with m.Else():
                        m.d[self.clk] += cmd4.eq(BASER.IDLE)

        with m.If(last[1]):
            with m.Switch(size):
                with m.Case(0):
                    m.d[self.clk] += sel_crc3.eq(0)
                with m.Case(1):
                    m.d[self.clk] += sel_crc3.eq(0xE)
                with m.Case(2):
                    m.d[self.clk] += sel_crc3.eq(0xC)
                with m.Case(3):
                    m.d[self.clk] += sel_crc3.eq(0x8)
        with m.Elif(last[2]):
            m.d[self.clk] += sel_crc3.eq(~sel_crc3)
        with m.Else():
            m.d[self.clk] += sel_crc3.eq(0)

        with m.Switch(cmd4):
            with m.Case(BASER.IDLE):
                m.d[self.clk] += self.odata.eq(Mux(lown, 0x1E, 0))
            with m.Case(BASER.START0):
                m.d[self.clk] += self.odata.eq(Mux(lown, 0x55555578, 0xD5555555))
            with m.Case(BASER.START4):
                m.d[self.clk] += self.odata.eq(Mux(lown, 0x00000033, 0x55555500))
            with m.Case(BASER.TERMINATE):
                m.d[self.clk] += self.odata.eq(Cat(terminatebyte, d4[0:24]))
            with m.Case(BASER.DATA):
                m.d[self.clk] += self.odata.eq(d4)
            with m.Case(BASER.PREDATA):
                m.d[self.clk] += self.odata.eq(Mux(lown, 0xD5555555, d4))

        #m.d[self.clk] += self.olast.eq(low)
        with m.If(lown):
            m.d[self.clk] += self.ocontrol.eq((cmd4 != BASER.DATA) & (cmd4 != BASER.PREDATA))

        with m.Switch(size):
            for i in range(4):
                with m.Case(i):
                    m.d.comb += crc_rotated.eq(crc.crc.rotate_left(8*i))
        return m
