# Copyright 2020 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth import *

# some useful CRC properties
# https://groups.google.com/forum/#!topic/comp.lang.vhdl/WHsU0PxD28E
# inverting the first N data bits and initializing to 0 is equivalent to initializing to ~0
# CRC(0000, A) == CRC(A)
# CRC(A^B) == CRC(A) ^ CRC(B)

# inverting the first 32 bits of the message, initializing to 0
# allows us any number of leading 0 bits

# generate a n bit CRC table for dbits bits of data
# CRC[n](crc_prev, x) = ^(rv[n] & (crc_prev ^ x))
# ^x is reduction xor like in Verilog
# The poly is bit reversed and this runs LSB first
# the default poly is 802.3 Ethernet FCS
def gen_table(n,dbits,poly=0xedb88320):
    rv = [0]*n
    for dbit in range(dbits):
        # rotate the crc data
        fb = (1<<dbit) ^ rv[0]
        rv[0:n-1] = rv[1:]
        rv[n-1] = 0
        # add the data bit
        for j in range(n):
            if (poly & (1<<j)) != 0:
                rv[j] ^= fb
    for i in range(n-dbits):
        rv[i] |= 1<<(i+dbits)
    return rv

# parallel combinatorial CRC for Ethernet FCS, etc
# CRC size is determined by len(crc_prev)
# data size id determined by len(i)
class CRC(Elaboratable):
    def __init__(self, crc_prev, i, poly=0xedb88320):
        self.i = i
        self.crc_prev = crc_prev
        self.o = Signal(len(crc_prev))
        self.table = gen_table(len(crc_prev), len(i), poly)
    def elaborate(self, platform):
        m = Module()
        di = Signal(max(len(self.i), len(self.crc_prev)))
        m.d.comb += di.eq(self.i ^ self.crc_prev)
        for i in range(len(self.crc_prev)):
            sigs_to_xor = []
            row = self.table[i]
            for j in range(len(di)):
                if row & (1<<j):
                    sigs_to_xor.append(di[j])
            o = Cat(sigs_to_xor).xor()
            m.d.comb += self.o[i].eq(o)
        return m
