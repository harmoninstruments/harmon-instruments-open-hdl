# Copyright (C) 2020 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth import *
from amaranth.sim import Simulator, Passive, Settle
import struct

def genpacket(size, startbyte=0x55):
    pkt = bytearray(size)
    for i in range(1,size):
        pkt[i] = i
    pkt[0] = startbyte
    return pkt

def send_packet_32(din, packet, error=False):
    yield din.valid.eq(1)
    yield din.error.eq(1 if error else 0)
    words = (len(packet) + 3)//4
    size = len(packet) % 4
    for i in range(words):
        d = packet[4*i:4*i+4]
        d += bytes(4-len(d))
        yield din.data.eq(struct.unpack("I", d)[0])
        if words == (i+1):
            yield din.last.eq(1)
            yield din.size.eq(size)
        yield
        yield din.error.eq(0)
    yield din.last.eq(0)
    yield din.valid.eq(0)
    yield din.error.eq(0)
    yield
    yield

def send_packet_8(din, packet, error=False):
    yield din.valid.eq(1)
    yield din.error.eq(1 if error else 0)
    size = len(packet)
    for i in range(size):
        yield din.data.eq(packet[i])
        if size == (i+1):
            yield din.last.eq(1)
        yield
        yield din.error.eq(0)
    yield din.last.eq(0)
    yield din.valid.eq(0)
    yield din.error.eq(0)
    yield
    yield

def test_ethernet_rxbuf_32(bits=32):
    rvalid = Signal()
    raddr = Signal(20)
    wvalid = Signal()
    wdata = Signal(32)
    if bits == 32:
        from Ethernet.RXbuf_32 import RXbuf_32
        din = Record([("data", 32), ("size", 2), ("last", 1), ("error", 1), ("valid", 1)])
        dut = RXbuf_32(din=din, rvalid=rvalid, raddr=raddr, wvalid=wvalid, wdata=wdata,
                       primitive='generic')
        def send_packet(packet, error=False):
            yield from send_packet_32(din, packet, error)
    if bits == 8:
        from Ethernet.RXbuf_8 import RXbuf_8
        din = Record([("data", 8), ("last", 1), ("error", 1), ("valid", 1)])
        dut = RXbuf_8(din=din, rvalid=rvalid, raddr=raddr, wvalid=wvalid, wdata=wdata,
                             primitive='generic')
        def send_packet(packet, error=False):
            yield from send_packet_8(din, packet, error)

    def read(addr):
        yield raddr.eq(addr)
        yield rvalid.eq(1)
        yield
        yield rvalid.eq(0)
        yield
        rv = yield dut.rdata
        print("read", hex(rv), 'at', addr)
        return rv

    def read_fifo():
        return read(1<<11)

    def set_optr(data):
        yield wdata.eq(data)
        yield wvalid.eq(1)
        yield
        yield wvalid.eq(0)

    def verify_packet(packet, offset=None):
        stat = yield from wait_packet()
        stat_offset = stat & 0xFFF
        stat_len = stat >> 12
        if offset is not None:
            assert stat_offset == offset
        else:
            offset = stat_offset
        assert stat_len == len(packet) # 8 bit version is offset by 1
        offset //= 4 # word addr here
        rv = bytearray()
        for i in range((len(packet)+5)//4): # 5 = 3 + 2 round up words, 2 bytes stuffed
            d = yield from read(i+offset)
            rv += struct.pack("I", d)
        rv = rv[2:2+len(packet)]
        assert rv == packet

    rand1 = genpacket(64, 3)
    rand2 = genpacket(64, 4)

    def wait_packet(max_cycles=200):
        while True:
            rv = yield from read_fifo()
            if rv != 0:
                return rv
            assert max_cycles > 0
            max_cycles -= 1

    def sim_proc():
        yield din.valid.eq(0)
        yield from set_optr(0xF0)
        rv = yield from read_fifo()
        assert rv == 0
        yield from send_packet(genpacket(60))
        yield from send_packet(genpacket(53), error=True)
        yield from send_packet(genpacket(59))
        yield from send_packet(rand1)
        # the next one should be dropped due to overflow
        yield from send_packet(genpacket(60))
        yield from verify_packet(genpacket(60), 0)
        yield from verify_packet(genpacket(59), 0x40)
        yield from verify_packet(rand1, 0x80)
        yield from set_optr(0x2F0)
        for i in range(10):
            yield
        for i in range(4):
            pkt = genpacket(12+i)
            yield from send_packet(pkt)
            yield from verify_packet(pkt)
        rv = yield from read_fifo()
        assert rv == 0
        yield

    sim = Simulator(dut)
    sim.add_clock(8e-9)
    sim.add_sync_process(sim_proc)
    with sim.write_vcd("dump.vcd"):
        sim.run()

def test_ethernet_rxbuf_8():
    test_ethernet_rxbuf_32(8)
