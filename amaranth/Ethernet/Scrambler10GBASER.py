# Copyright 2020 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth import *

# IEEE 802.3-2015 49.2.6 Scrambler G(x) = 1 + x^39 + x^58
# used in 10GBASE-R, 40GBASE-R, etc
class Scrambler(Elaboratable):
    def __init__(self, i, en=C(1), clk='sync', sim_bypass=False):
        self.i = i
        self.o = Signal(len(i), name="scrambler_out")
        self.en = en
        self.clk = clk
        self.sim_bypass = sim_bypass
    def elaborate(self, platform):
        m = Module()
        state = Signal(58)
        nb = len(self.i)
        onext = Signal(nb)
        concatenated = Cat(state, onext)
        m.d.comb += onext.eq(self.i ^ concatenated[0:nb] ^ concatenated[19:19+nb])
        with m.If(self.en):
            m.d[self.clk] += [
                self.o.eq(self.i if self.sim_bypass else onext),
                state.eq(concatenated[-58:]),
            ]
        return m

# IEEE 802.3-2015 49.2.11 Descrambler G(x) = 1 + x^39 + x^58
class Descrambler(Elaboratable):
    def __init__(self, i, en=C(1), clk='sync'):
        self.i = i
        self.o = Signal(len(i))
        self.en = en
        self.clk = clk
    def elaborate(self, platform):
        m = Module()
        state = Signal(58)
        nb = len(self.i)
        concatenated = Cat(state,self.i)
        with m.If(self.en):
            m.d[self.clk] += [
                self.o.eq(self.i ^ concatenated[:nb] ^ concatenated[19:19+nb]),
                state.eq(concatenated[nb:]),
            ]
        return m
