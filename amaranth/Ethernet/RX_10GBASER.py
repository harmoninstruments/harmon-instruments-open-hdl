# Copyright (C) 2020 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth import *
from enum import Enum, unique
from Ethernet.CRC import CRC

# input data stream has already been block synchronized and descrambled
# o.size is valid with o.last and o.valid, 0 = 4 bytes, 1-3 = 1-3 bytes
# o.error is valid with olast, indicates bad CRC or other error

class RX_10GBASER(Elaboratable):
    def __init__(self, idata, icontrol, ierror, ilast, clk='sync'):
        self.idata = idata # 32 bit data
        self.icontrol = icontrol # is control word
        self.ilast = ilast # high with upper 64 bits
        self.ierror = ierror # high if sync error
        orecord = [("data", 32), ("size", 2), ("last", 1), ("error", 1), ("valid", 1)]
        self.oearly = Record(orecord)
        self.o = Record(orecord)
        self.clk = clk
    def elaborate(self, platform):
        m = Module()
        sync = m.d[self.clk]
        d1 = Signal(32)
        d2 = Signal(32)
        d3 = Signal(32)
        crc_reset = Signal(reset=1)
        crc_reg = Signal(32)
        crc_invert = Signal(4)
        crci = Signal(32)
        for i in range(4):
            m.d.comb += crci[8*i:8*i+8].eq(Repl(crc_invert[i], 8) ^ d3[8*i:8*i+8])
        crc = m.submodules.crc = CRC(crc_prev = crc_reg, i=crci)
        crc_fail = Signal()
        m.d.comb += crc_fail.eq(crc.o != 0)
        is_terminate_comb = Signal()
        terminate = Signal(5)
        terminate_cmd = Signal(2)
        is_control_comb = Signal()
        error = Signal(4 )
        s0c = Signal()
        start4 = Signal() # start at byte 4 command
        start = Signal(6)
        control_low_expected = Signal(4)
        mask_d3 = Signal(4)
        last_control_was_start = Signal()

        tsize = Signal(2)
        m.d.comb += [
            is_control_comb.eq(~self.ilast & self.icontrol),
            is_terminate_comb.eq(is_control_comb & self.idata[7]),
            control_low_expected.eq(Repl(self.idata[4:8].xor(),4) ^ self.idata[0:4]),
            s0c.eq(is_control_comb & (self.idata[4:8] == 0x7)),
            self.oearly.error.eq(error[3] | (terminate[3] & crc_fail)),
        ]

        sync += [
            start4.eq(is_control_comb & ((self.idata[4:8] == 0x3)|(self.idata[4:8] == 0x6))),
            start.eq(Cat(s0c|start4, start[:-1])),
            d1.eq(self.idata),
            error[0].eq((error[0] & ~start[0]) | self.ierror |
                        (last_control_was_start & is_control_comb & ~is_terminate_comb) |
                        (is_control_comb & (self.idata[4:8] != control_low_expected))),
            error[1].eq(error[0]),
            error[2:].eq(error[1:-1]),
            crc_reg.eq(Mux(crc_reset, 0, crc.o)),
            self.oearly.last.eq(terminate[2]),
            terminate_cmd.eq(Cat(is_terminate_comb, terminate_cmd[:-1])),
            self.o.eq(self.oearly),
        ]

        with m.If(start[4]):
            sync += self.oearly.valid.eq(1)
        with m.Elif(terminate[3]):
            sync += self.oearly.valid.eq(0)
        with m.If(terminate[2]):
            sync += self.oearly.size.eq(tsize)

        with m.If(start[3]): # invert first 4 bytes
            sync += crc_invert.eq(0xF)
        with m.Else(): # invert CRC
            with m.Switch(tsize):
                with m.Case(0):
                    sync += crc_invert.eq(Mux(terminate[2], 0xF, 0))
                with m.Case(1):
                    sync += crc_invert.eq(Mux(terminate[1], 0xE, Mux(terminate[2], 1, 0)))
                with m.Case(2):
                    sync += crc_invert.eq(Mux(terminate[1], 0xC, Mux(terminate[2], 3, 0)))
                with m.Case(3):
                    sync += crc_invert.eq(Mux(terminate[1], 0x8, Mux(terminate[2], 7, 0)))

        with m.If(start[0]):
            sync += last_control_was_start.eq(1)
        with m.Elif(is_control_comb):
            sync += last_control_was_start.eq(0)

        with m.If(terminate[3]):
            sync += crc_reset.eq(1)
        with m.If(start[3]):
            sync += crc_reset.eq(0)

        with m.If(terminate[1]):
            with m.Switch(tsize):
                with m.Case(0):
                    sync += mask_d3.eq(0)
                with m.Case(1):
                    sync += mask_d3.eq(0xE)
                with m.Case(2):
                    sync += mask_d3.eq(0xC)
                with m.Case(3):
                    sync += mask_d3.eq(0x8)
        with m.Elif(terminate[2]):
            sync += mask_d3.eq(0xF)
        with m.Elif(start[2]):
            sync += mask_d3.eq(0)

        for i in range(4):
            sync += d3[i*8:i*8+8].eq(Mux(mask_d3[i], 0, d2[i*8:i*8+8]))
            sync += self.oearly.data[i*8:i*8+8].eq(Mux(mask_d3[i], 0, d3[i*8:i*8+8]))

        with m.If(is_terminate_comb):
            sync += tsize.eq(self.idata[4:6])
            with m.If(self.idata[4:7] == 0):
                sync += terminate.eq(4)
            with m.Elif(self.idata[4:7] < 5):
                sync += terminate.eq(2)
            with m.Else():
                sync += terminate.eq(1)
        with m.Else():
            sync += terminate.eq(Cat(C(0,1), terminate[:-1]))

        with m.If(terminate_cmd[0] | terminate_cmd[1]):
            sync += d2.eq(Cat(d1[8:32],self.idata[0:8]))
        with m.Else():
            sync += d2.eq(d1)

        return m
