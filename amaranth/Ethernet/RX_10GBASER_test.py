# Copyright (C) 2020 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth import *
from struct import pack, unpack
import zlib

rxpacket = bytearray()

def test_RX_10GBASER():
    from amaranth.sim import Simulator, Passive
    from Ethernet.RX_10GBASER import RX_10GBASER
    idata = Signal(32)
    icontrol = Signal()
    ierror = Signal()
    ilast = Signal()
    dut = RX_10GBASER(idata=idata, icontrol=icontrol, ilast=ilast, ierror=ierror)

    maxpacket = bytearray(33)
    maxpacket[0] = 0x55
    for i in range(1,len(maxpacket)):
        maxpacket[i] = i

    def send64(data, control = True, input_error = False):
        yield icontrol.eq(1 if control else 0)
        yield ierror.eq(1 if input_error else 0)
        yield idata.eq(data & (0xFFFFFFFF))
        yield ilast.eq(0)
        yield
        yield idata.eq(data >> 32)
        yield ilast.eq(1)
        yield

    def send_packet(data, start4, failcrc=False, error=[], input_error=[]):
        global rxpacket
        crc = zlib.crc32(data)
        if failcrc:
            crc += 1
        rdata = data + pack("I", crc)
        print(rdata)
        # send start, first of data
        if start4:
            yield from send64(0x5555550000000033, True)
            yield from send64(0xD5555555 | (unpack("I", data[:4])[0]<<32), False)
            rdata = rdata[4:]
        else:
            yield from send64(0xD555555555555578, True)
        # send data
        full_words = len(rdata)//8
        for i in range(full_words):
            yield from send64(unpack("Q", rdata[8*i:8*i+8])[0],
                              control = i in error, input_error=i in input_error)
        rdata = rdata[full_words*8:]
        # send terminate
        termcmd = (len(rdata)+8)
        parity = bin(termcmd).count('1') & 1
        termcmd = (termcmd << 4) | (termcmd)
        if parity:
            termcmd ^= 0xF
        dlast = 0x55555555555555
        for byte in rdata[::-1]:
            dlast = (dlast << 8) | byte
        dlast &= (1<<56) - 1
        yield from send64(termcmd | (dlast << 8), True)
        # send idles
        for i in range(3):
            yield from send64(0x555555555555551E, True)
        if failcrc or error or input_error:
            assert rxpacket == "error"
        else:
            assert rxpacket == data
        rxpacket = bytearray()

    def capture():
        global rxpacket
        yield Passive()
        while True:
            yield
            ov = (yield dut.o.valid)
            if ov == 0:
                continue
            od = (yield dut.o.data)
            ol = (yield dut.o.last)
            if not ol:
                rxpacket += pack("I", od)
                continue
            oe = (yield dut.o.error)
            os = (yield dut.o.size)
            if os == 0:
                rxpacket += pack("I", od)
            else:
                rxpacket += pack("I", od)[:os]
            if oe:
                rxpacket = "error"

    def test_proc():
        yield
        for i in range(12,16):
            yield from send_packet(maxpacket[:i], start4=False)
            yield from send_packet(maxpacket[:i], start4=True, failcrc=True)
            yield from send_packet(maxpacket[:i], start4=False, error=[0])
            yield from send_packet(maxpacket[:i], start4=False, error=[1])
            yield from send_packet(maxpacket[:i], start4=True)
            yield from send_packet(maxpacket[:i], start4=False, input_error=[0])
            yield from send_packet(maxpacket[:i], start4=True, input_error=[0])
            yield from send_packet(maxpacket[:i], start4=False, error=[])
            yield from send_packet(maxpacket[:i], start4=False, error=[1])
    sim = Simulator(dut)
    sim.add_clock(3.1e-9)
    sim.add_sync_process(test_proc)
    sim.add_sync_process(capture)
    with sim.write_vcd("dump.vcd"):
        sim.run()
