# Copyright (C) 2020 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth import *
from Ethernet.CRC import CRC

# Calculate a CRC32 of a 128 bit data stream
# by negating the first 32 bits rather than initializing to all 1's
# leading zeros don't matter. The data is shifted so that the last
# CRC has 128 bits of data.

class CRC_Stream_128(Elaboratable):
    def __init__(self, idata, istart, ilast, isize, clk='sync'):
        self.idata = idata
        self.istart = istart # assert with start of packet data
        self.ilast = ilast # assert with last of packet
        self.isize = isize # packet size modulo 16, must be valid with istart
        self.crc = Signal(32)
        self.clk = clk
    def elaborate(self, platform):
        m = Module()
        start1 = Signal()
        start2 = Signal()
        last1 = Signal()
        last2 = Signal()
        last3 = Signal()
        d1 = Signal(248) # 128 + 96 + 24
        d2 = Signal(152) # 128 + 24
        d3 = Signal(128)
        size = Signal(4)
        crc_prev = Signal(32)
        mux1 = Signal(152)
        mux2 = Signal(128)

        crc = m.submodules.crc = CRC(crc_prev=crc_prev, i=d3)
        crcn = Signal(32)
        m.d.comb += crcn.eq(~crc.o)

        with m.If(self.istart):
            m.d[self.clk] += size.eq(self.isize - 1)

        m.d[self.clk] += [
            # stage 1: 15 bytes of previous word, current word
            start1.eq(self.istart),
            d1[:120].eq(Mux(self.istart, 0, d1[128:248])),
            d1[120:].eq(self.idata ^ Mux(self.istart,0xFFFFFFFF, 0)),
            last1.eq(self.ilast),

            # stage 2: shift 0, 4, 8 or 12 bytes
            d2.eq(mux1),
            start2.eq(start1),
            last2.eq(last1),

            # stage 3: shift 0, 1, 2, or 3 bytes
            d3.eq(mux2),
            last3.eq(last2),
            crc_prev.eq(Mux(start2, 0, crc.o)),
        ]

        # stage 4: output flops
        with m.If(last3):
            m.d[self.clk] += self.crc.eq(crcn)

        # stage 1 mux
        with m.Switch(size[2:4]):
            with m.Case(0):
                m.d.comb += mux1.eq(d1[0:152])
            with m.Case(1):
                m.d.comb += mux1.eq(d1[32:184])
            with m.Case(2):
                m.d.comb += mux1.eq(d1[64:216])
            with m.Case(3):
                m.d.comb += mux1.eq(d1[96:248])

        # stage 2 mux
        with m.Switch(size[0:2]):
            with m.Case(0):
                m.d.comb += mux2.eq(d2[0:128])
            with m.Case(1):
                m.d.comb += mux2.eq(d2[8:136])
            with m.Case(2):
                m.d.comb += mux2.eq(d2[16:148])
            with m.Case(3):
                m.d.comb += mux2.eq(d2[24:152])
        return m

if __name__ == "__main__":
    from amaranth.cli import main
    idata = Signal(128)
    istart = Signal()
    ilast = Signal()
    isize = Signal()
    m = CRC_Stream_128(idata=idata, istart=istart, ilast=ilast, isize=isize)
    main(m, ports=[idata, istart, ilast, isize, m.crc])
