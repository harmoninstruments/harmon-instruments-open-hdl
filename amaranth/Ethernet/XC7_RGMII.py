# Copyright 2019-2020 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth import *

from XC7.clockbuffers import bufr_instance, bufio_instance
from Ethernet.RX_CRC_RGMII import RX_CRC_RGMII
from Ethernet.TX_CRC_RGMII import TX_CRC_RGMII
from Ethernet.TXbuf_8 import *
from Ethernet.RXbuf_8 import *
from XC7.idelay import idelay_fixed_inst

def iddr_inst(pin, o0, o1, clk='sync', ce=C(1)):
    return Instance(
        "IDDR",
        p_DDR_CLK_EDGE="SAME_EDGE_PIPELINED",
        p_INIT_Q1=0,
        p_INIT_Q2=0,
        p_SRTYPE="ASYNC",
        i_C=ClockSignal(clk),
        i_CE=ce,
        i_D=pin,
        o_Q1=o0,
        o_Q2=o1,
        i_R=C(0),
        i_S=C(0)
    )

class Ethernet_RGMII(Elaboratable):
    def __init__(self, n, cpu, count, rdata):
        self.n = n
        self.cpu = cpu
        self.count = count
        self.rdata = rdata
    def elaborate(self, platform):
        cpu = self.cpu
        gbeth = platform.request(
            "gbeth",
            xdr={'clock':2, 'txclk':2, 'txen':2, 'txd':2},
            dir={'rxd':'-', 'rxdv':'-'})
        m = Module()

        m.domains.gberx = ClockDomain(reset_less=True)
        m.domains.gberxio = ClockDomain(reset_less=True)
        m.submodules.bufr_gberx = bufr_instance(div=1, i=gbeth.rxclk, o="gberx")
        m.submodules.bufio_gberx = bufio_instance(i=gbeth.rxclk, o="gberxio")

        # PHY 25 MHz clock out
        cdiv5 = Signal(3)
        m.d.comb += [
            gbeth.clock.o_clk.eq(ClockSignal("sync_io")),
            gbeth.clock.o0.eq(cdiv5 > 2),
            gbeth.clock.o1.eq(cdiv5 > 1)
        ]
        m.d.sync += cdiv5.eq(Mux(cdiv5[2], 0, cdiv5 + 1))

        # reset
        enetreset=Signal(reset=1)
        with m.If(self.count[-8]):
            m.d.sync += enetreset.eq(0)
        m.d.comb += gbeth.resetn.eq(~enetreset)

        # PHY RX
        rx = m.submodules.rgmii_rx = DomainRenamer({"sync": "gberx"})(RX_CRC_RGMII())
        rxd_delay = Signal(4)
        rxctl_delay = Signal()
        idelayval = 14
        for i in range(4):
            m.submodules['idelay_rx{}'.format(i)] = idelay_fixed_inst(
                i=gbeth.rxd[i], o=rxd_delay[i], initial=idelayval)
            m.submodules['iddr_rx{}'.format(i)] = iddr_inst(
                clk='gberxio', pin=rxd_delay[i], o0=rx.rxd[i], o1=rx.rxd[i+4])
        m.submodules.idelay_rxctl = idelay_fixed_inst(i=gbeth.rxdv, o=rxctl_delay, initial=idelayval)
        m.submodules.iddr_rxdv = iddr_inst(clk='gberxio', pin=rxctl_delay, o0=rx.rxc[0], o1=rx.rxc[1])

        erxbuf = m.submodules.rxbuf = RXbuf_8(
            din=rx.o,
            rvalid=cpu.rvalid & (cpu.dbus_addr[28:32] == 0x3),
            raddr=cpu.dbus_addr[2:14],
            wvalid=cpu.wvalid & (cpu.waddr[26:30] == 0x3),
            wdata=cpu.wdata,
            clk_in='gberx',
            clockname_in='gberx_clk',
        )
        m.d.comb += self.rdata.eq(erxbuf.rdata)

        etx = m.submodules.etx = TXbuf_8(
            wv=cpu.get_wvalid(0x20000000,lsb=22),
            wbe=cpu.wmask,
            wdata=cpu.wdata,
            waddr=cpu.waddr[:11],
        )
        txmac = m.submodules.txmac = TX_CRC_RGMII(etx.ostream)
        m.d.sync += gbeth.txen.o0.eq(txmac.txc[0])
        m.d.sync += gbeth.txen.o1.eq(txmac.txc[1])
        m.d.sync += gbeth.txd.o0.eq(txmac.txd[0:4])
        m.d.sync += gbeth.txd.o1.eq(txmac.txd[4:8])
        # PHY TX clock (90 deg to data)
        m.d.comb += gbeth.txclk.o_clk.eq(ClockSignal("sync_90"))
        m.d.comb += gbeth.txclk.o0.eq(0)
        m.d.comb += gbeth.txclk.o1.eq(1)
        # PHY TX data
        m.d.comb += gbeth.txd.o_clk.eq(ClockSignal("sync_io"))
        m.d.comb += gbeth.txen.o_clk.eq(ClockSignal("sync_io"))

        cpu.add_ro_reg(0x20000000, etx.busy, lsb=14)

        return m
