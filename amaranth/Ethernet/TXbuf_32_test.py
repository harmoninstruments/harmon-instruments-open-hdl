# Copyright (C) 2020 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth import *
from struct import pack

# test packet with SOF and CRC
packet = bytearray()
for i in range(32):
    packet.append(i)
packet[0] = 0x55

def test_ethernet_txbuf_32():
    from amaranth.sim import Simulator
    from Ethernet.TX_CRC_RGMII import TX_CRC_RGMII
    from Ethernet.TXbuf_32 import TXbuf_32
    wv = Signal()
    wbe = Signal(4)
    wdata = Signal(32)
    waddr = Signal(30)
    dut = TXbuf_32(wv=wv, wbe=wbe, wdata=wdata, waddr=waddr)
    def write(addr, data):
        yield wv.eq(1)
        yield waddr.eq(addr)
        yield wbe.eq(0xF)
        yield wdata.eq(data)
        yield
        yield wv.eq(0)
    def do_packet(packet, ready_init=1):
        d = bytes(2) + packet + bytes(8)
        for i in range((len(d)-2)//4):
            yield from write(i, d[0+4*i] | (d[1+4*i] << 8) | (d[2+4*i] << 16) | (d[3+4*i] << 24))
        yield from write(1024, len(packet)+2)
        rdata = bytearray()
        for i in range(5):
            yield
        yield dut.ostream.ready.eq(ready_init)
        while True:
            yield
            valid = yield dut.ostream.valid
            if not valid:
                continue
            ready = yield dut.ostream.ready
            if ready == 0:
                yield
                yield dut.ostream.ready.eq(1)
                continue
            last = yield dut.ostream.last
            size = yield dut.ostream.size
            data = yield dut.ostream.data
            if last:
                if size == 0:
                    rdata += pack("I", data)
                else:
                    rdata += pack("I", data)[:size]
                break
            rdata += pack("I", data)
        yield
        assert len(rdata) == len(packet)
        assert bytes(rdata) == packet
    def test_proc():
        for last in [28, 29, 30, 31, 32]:
            yield from do_packet(packet[:last])
            yield from do_packet(packet[:last], ready_init=0)

    sim = Simulator(dut)
    sim.add_clock(8e-9)
    sim.add_sync_process(test_proc)
    with sim.write_vcd("dump.vcd"):
        sim.run()
