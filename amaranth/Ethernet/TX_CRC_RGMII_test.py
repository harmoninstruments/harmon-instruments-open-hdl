# Copyright (C) 2020 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

import queue
from amaranth import *
from amaranth.sim import Simulator, Passive
from Ethernet.TX_CRC_RGMII import *
from AXI.Stream import *

# test packet with SOF and CRC
packet_sof_crc=b'UUUUUUU\xd5\xff\xff\xff\xff\xff\xff\x00\xde\xad\xbe\xef\x00\x08\x06\x00\x01\x08\x00\x06\x04\x00\x01\x00\xde\xad\xbe\xef\x00\xc0\xa8\x01!\x00\x00\x00\x00\x00\x00\xc0\xa8\x01\xdb\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x13\xae{\xbd'

packet=packet_sof_crc[8:-4]
packets = 0

def test_tx_crc():

    stream = AXIStream('txdata', u=[('last', 1), ('data', 8)])
    stream.sim_queue = queue.Queue()
    dut = TX_CRC_RGMII(stream)

    def send_packet(packet):
        for byte in packet[:-1]:
            stream.sim_queue.put({'data':byte, 'last':0})
        stream.sim_queue.put({'data':packet[-1], 'last':1})

    def listener():
        global packets
        yield Passive()
        # process a packet
        while True:
            data = bytearray()
            # wait for the first byte
            interframe = 0
            while True:
                dv = yield dut.txc
                byte = yield dut.txd
                yield
                if dv == RGMII_Status.IDLE.value:
                    interframe += 1
                    if len(data) != 0:
                        packets += 1
                        break
                    continue
                data.append(byte)
            assert interframe >= 12
            assert data == packet_sof_crc

    def test_proc():
        send_packet(packet)
        send_packet(packet)
        for i in range(200):
            yield
        send_packet(packet)
        for i in range(100):
            yield
        assert packets == 3
    sim = Simulator(dut)
    sim.add_clock(8e-9)
    sim.add_sync_process(test_proc)
    sim.add_sync_process(listener)

    sim.add_sync_process(stream.sim_writer)
    with sim.write_vcd("dump.vcd"):
        sim.run()
