# Copyright 2020 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth import *
from amaranth.sim import Simulator
from random import randint
from Ethernet.Scrambler10GBASER import Scrambler, Descrambler

# a simple Python model of the 10GBASE-R scrambler
class Model():
    def __init__(self):
        self.state = 0
    def bit(self, b):
        b ^= ((self.state >> 38) ^ (self.state >> 57)) & 1
        mask = 2**58 - 1
        self.state = b | ((self.state << 1) & mask)
        return b
    def scramble(self, data, nb):
        rv = 0
        for i in range(nb):
            rv |= self.bit(1 if (data & (1<<i)) else 0) << i
        return rv

def test_scrambler():
    m = Module()
    nb = 64
    din = Signal(nb)
    scramble = m.submodules.scramble = Scrambler(din)
    descramble = m.submodules.descramble = Descrambler(scramble.o)
    sim = Simulator(m)
    sim.add_clock(4e-9)

    def test_proc():
        model = Model()
        modeled_next = 0
        di_delay1 = 0
        di_delay2 = 0
        for i in range(40):
            di = randint(0, 2**nb-1)
            modeled = modeled_next
            modeled_next = model.scramble(di, nb)
            yield din.eq(di)
            yield
            scrambled = yield scramble.o
            descrambled = yield descramble.o
            assert scrambled == modeled
            assert descrambled == di_delay2
            di_delay2 = di_delay1 # delayed to match pipeline in logic
            di_delay1 = di
        print("pass")
    sim.add_sync_process(test_proc)
    with sim.write_vcd("dump.vcd"):
        sim.run()
