# Copyright (C) 2020 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth import *
import zlib

def test_CRC_Stream_32():
    from amaranth.sim import Simulator
    from Ethernet.CRC_Stream_32 import CRC_Stream_32
    idata = Signal(32)
    istart = Signal()
    ilast = Signal()
    isize = Signal(2)
    dut = CRC_Stream_32(idata=idata, istart=istart, ilast=ilast, isize=isize)

    packets = []
    maxpacket = bytearray(9)
    maxpacket[0] = 0x55
    for i in range(1,len(maxpacket)):
        maxpacket[i] = i
    for i in range(4,9):
        packet = maxpacket[:i]
        crc = zlib.crc32(packet)
        packets.append((packet, crc))

    def send_packet(data, expected_crc):
        yield istart.eq(1)
        tail = len(data) & 0x3
        yield isize.eq(tail)
        data32 = []
        if tail != 0:
            data += bytes(4-tail)
        for i in range(len(data)//4):
            d = 0
            for j in range(4):
                d |= data[4*i+j] << (8*j)
            data32.append(d)
        for word in data32[:-1]:
            yield idata.eq(word)
            yield
            yield istart.eq(0)
        yield ilast.eq(1)
        yield idata.eq(data32[-1])
        yield
        yield ilast.eq(0)
        yield # latency is 3 clocks
        yield
        yield
        assert (yield dut.crc) == expected_crc

    def test_proc():
        for packet in packets:
            yield from send_packet(packet[0], packet[1])
            print(hex(packet[1]), len(packet[0]))

    sim = Simulator(dut)
    sim.add_clock(3.1e-9)
    sim.add_sync_process(test_proc)
    with sim.write_vcd("dump.vcd"):
        sim.run()
