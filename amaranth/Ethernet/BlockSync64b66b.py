# Copyright 2020 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD
# loosely based on Figure 49-14 in 802.3-2018

from amaranth import *
from enum import Enum, unique

@unique
class BlockSyncState(Enum):
    RESET_CNT= 0
    TEST_SH = 1
    SLIP = 2
    SLIP_WAIT = 3

class BlockSync64b66b(Elaboratable):
    def __init__(
            self,
            header_err,
            header_valid,
            slip,
            clk='sync',
    ):
        self.header_err = header_err
        self.header_valid = header_valid # from gtx.rxheadervalid
        self.slip = slip # to gtx.rxgearboxslip
        self.clk = clk
        self.lock = Signal() # status output
        self.header_fail = Signal()

    def elaborate(self, platform):
        c = self.clk
        m = Module()
        sh_cnt = Signal(7)
        sh_invalid_cnt = Signal(5)
        state = Signal(BlockSyncState)
        m.d[c] += [
            self.header_fail.eq(self.header_valid & self.header_err),
            self.slip.eq(state == BlockSyncState.SLIP),
        ]
        with m.Switch(state):
            with m.Case(BlockSyncState.RESET_CNT):
                m.d[c] += [
                    sh_cnt.eq(0),
                    sh_invalid_cnt.eq(0),
                    state.eq(BlockSyncState.TEST_SH),
                ]
            with m.Case(BlockSyncState.TEST_SH):
                with m.If(self.header_valid):
                    m.d[c] += [
                        sh_cnt.eq(sh_cnt + 1),
                        sh_invalid_cnt.eq(sh_invalid_cnt + self.header_err),
                    ]
                with m.If(sh_invalid_cnt[-1]):
                    m.d[c] += state.eq(BlockSyncState.SLIP)
                with m.Elif(sh_cnt[-1]):
                    m.d[c] += state.eq(BlockSyncState.RESET_CNT)
                    with m.If(sh_invalid_cnt == 0):
                        m.d[c] += self.lock.eq(1)
            with m.Case(BlockSyncState.SLIP):
                m.d[c] += [
                    self.lock.eq(0),
                    state.eq(BlockSyncState.SLIP_WAIT),
                    sh_cnt.eq(0),
                ]
            # wait 64 clocks after a slip operation for stabilization
            with m.Case(BlockSyncState.SLIP_WAIT):
                m.d[c] += [
                    sh_cnt.eq(sh_cnt + 1),
                    state.eq(Mux(sh_cnt[-1], BlockSyncState.RESET_CNT, BlockSyncState.SLIP_WAIT)),
                ]
        return m
