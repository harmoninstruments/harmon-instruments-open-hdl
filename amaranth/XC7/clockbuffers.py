# Copyright 2019 - 2020 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth import *

def bufr_instance(i, o, div=1):
    if div < 1 or div > 8:
        raise Error("BUFR div out of range")
    return Instance("BUFR", p_BUFR_DIVIDE="{}".format(div), i_I=i, o_O=ClockSignal(o), i_CE=C(1), i_CLR=C(0))

def bufio_instance(i, o):
    return Instance("BUFIO", i_I=i, o_O=ClockSignal(o))

def bufg_instance(i, o):
    return Instance("BUFG", i_I=i, o_O=ClockSignal(o))

def bufhce_instance(i, ce, o, loc=None):
    if loc is not None:
        return Instance("BUFHCE", i_I=i, i_CE=ce, o_O=ClockSignal(o), a_LOC=loc)
    else:
        return Instance("BUFHCE", i_I=i, i_CE=ce, o_O=ClockSignal(o))

def bufh_instance(i, o, loc=None):
    if loc is not None:
        return Instance("BUFH", i_I=i, o_O=ClockSignal(o), a_LOC=loc)
    else:
        return Instance("BUFH", i_I=i, o_O=ClockSignal(o))
