# Copyright 2015 - 2019 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth import *

# DSP48 to DSP48 cascade signals
class _DSP48_cascade():
    def __init__(self):
        self.a = Signal(signed(30))
        self.b = Signal(signed(18))
        self.carry = Signal()
        self.multsign = Signal()
        self.p = Signal(signed(48))

# control signals:
#
# inmode: A, D inputs to adder
# inmode[0] 0 selects A2 reg out, 1 selects A1 reg out
# inmode[1] 1 forces A input to the preadder to 0
# inmode[2] 0 forces the D input to the preadder to 0
# inmode[3] add/sub, 1: D-A, 0: D+A
# inmode[4] 0: B2 to multiplier

# opmode P options must have PREG=1
# opmode[4:7] Zmux 0:0 1:pcin 2:P 3:C 4:P 5:pcin>>17 6: p>>17, 7:illegal
# opmode[0:2] Xmux 0:0 1:M 2:P 3:A:B
# opmode[2:4] Ymux 0:0 1:M 2:~0 3:C

# alumode
# 0: Z+(X+Y+CIN)
# 3: Z-(X+Y+CIN)
# 1: ~Z+(X+Y+CIN)
# 2: ~(Z+(X+Y+CIN))

class DSP48E1(Elaboratable):
    def __init__(
            self,
            clk='sync',
            # parameters
            AREG=1, # 0-2
            BREG=1, # 0-2
            CREG=1, # 0-1
            DREG=1, # 0-1
            MREG=1, # 0-1
            PREG=1, # 0-1
            ADREG=1, # 0-1
            ACASCREG=1, # 0-2
            A_INPUT="DIRECT", # can be "CASCADE"
            B_INPUT="DIRECT", # can be "CASCADE"
            # input signals
            a=C(0,30),
            b=C(0,18),
            c=C(0,48),
            d=None, # 25 bit signed if provided
            cascade_in = None,
            # control
            alumode = 0,
            carryinsel = C(0,3),
            inmode = C(0,5),
            opmode = C(0,7),
            cec = C(1),
            reset_a = C(0),
            reset_b = C(0),
            reset_c = C(0),
            reset_d = C(0),
            reset_m = C(0),
            reset_p = C(0),
    ):
        # outputs
        self.overflow = Signal()
        self.patterndetect = Signal()
        self.patternbdetect = Signal()
        self.underflow = Signal()
        self.o = Signal(signed(48))
        self.carryout = Signal(4)
        self.cascade_out = _DSP48_cascade()

        self.inst = Instance(
            "DSP48E1",
            p_ACASCREG=ACASCREG,
            p_ADREG=ADREG, # 0-1
            p_A_INPUT=A_INPUT,
            p_B_INPUT=B_INPUT,
            p_ALUMODEREG=1, # 0-1
            p_AREG=AREG,
            p_BREG=BREG,
            p_CREG=CREG,
            p_DREG=DREG,
            p_MREG=MREG,
            p_PREG=PREG,
            p_USE_DPORT="FALSE" if (d is None) else "TRUE",
            # outputs
            o_OVERFLOW=self.overflow,
            o_PATTERNDETECT=self.patterndetect,
            o_PATTERNBDETECT=self.patternbdetect,
            o_UNDERFLOW=self.underflow,
            o_CARRYOUT=self.carryout,
            o_P=self.o,
            # control
            i_ALUMODE=C(alumode, 4) if isinstance(alumode, int) else alumode,
            i_CARRYINSEL=carryinsel,
            i_CLK=ClockSignal(clk),
            i_INMODE=inmode,
            i_OPMODE=opmode,
            #  signal inputs
            i_A=Cat(a, C(0,5)) if len(a) == 25 else a, # MREG + AREG + PREG regs to o
            i_B=b, # 1 + BREG + PREG regs to o
            i_C=c, # 1 + CREG regs to out
            i_CARRYIN=C(0),
            i_D=C(0,25) if d is None else d,
            # cascade ports
            o_ACOUT=self.cascade_out.a,
            o_BCOUT=self.cascade_out.b,
            o_CARRYCASCOUT=self.cascade_out.carry,
            o_MULTSIGNOUT=self.cascade_out.multsign,
            o_PCOUT=self.cascade_out.p,
            i_ACIN=C(0, 30) if cascade_in is None else cascade_in.a,
            i_BCIN=C(0, 18) if cascade_in is None else cascade_in.b,
            i_CARRYCASCIN=C(0) if cascade_in is None else cascade_in.carry,
            i_MULTSIGNIN= C(0) if cascade_in is None else cascade_in.multsign,
            i_PCIN=C(0, 48) if cascade_in is None else cascade_in.p,
            # clock enables
            i_CEA1=C(AREG != 0, 1),
            i_CEA2=C(AREG != 0, 1),
            i_CEAD=C(0) if (d is None) else C(1),
            i_CEALUMODE=C(1),
            i_CEB1=C(1),
            i_CEB2=C(1),
            i_CEC=cec,
            i_CECARRYIN=C(1),
            i_CECTRL=C(1),
            i_CED=C(0) if (d is None) else C(1),
            i_CEINMODE=C(1),
            i_CEM=C(1),
            i_CEP=C(1),
            # resets
            i_RSTA=reset_a,
            i_RSTALLCARRYIN=C(0),
            i_RSTALUMODE=C(0),
            i_RSTB=reset_b,
            i_RSTC=reset_c,
            i_RSTCTRL=C(0),
            i_RSTD=reset_d,
            i_RSTINMODE=C(0),
            i_RSTM=reset_m,
            i_RSTP=reset_p,
        )

    def elaborate(self, platform):
        m = Module()
        m.submodules.dsp = self.inst
        return m
