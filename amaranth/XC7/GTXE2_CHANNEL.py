# Copyright 2020 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

# default settings are for 10GBASE-R

from amaranth import *

class GTXE2_CHANNEL(Elaboratable):
    def __init__(
            self,
            pins,
            qpll,
            invert_tx = False,
            invert_rx = False,
            txusrclk = 'txclk',
            rxusrclk = 'rxclk0',
            drp_clock = None,
            pio = None,
            addr = 0,
    ):
        self.pins = pins #
        self.qpll = qpll
        self.invert_tx = invert_tx
        self.invert_rx = invert_rx
        self.txusrclk = txusrclk
        self.rxusrclk = rxusrclk
        self.drp_clock = drp_clock
        self.pio = pio
        self.addr = addr
        # inputs
        self.txdata = Signal(64)
        self.txheader = Signal(3)
        self.txsequence = Signal(7)
        self.rxdfelpmreset = Signal()
        self.rxgearboxslip = Signal()
        self.gtrxreset = Signal()
        self.txdiffctrl = C(8,4) # TX amplitude, txusrclk
        self.prbssel = C(0,3) # 0: off, 1: PRBS-7, 2: 15, 3: 23, 4: 31
        # outputs
        self.rxclk = Signal() #
        self.txclk = Signal() #
        self.rxdata = Signal(64)
        self.rxdatavalid = Signal()
        self.rxheader = Signal(3)
        self.rxheadervalid = Signal()
        self.rxbufstatus = Signal(3) # 0 Nominal, 1:low, 2: high, 5: underflow, 6: overflow
        self.rxresetdone = Signal()

        self.txbufstatus = Signal(2)
        self.txresetdone = Signal()
        self.rxprbserr = Signal()
        self.rxcdrlock = Signal() # reserved?
        self.rxvalid = Signal()
        # DRP
        self.drpdo = Signal(16)
        self.drpdi = Signal(16+9+1)
        self.drp_enable = Signal()
        self.drp_ready = Signal()
        self.dout = Signal(16)
        self.busy = Signal()
        if self.pio is not None:
            pio.add_ro_reg(addr, Cat(self.dout, self.busy))
            self.wdrp = pio.add_wo_reg(addr, self.drpdi)

    def elaborate(self, platform):
        m = Module()

        if self.pio is not None:
            with m.If(self.drp_ready):
                m.d[self.drp_clock] += self.dout.eq(self.drpdo)
            m.d[self.drp_clock] += [
                self.drp_enable.eq(self.wdrp),
                self.busy.eq((self.wdrp | self.busy) & ~self.drp_ready),
            ]

        m.submodules.gtxe2_i = Instance(
            "GTXE2_CHANNEL",
            p_ALIGN_COMMA_DOUBLE = "FALSE",
            p_ALIGN_COMMA_ENABLE = 0b1111111111,
            p_ALIGN_COMMA_WORD = 1,
            p_ALIGN_MCOMMA_DET = "FALSE",
            p_ALIGN_MCOMMA_VALUE = 0b1010000011,
            p_ALIGN_PCOMMA_DET = "FALSE",
            p_ALIGN_PCOMMA_VALUE = 0b0101111100,
            p_SHOW_REALIGN_COMMA = "FALSE",
            p_RXSLIDE_AUTO_WAIT = 7,
            p_RXSLIDE_MODE = "OFF",
            p_RX_SIG_VALID_DLY = 10,
            # RX 8B/10B Decoder Attributes
            p_RX_DISPERR_SEQ_MATCH = "FALSE",
            p_DEC_MCOMMA_DETECT = "FALSE",
            p_DEC_PCOMMA_DETECT = "FALSE",
            p_DEC_VALID_COMMA_ONLY = "FALSE",
            # RX Clock Correction
            p_CBCC_DATA_SOURCE_SEL = "ENCODED",
            p_CLK_COR_SEQ_2_USE = "FALSE",
            p_CLK_COR_KEEP_IDLE = "FALSE",
            p_CLK_COR_MAX_LAT = 19,
            p_CLK_COR_MIN_LAT = 15,
            p_CLK_COR_PRECEDENCE = "TRUE",
            p_CLK_COR_REPEAT_WAIT = 0,
            p_CLK_COR_SEQ_LEN = 1,
            p_CLK_COR_SEQ_1_ENABLE = 0b1111,
            p_CLK_COR_SEQ_1_1 = 0b0100000000,
            p_CLK_COR_SEQ_1_2 = 0b0000000000,
            p_CLK_COR_SEQ_1_3 = 0b0000000000,
            p_CLK_COR_SEQ_1_4 = 0b0000000000,
            p_CLK_CORRECT_USE = "FALSE",
            p_CLK_COR_SEQ_2_ENABLE = 0b1111,
            p_CLK_COR_SEQ_2_1 = 0b0100000000,
            p_CLK_COR_SEQ_2_2 = 0b0000000000,
            p_CLK_COR_SEQ_2_3 = 0b0000000000,
            p_CLK_COR_SEQ_2_4 = 0b0000000000,
            # RX Channel Bonding
            p_CHAN_BOND_KEEP_ALIGN = "FALSE",
            p_CHAN_BOND_MAX_SKEW = 1,
            p_CHAN_BOND_SEQ_LEN = 1,
            p_CHAN_BOND_SEQ_1_1 = 0b0000000000,
            p_CHAN_BOND_SEQ_1_2 = 0b0000000000,
            p_CHAN_BOND_SEQ_1_3 = 0b0000000000,
            p_CHAN_BOND_SEQ_1_4 = 0b0000000000,
            p_CHAN_BOND_SEQ_1_ENABLE = 0b1111,
            p_CHAN_BOND_SEQ_2_1 = 0b0000000000,
            p_CHAN_BOND_SEQ_2_2 = 0b0000000000,
            p_CHAN_BOND_SEQ_2_3 = 0b0000000000,
            p_CHAN_BOND_SEQ_2_4 = 0b0000000000,
            p_CHAN_BOND_SEQ_2_ENABLE = 0b1111,
            p_CHAN_BOND_SEQ_2_USE = "FALSE",
            p_FTS_DESKEW_SEQ_ENABLE = 0b1111,
            p_FTS_LANE_DESKEW_CFG = 0b1111,
            p_FTS_LANE_DESKEW_EN = "FALSE",
            # RX Margin Analysis
            p_ES_CONTROL = 0b000000,
            p_ES_ERRDET_EN = "FALSE",
            p_ES_EYE_SCAN_EN = "TRUE",
            p_ES_HORZ_OFFSET = 0,
            p_ES_PMA_CFG = 0,
            p_ES_PRESCALE = 0,
            p_ES_QUALIFIER = 0,
            p_ES_QUAL_MASK = 0,
            p_ES_SDATA_MASK = 0,
            p_ES_VERT_OFFSET = 0,
            # FPGA RX Interface Attributes
            p_RX_DATA_WIDTH = 32,
            # PMA Attributes
            p_OUTREFCLK_SEL_INV = 0b11,
            p_PMA_RSV = 0x001E7080,
            p_PMA_RSV2 = 0x2070,
            p_PMA_RSV3 = 0,
            p_PMA_RSV4 = 0,
            p_RX_BIAS_CFG = 0b000000000100,
            p_DMONITOR_CFG = 0x000A00,
            p_RX_CM_SEL = 0b11,
            p_RX_CM_TRIM = 0b010,
            p_RX_DEBUG_CFG = 0b000000000000,
            p_RX_OS_CFG = 0b0000010000000,
            p_TERM_RCAL_CFG = 0b10000,
            p_TERM_RCAL_OVRD = 0,
            p_TST_RSV = 0,
            p_RX_CLK25_DIV = 7,
            p_TX_CLK25_DIV = 7,
            p_UCODEER_CLR = 0,
            # PCI Express
            p_PCS_PCIE_EN = "FALSE",
            # PCS
            p_PCS_RSVD_ATTR = 0,
            # RX Buffer
            p_RXBUF_ADDR_MODE = "FAST",
            p_RXBUF_EIDLE_HI_CNT = 0b1000,
            p_RXBUF_EIDLE_LO_CNT = 0b0000,
            p_RXBUF_EN = "TRUE",
            p_RX_BUFFER_CFG = 0b000000,
            p_RXBUF_RESET_ON_CB_CHANGE = "TRUE",
            p_RXBUF_RESET_ON_COMMAALIGN = "FALSE",
            p_RXBUF_RESET_ON_EIDLE = "FALSE",
            p_RXBUF_RESET_ON_RATE_CHANGE = "TRUE",
            p_RXBUFRESET_TIME = 0b00001,
            p_RXBUF_THRESH_OVFLW = 61,
            p_RXBUF_THRESH_OVRD = "FALSE",
            p_RXBUF_THRESH_UNDFLW = 4,
            p_RXDLY_CFG = 0x001F,
            p_RXDLY_LCFG = 0x030,
            p_RXDLY_TAP_CFG = 0x0000,
            p_RXPH_CFG = 0x000000,
            p_RXPHDLY_CFG = 0x084020,
            p_RXPH_MONITOR_SEL = 0,
            p_RX_XCLK_SEL = "RXREC",
            p_RX_DDI_SEL = 0b000000,
            p_RX_DEFER_RESET_BUF_EN = "TRUE",
            # CDR
            p_RXCDR_CFG = C(0xb000023ff10400020, 72), # table 4-17 > 6.6 G
            p_RXCDR_FR_RESET_ON_EIDLE = 0,
            p_RXCDR_HOLD_DURING_EIDLE = 0,
            p_RXCDR_PH_RESET_ON_EIDLE = 0,
            p_RXCDR_LOCK_CFG = 0b010101,
            # RX Initialization and Reset Attributes
            p_RXCDRFREQRESET_TIME = 0b00001,
            p_RXCDRPHRESET_TIME = 0b00001,
            p_RXISCANRESET_TIME = 0b00001,
            p_RXPCSRESET_TIME = 0b00001,
            p_RXPMARESET_TIME = 0b00011,
            # RX OOB Signaling Attributes
            p_RXOOB_CFG = 0b0000110,
            # RX Gearbox Attributes
            p_RXGEARBOX_EN = "TRUE",
            p_GEARBOX_MODE = 0b001,
            # PRBS Detection Attribute
            p_RXPRBS_ERR_LOOPBACK = 0,
            # Power-Down Attributes
            p_PD_TRANS_TIME_FROM_P2 = 0x03c,
            p_PD_TRANS_TIME_NONE_P2 = 0x19,
            p_PD_TRANS_TIME_TO_P2 = 0x64,
            # RX OOB Signaling Attributes
            p_SAS_MAX_COM = 64,
            p_SAS_MIN_COM = 36,
            p_SATA_BURST_SEQ_LEN = 0b0101,
            p_SATA_BURST_VAL = 0b100,
            p_SATA_EIDLE_VAL = 0b100,
            p_SATA_MAX_BURST = 8,
            p_SATA_MAX_INIT = 21,
            p_SATA_MAX_WAKE = 7,
            p_SATA_MIN_BURST = 4,
            p_SATA_MIN_INIT = 12,
            p_SATA_MIN_WAKE = 4,
            # RX Fabric Clock Output Control Attributes
            p_TRANS_TIME_RATE = 0x0E,
            # TX Buffer Attributes
            p_TXBUF_EN = "TRUE",
            p_TXBUF_RESET_ON_RATE_CHANGE = "TRUE",
            p_TXDLY_CFG = 0x001F,
            p_TXDLY_LCFG = 0x030,
            p_TXDLY_TAP_CFG = 0x0000,
            p_TXPH_CFG = 0x0780,
            p_TXPHDLY_CFG = 0x084020,
            p_TXPH_MONITOR_SEL = 0,
            p_TX_XCLK_SEL = "TXOUT",
            # FPGA TX Interface
            p_TX_DATA_WIDTH = 32,
            # TX Configurable Driver
            p_TX_DEEMPH0 = 0b00000,
            p_TX_DEEMPH1 = 0b00000,
            p_TX_EIDLE_ASSERT_DELAY = 0b110,
            p_TX_EIDLE_DEASSERT_DELAY = 0b100,
            p_TX_LOOPBACK_DRIVE_HIZ = "FALSE",
            p_TX_MAINCURSOR_SEL = 0,
            p_TX_DRIVE_MODE = "DIRECT",
            p_TX_MARGIN_FULL_0 = 0b1001110,
            p_TX_MARGIN_FULL_1 = 0b1001001,
            p_TX_MARGIN_FULL_2 = 0b1000101,
            p_TX_MARGIN_FULL_3 = 0b1000010,
            p_TX_MARGIN_FULL_4 = 0b1000000,
            p_TX_MARGIN_LOW_0  = 0b1000110,
            p_TX_MARGIN_LOW_1  = 0b1000100,
            p_TX_MARGIN_LOW_2  = 0b1000010,
            p_TX_MARGIN_LOW_3  = 0b1000000,
            p_TX_MARGIN_LOW_4  = 0b1000000,
            # TX Gearbox
            p_TXGEARBOX_EN  = "TRUE",
            # TX Initialization and Reset
            p_TXPCSRESET_TIME = 0b00001,
            p_TXPMARESET_TIME = 0b00001,
            # TX Receiver Detection
            p_TX_RXDETECT_CFG = 0x1832,
            p_TX_RXDETECT_REF = 0b100,
            # CPLL
            p_CPLL_CFG = 0xBC07DC,
            p_CPLL_FBDIV = 4,
            p_CPLL_FBDIV_45 = 5,
            p_CPLL_INIT_CFG = 0x00001E,
            p_CPLL_LOCK_CFG = 0x01E8,
            p_CPLL_REFCLK_DIV = 1,
            p_RXOUT_DIV = 1,
            p_TXOUT_DIV = 1,
            p_SATA_CPLL_CFG = "VCO_3000MHZ",
            # RX Initialization and Reset
            p_RXDFELPMRESET_TIME = 0b0001111,
            # RX Equalizer
            p_RXLPM_HF_CFG = 0b00000011110000,
            p_RXLPM_LF_CFG = 0b00000011110000,
            p_RX_DFE_GAIN_CFG = 0x020FEA,
            p_RX_DFE_H2_CFG = 0b000000000000,
            p_RX_DFE_H3_CFG = 0b000001000000,
            p_RX_DFE_H4_CFG = 0b00011110000,
            p_RX_DFE_H5_CFG = 0b00011100000,
            p_RX_DFE_KL_CFG = 0b0000011111110,
            p_RX_DFE_LPM_CFG =0x0954,
            p_RX_DFE_LPM_HOLD_DURING_EIDLE = 0,
            p_RX_DFE_UT_CFG = 0b10001111000000000,
            p_RX_DFE_VP_CFG = 0b00011111100000011,
            # Power-Down
            p_RX_CLKMUX_PD = 1,
            p_TX_CLKMUX_PD = 1,
            # FPGA RX Interface
            p_RX_INT_DATAWIDTH = 1,
            # FPGA TX Interface
            p_TX_INT_DATAWIDTH = 1,
            # TX Configurable Driver
            p_TX_QPI_STATUS_EN = 0,
            # RX Equalizer
            p_RX_DFE_KL_CFG2 = 0x301148AC,
            p_RX_DFE_XYD_CFG = 0,
            # TX Configurable Driver
            p_TX_PREDRIVER_MODE = 0,
            # IO Pins
            o_GTXTXP = self.pins.tx.p,
            o_GTXTXN = self.pins.tx.n,
            i_GTXRXP = self.pins.rx.p,
            i_GTXRXN = self.pins.rx.n,
            # CPLL
            o_CPLLFBCLKLOST = Signal(),
            o_CPLLLOCK = Signal(),
            i_CPLLLOCKDETCLK = C(0),
            i_CPLLLOCKEN = C(1),
            i_CPLLPD = C(1),
            o_CPLLREFCLKLOST = Signal(),
            i_CPLLREFCLKSEL = C(1,3),
            i_CPLLRESET = C(0),
            i_GTRSVD = C(0,16),
            i_PCSRSVDIN = C(0,16),
            i_PCSRSVDIN2 = C(0,5),
            i_PMARSVDIN = C(0,5),
            i_PMARSVDIN2 = C(0,5),
            i_TSTIN = C(0,20),
            o_TSTOUT = Signal(10),
            # Channel
            i_CLKRSVD = C(0,4),
            # Channel - Clocking
            i_GTGREFCLK = C(0),
            i_GTNORTHREFCLK0 = C(0),
            i_GTNORTHREFCLK1 = C(0),
            i_GTREFCLK0 = C(0),
            i_GTREFCLK1 = C(0),
            i_GTSOUTHREFCLK0 = C(0),
            i_GTSOUTHREFCLK1 = C(0),
            # Channel - DRP
            i_DRPADDR = self.drpdi[16:25],
            i_DRPCLK = C(0) if self.drp_clock is None else ClockSignal(self.drp_clock),
            i_DRPDI = self.drpdi[:16],
            o_DRPDO = self.drpdo,
            i_DRPEN = self.drp_enable,
            o_DRPRDY = self.drp_ready,
            i_DRPWE = self.drpdi[25] & self.drp_enable,
            # Clocking
            o_GTREFCLKMONITOR = Signal(),
            i_QPLLCLK = self.qpll.o_clock,
            i_QPLLREFCLK = self.qpll.o_refclock,
            i_RXSYSCLKSEL = C(3,2),
            i_TXSYSCLKSEL = C(3,2),
            # Digital Monitor Ports
            o_DMONITOROUT = Signal(8),
            # FPGA TX Interface Datapath Configuration
            i_TX8B10BEN = C(0),
            # Loopback Ports
            i_LOOPBACK = C(0,3), # 0: normal, 1, near end PCS, 2 near end PMA, 4: far PMA, 6: far PCS
            # PCI Express Ports
            o_PHYSTATUS = Signal(),
            i_RXRATE = C(0,3),
            o_RXVALID = self.rxvalid, # RXUSRCLK2 domain
            # Power-Down Ports
            i_RXPD = C(0,2),
            i_TXPD = C(0,2),
            # RX 8B/10B Decoder Ports
            i_SETERRSTATUS = C(0),
            # RX Initialization and Reset Ports
            i_EYESCANRESET = C(0),
            i_RXUSERRDY = C(1), # RXUSRCLK, RXUSRCLK2 stable
            # RX Margin Analysis Ports
            o_EYESCANDATAERROR = Signal(),
            i_EYESCANMODE = C(0),
            i_EYESCANTRIGGER = C(0),
            # Receive Ports - CDR Ports
            i_RXCDRFREQRESET = C(0), # reserved, tie low
            i_RXCDRHOLD = C(0),
            o_RXCDRLOCK = self.rxcdrlock,
            i_RXCDROVRDEN = C(0),
            i_RXCDRRESET = C(0),
            i_RXCDRRESETRSV = C(0),
            # Receive Ports - Clock Correction Ports
            o_RXCLKCORCNT = Signal(2),
            # Receive Ports - FPGA RX Interface Datapath Configuration
            i_RX8B10BEN = C(0),
            # Receive Ports - FPGA RX Interface Ports
            i_RXUSRCLK = ClockSignal(self.rxusrclk),
            i_RXUSRCLK2 = ClockSignal(self.rxusrclk),
            # receive Ports - FPGA RX interface Ports
            o_RXDATA = self.rxdata, # 64 bits, data on low 32
            # PRBS
            o_RXPRBSERR = self.rxprbserr,
            i_RXPRBSSEL = self.prbssel,
            i_RXPRBSCNTRESET = C(0), # reset PRBS error counter
            i_TXPRBSFORCEERR = C(0),
            i_TXPRBSSEL = self.prbssel,
            # RX Equalizer
            i_RXDFEXYDEN = C(1),
            i_RXDFEXYDHOLD = C(0),
            i_RXDFEXYDOVRDEN = C(0),
            # 8B/10B Decoder
            o_RXDISPERR = Signal(8),
            o_RXNOTINTABLE = Signal(8),
            # RX Buffer Bypass
            i_RXBUFRESET = C(0),
            o_RXBUFSTATUS = self.rxbufstatus,
            i_RXDDIEN = C(0),
            i_RXDLYBYPASS = C(1),
            i_RXDLYEN = C(0),
            i_RXDLYOVRDEN = C(0),
            i_RXDLYSRESET = C(0),
            o_RXDLYSRESETDONE = Signal(), # RX delay soft reset done
            i_RXPHALIGN = C(0),
            o_RXPHALIGNDONE = Signal(), # RX phase align done
            i_RXPHALIGNEN = C(0),
            i_RXPHDLYPD = C(0),
            i_RXPHDLYRESET = C(0),
            o_RXPHMONITOR = Signal(5),
            i_RXPHOVRDEN = C(0),
            o_RXPHSLIPMONITOR = Signal(5),
            o_RXSTATUS = Signal(3),
            # RX Alignment
            o_RXBYTEISALIGNED = Signal(),
            o_RXBYTEREALIGN = Signal(),
            o_RXCOMMADET = Signal(),
            i_RXCOMMADETEN = C(0),
            i_RXMCOMMAALIGNEN = C(0),
            i_RXPCOMMAALIGNEN = C(0),
            # RX Channel Bonding
            o_RXCHANBONDSEQ = Signal(),
            i_RXCHBONDEN = C(0),
            i_RXCHBONDLEVEL = C(0,3),
            i_RXCHBONDMASTER = C(0),
            o_RXCHBONDO = Signal(5),
            i_RXCHBONDSLAVE = C(0),
            o_RXCHANISALIGNED = Signal(),
            o_RXCHANREALIGN = Signal(),
            # RX Equalizer
            i_RXLPMHFHOLD = C(0),
            i_RXLPMHFOVRDEN = C(0),
            i_RXLPMLFHOLD = C(0),
            i_RXDFEAGCHOLD = C(0),
            i_RXDFEAGCOVRDEN = C(0),
            i_RXDFECM1EN = C(0),
            i_RXDFELFHOLD = C(0),
            i_RXDFELFOVRDEN = C(1), # change?
            i_RXDFELPMRESET = C(0),
            i_RXDFETAP2HOLD = C(0),
            i_RXDFETAP2OVRDEN = C(0),
            i_RXDFETAP3HOLD = C(0),
            i_RXDFETAP3OVRDEN = C(0),
            i_RXDFETAP4HOLD = C(0),
            i_RXDFETAP4OVRDEN = C(0),
            i_RXDFETAP5HOLD = C(0),
            i_RXDFETAP5OVRDEN = C(0),
            i_RXDFEUTHOLD = C(0),
            i_RXDFEUTOVRDEN = C(0),
            i_RXDFEVPHOLD = C(0),
            i_RXDFEVPOVRDEN = C(0),
            i_RXDFEVSEN = C(0),
            i_RXLPMLFKLOVRDEN = C(0),
            o_RXMONITOROUT = Signal(7),
            i_RXMONITORSEL = C(1,2),
            i_RXOSHOLD = C(0),
            i_RXOSOVRDEN = C(0),
            # RX Fabric ClocK Output Control
            o_RXRATEDONE = Signal(),
            o_RXOUTCLK = self.rxclk,
            o_RXOUTCLKFABRIC = Signal(),
            o_RXOUTCLKPCS = Signal(),
            i_RXOUTCLKSEL = C(2,3),
            # RX Gearbox
            o_RXDATAVALID = self.rxdatavalid,
            o_RXHEADER = self.rxheader,
            o_RXHEADERVALID = self.rxheadervalid,
            o_RXSTARTOFSEQ = Signal(),
            i_RXGEARBOXSLIP = self.rxgearboxslip,
            i_RXSLIDE = C(0), # CAUI only
            # RX Initialization and Reset
            i_GTRXRESET = self.gtrxreset,
            i_RXOOBRESET = C(0),
            i_RXPCSRESET = C(0),
            i_RXPMARESET = C(0),
            o_RXRESETDONE = self.rxresetdone,
            i_RXLPMEN = C(0), # 0: DFE, 1:LPM
            # RX OOB Signaling
            o_RXCOMSASDET = Signal(),
            o_RXCOMWAKEDET = Signal(),
            o_RXCOMINITDET = Signal(),
            o_RXELECIDLE = Signal(),
            i_RXELECIDLEMODE = C(3,2), # disabled
            i_RXPOLARITY = C(1 if self.invert_rx else 0),
            o_RXCHARISCOMMA = Signal(8), # 8b10b
            o_RXCHARISK = Signal(8),
            i_RXCHBONDI = C(0,5), # Channel bonding
            i_RXQPIEN = C(0),
            o_RXQPISENN = Signal(),
            o_RXQPISENP = Signal(),
            # TX Configurable Driver
            i_TXPOSTCURSOR = C(0,5),
            i_TXPOSTCURSORINV = C(0),
            i_TXPRECURSOR = C(0,5),
            i_TXPRECURSORINV = C(0),
            i_TXQPIBIASEN = C(0),
            i_TXQPISTRONGPDOWN = C(0),
            i_TXQPIWEAKPUP = C(0),
            # TX Init and Reset
            i_CFGRESET = C(0),
            i_GTTXRESET = self.qpll.tx_reset,
            o_PCSRSVDOUT = Signal(16),
            i_TXUSERRDY = C(1), # not using MMCM
            # Transceiver Reset Mode Operation
            i_GTRESETSEL = C(0), # 0 for sequential
            i_RESETOVRD = C(0), # reserved, tie to GND
            # 8b10b
            i_TXCHARDISPMODE = C(0,8),
            i_TXCHARDISPVAL = C(0,8),
            i_TXUSRCLK = ClockSignal(self.txusrclk),
            i_TXUSRCLK2 = ClockSignal(self.txusrclk),
            # PCI Express
            i_TXELECIDLE = C(0),
            i_TXMARGIN = C(0,3),
            i_TXRATE = C(0,3),
            i_TXSWING = C(0),
            # TX Buffer Bypass
            i_TXPHDLYTSTCLK = C(0),
            i_TXDLYBYPASS = C(1),
            i_TXDLYEN = C(0),
            i_TXDLYHOLD = C(0),
            i_TXDLYOVRDEN = C(0),
            i_TXDLYSRESET = C(0),
            o_TXDLYSRESETDONE = Signal(),
            i_TXDLYUPDOWN = C(0),
            i_TXPHALIGN = C(0),
            o_TXPHALIGNDONE = Signal(),
            i_TXPHALIGNEN = C(0),
            i_TXPHDLYPD = C(0),
            i_TXPHDLYRESET = C(0),
            i_TXPHINIT = C(0),
            o_TXPHINITDONE = Signal(),
            i_TXPHOVRDEN = C(0),
            # TX Buffer
            o_TXBUFSTATUS = self.txbufstatus,
            # Configurable Driver
            i_TXBUFDIFFCTRL = C(4,3),
            i_TXDEEMPH = C(0),
            i_TXDIFFCTRL = self.txdiffctrl,
            i_TXDIFFPD = C(0),
            i_TXINHIBIT = C(0),
            i_TXMAINCURSOR = C(80,7),
            i_TXPISOPD = C(0),
            o_TXQPISENN = Signal(),
            o_TXQPISENP = Signal(),
            # TX Data
            i_TXDATA = self.txdata,
            i_TXCHARISK = C(0,8),
            o_TXGEARBOXREADY = Signal(),
            i_TXHEADER = self.txheader,
            i_TXSEQUENCE = self.txsequence,
            i_TXSTARTSEQ = C(0),
            # TX Clock
            o_TXOUTCLK = self.txclk,
            o_TXOUTCLKFABRIC = Signal(),
            o_TXOUTCLKPCS = Signal(),
            i_TXOUTCLKSEL = C(2,3),
            o_TXRATEDONE = Signal(),
            # TX Initialization and Reset
            i_TXPCSRESET = C(0),
            i_TXPMARESET = C(0),
            o_TXRESETDONE = self.txresetdone,
            # TX OOB signalling
            o_TXCOMFINISH = Signal(),
            i_TXCOMINIT = C(0),
            i_TXCOMSAS = C(0),
            i_TXCOMWAKE = C(0),
            i_TXPDELECIDLEMODE = C(0),
            # TX Polarity Control
            i_TXPOLARITY = C(1 if self.invert_tx else 0),
            # TX Receiver Detection
            i_TXDETECTRX = C(0),
            # TX 8b/10b Encoder
            i_TX8B10BBYPASS = C(0,8),
        )
        return m
