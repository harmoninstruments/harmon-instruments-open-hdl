# Copyright (C) 2021 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

import sys
import time
from amaranth import *
from enum import Enum, unique

from PIO.Generic import PIO_Generic
from AXI.Stream import AXIStream

class Async_RX_PHY(Elaboratable):
    def __init__(
            self, pins, reset,
            clk='c500', # fast clock, 0.5 * data rate, BUFIO or BUFG
            cdiv='c250r', # slow clock, 0.25 * data rate, BUFR if clk on BUFIO else BUFG
            initial_taps = [0,0],
            invert=False,
    ):
        self.pins = pins
        self.reset = reset
        self.clk = clk
        self.cdiv = cdiv
        self.delval = Signal(5, name="delval")
        self.delload = Signal(2)
        self.o = Array(Signal(4, name='phy_out{}'.format(a)) for a in range(2))
        self.invert = invert
        self.initial_taps = initial_taps

    def elaborate(self, platform):
        m = Module()
        bufo = Signal(2)
        delo = Signal(2)
        m.submodules.ibuf = Instance(
            "IBUFDS_DIFF_OUT",
            p_IBUF_LOW_PWR="FALSE",
            i_I=self.pins.p, i_IB=self.pins.n,
            o_O=bufo[0], o_OB=bufo[1],
        )
        serout_raw = Array(Signal(8, name='serout_raw{}'.format(a)) for a in range(2))
        for i in range(2):
            m.submodules["idel{}".format(i)] = Instance(
                "IDELAYE2",
                p_IDELAY_TYPE="VAR_LOAD",
                p_DELAY_SRC="IDATAIN",
                p_IDELAY_VALUE=0 if i == 0 else 6, # fixme
                p_HIGH_PERFORMANCE_MODE="FALSE", # power vs jitter trade off
                p_SIGNAL_PATTERN="DATA",
    	        p_REFCLK_FREQUENCY=200.0, # Clock freq into IDELAYCTRL in MHz
	        i_C=ClockSignal(self.cdiv),
                i_REGRST=C(0),
                i_LD=self.delload[i],
                i_CE=C(0),
                i_INC=C(0),
                i_CINVCTRL=C(0),
                i_CNTVALUEIN=self.delval,
	        i_IDATAIN=bufo[i],
                i_DATAIN=C(0),
                i_LDPIPEEN=C(0),
                o_DATAOUT=delo[i],
                o_CNTVALUEOUT=Signal(5),
            )
            dbits = {}
            for j in range(8):
                dbits["o_Q{}".format(j+1)] = serout_raw[i][7-j]
            shiftout = Signal(2, name='shiftout{}'.format(i))
            m.submodules["iser{}".format(i)] = Instance(
                "ISERDESE2",
                p_DATA_RATE="DDR",
                p_DATA_WIDTH=4,
                p_INTERFACE_TYPE="NETWORKING",
                p_IOBDELAY="IFD",
                o_O=Signal(name='sero{}'.format(i)),
                o_SHIFTOUT1=shiftout[0],
                o_SHIFTOUT2=shiftout[1],
                i_D=C(0),
                i_DDLY=delo[i],
                i_CLK=ClockSignal(self.clk),
                i_CLKB=~ClockSignal(self.clk),
                i_CLKDIV=ClockSignal(self.cdiv),
                i_CLKDIVP=C(0),
                i_CE1=C(1),
                i_CE2=C(1),
                i_RST=self.reset,
                i_OCLK=C(0),
                i_OCLKB=C(0),
                i_BITSLIP=C(0),
                i_SHIFTIN1=C(0),
                i_SHIFTIN2=C(0),
                i_OFB=C(0),
                i_DYNCLKDIVSEL=C(0),
                i_DYNCLKSEL=C(0),
                **dbits,
            )
        m.d.comb += self.o[0].eq(~serout_raw[0][4:] if self.invert else serout_raw[0][4:])
        m.d.comb += self.o[1].eq(serout_raw[1][4:] if self.invert else ~serout_raw[1][4:])
        return m

class Async_RX_PHY_sim(Elaboratable):
    def __init__(
            self, pins, reset,
            clk='c500', # fast clock, 0.5 * data rate, BUFIO or BUFG, not required in simulation
            cdiv='c250r', # slow clock, 0.25 * data rate, BUFR if clk on BUFIO else BUFG
            initial_taps = [0,0],
            invert=False,
    ):
        self.cdiv = cdiv
        self.curdel = Array(
            Signal(5, name="curdel{}".format(a), reset=initial_taps[a]) for a in range(2))
        self.delval = Signal(5, name="delval")
        self.delload = Signal(2)
        self.o = Array(Signal(4, name='phy_out{}'.format(a)) for a in range(2))

    def elaborate(self, platform):
        m = Module()
        for i in range(2):
            with m.If(self.delload[i]):
                m.d[self.cdiv] += self.curdel[i].eq(self.delval)
        return m

# Accepts differential input, outputs 3, 4 or 5 bits per cdiv cycle
class Async_RX(Elaboratable):
    def __init__(
            self, pins, reset,
            taps_ui=13, # IDELAY taps (78.125 ps) for 1 UI
            clk='c500', # fast clock, 0.5 * data rate, BUFIO or BUFG
            cdiv='c250r', # slow clock, 0.25 * data rate, BUFR if clk on BUFIO else BUFG
            invert=False, # diff pair swap
            sim = False
    ):
        self.reset = reset
        self.taps_ui = taps_ui
        self.cdiv = cdiv
        self.o = Signal(5) # MSBs are valid bits
        self.obits = Signal(3) # number of bits in o
        self.odel = Signal(5) # only valid when obits = 4
        phymod = Async_RX_PHY_sim if sim else Async_RX_PHY
        self.phy = phymod(pins, reset, clk, cdiv, initial_taps=[0,taps_ui//2], invert=invert)

    def elaborate(self, platform):
        m = Module()
        sync = m.d[self.cdiv]
        phy = m.submodules.phy = self.phy
        sync += self.o.eq(self.phy.o[0])
        early = Signal(4)
        late = Signal(4)
        n_early = Signal(3)
        n_late = Signal(3)
        tap = Signal(5, reset = 0)
        tap_next = Signal(5, reset = 0)
        timer = Signal(4)
        phase_error = Signal(signed(7))
        step = Signal() # condition reached to increment or decrement the delays
        shift_inc = Signal()
        shift_dec = Signal()
        @unique
        class STATE(Enum):
            RESET = 0
            RUN = 1
            SET1 = 2
            WAIT1 = 3
            SET2 = 4
            WAIT2 = 5
            SET3 = 6
            WAIT3 = 7
        state = Signal(STATE, reset=STATE.RESET)

        m.d.comb += [
            late.eq(phy.o[0] ^ phy.o[1]),
            early.eq(phy.o[1] ^ Cat(self.o[3], phy.o[0][0:3])),
            step.eq(phase_error[-1] ^ phase_error[-2]),
        ]

        sync += [
            n_early.eq(early[0] + early[1] + early[2] + early[3]),
            n_late.eq(late[0] + late[1] + late[2] + late[3]),
            self.odel.eq(tap),
        ]

        with m.If(state == STATE.RUN):
            sync += [
                shift_inc.eq(~phase_error[-1] & (tap==self.taps_ui-1)),
                shift_dec.eq(phase_error[-1] & (tap == 0)),
                phase_error.eq(phase_error + n_early - n_late),
            ]
        with m.Elif(state == STATE.WAIT2):
            sync += [
                phase_error.eq(0),
            ]

        with m.If(state == STATE.RUN):
            with m.If(phase_error[-1]):
                sync += tap_next.eq(Mux(tap == 0, self.taps_ui-1, tap-1))
            with m.Else():
                sync += tap_next.eq(Mux(tap > (self.taps_ui-2), 0, tap+1))

        with m.If(state == STATE.SET2):
            sync += tap.eq(tap_next)

        with m.If(state == STATE.RUN):
            sync += phy.delval.eq(tap_next)
        with m.Elif(state == STATE.SET3):
            sync += phy.delval.eq(phy.delval + self.taps_ui//2)

        sync += phy.delload[1].eq((state == STATE.SET1) | (state == STATE.SET3))
        sync += phy.delload[0].eq(state == STATE.SET2)

        with m.Switch(state):
            with m.Case(STATE.RUN):
                sync += state.eq(Mux(step, STATE.SET1, STATE.RUN))
            with m.Case(STATE.SET1):
                sync += state.eq(STATE.WAIT1)
            with m.Case(STATE.WAIT1):
                sync += state.eq(Mux(timer[-1], STATE.SET2, STATE.WAIT1))
            with m.Case(STATE.SET2):
                sync += state.eq(STATE.WAIT2)
            with m.Case(STATE.WAIT2):
                sync += state.eq(Mux(timer[-1], STATE.SET3, STATE.WAIT2))
            with m.Case(STATE.SET3):
                sync += state.eq(STATE.WAIT3)
            with m.Case(STATE.WAIT3):
                sync += state.eq(Mux(timer[-1], STATE.RUN, STATE.WAIT3))
            with m.Case():
                sync += state.eq(STATE.RUN)

        # increment overflow: delay reduced, 5 bits
        # decrement underflow: delay increased, bit 0 is dupe
        with m.If((state == STATE.SET2) | (state == STATE.WAIT2)):
            sync += self.o.eq(Cat(phy.o[0][0], phy.o[1]))
        with m.Else():
            sync += self.o.eq(Cat(C(0,1), phy.o[0]))

        with m.If(state == STATE.SET2):
            with m.If(shift_inc):
                sync += self.obits.eq(5)
            with m.Elif(shift_dec):
                sync += self.obits.eq(3)
            with m.Else():
                sync += self.obits.eq(4)
        with m.Else():
            sync += self.obits.eq(4)

        with m.If((state == STATE.WAIT1) | (state == STATE.WAIT2) | (state == STATE.WAIT3)):
            sync += timer.eq(timer + 1)
        with m.Else():
            sync += timer.eq(0)

        return m
