# Copyright 2020 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth import *

class STARTUPE2(Elaboratable):
    def __init__(self):
        # outputs
        self.cfgclk = Signal() # only during config
        self.cfgmclk = Signal() # ~ 65 MHz free running
        self.eos = Signal()
        self.preq = Signal()
        self.usrcclk = Signal()

    def elaborate(self, platform):
        m = Module()
        m.submodules.startupe2_i = Instance(
            "STARTUPE2",
            i_CLK=C(0),
            i_GSR=C(0),
            i_GTS=C(0),
            i_KEYCLEARB=C(1),
            i_PACK=C(0),
            o_PREQ=self.preq,
            i_USRCCLKO=self.usrcclk, # for SPI
            i_USRCCLKTS=C(0),
            i_USRDONEO=C(1),
            i_USRDONETS=C(0),
            o_CFGCLK=self.cfgclk,
            o_CFGMCLK=self.cfgmclk,
            o_EOS=self.eos
        )
        return m
