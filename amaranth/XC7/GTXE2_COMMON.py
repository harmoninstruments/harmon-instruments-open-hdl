# Copyright 2020 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth import *

class GTXE2_COMMON(Elaboratable):
    def __init__(
            self,
            qpll_reset,
            refclk0 = C(0),
            refclk1 = C(0),
    ):
        # inputs
        self.refclk0 = refclk0
        self.refclk1 = refclk1
        self.northrefclk0 = C(0)
        self.northrefclk1 = C(0)
        self.southrefclk0 = C(0)
        self.southrefclk1 = C(0)
        self.qpllrefclksel = Signal(3, reset=1)
        self.qpll_reset = qpll_reset
        # outputs
        self.qpll_lock = Signal()
        self.qpll_refclk_lost = Signal()
        self.drp_do = Signal(16)
        self.drp_rdy = Signal()
        self.o_clock = Signal()
        self.o_refclock = Signal()
        self.tx_reset = Signal() # TX reset into channel modules
    def elaborate(self, platform):
        m = Module()
        m.submodules.gtxe2i = Instance(
            "GTXE2_COMMON",
            # these are for 10.3125 Gb/s
            p_QPLL_CFG = C(0x0680181,27),
            p_QPLL_CLKOUT_CFG = C(0,4),
            p_QPLL_COARSE_FREQ_OVRD = C(0b010000,6),
            p_QPLL_COARSE_FREQ_OVRD_EN = C(0,1),
            p_QPLL_CP = C(0b0000011111,10),
            p_QPLL_CP_MONITOR_EN = C(0,1),
            p_QPLL_DMONITOR_SEL = C(0,1),
            p_QPLL_FBDIV = C(0b0101000000,10),
            p_QPLL_FBDIV_MONITOR_EN = C(0,1),
            p_QPLL_FBDIV_RATIO = C(0,1),
            p_QPLL_INIT_CFG = C(0x000006,23),
            p_QPLL_LOCK_CFG = C(0x21E8,16),
            p_QPLL_LPF = C(0b1111,4),
            p_QPLL_REFCLK_DIV = 1,
            # DRP
            i_DRPADDR = C(0,8),
            i_DRPCLK = C(0),
            i_DRPDI = C(0,16),
            i_DRPEN = C(0),
            i_DRPWE = C(0),
            o_DRPDO = self.drp_do,
            o_DRPRDY = self.drp_rdy,
            # Ref Clock
            i_GTGREFCLK = C(0),
            i_GTNORTHREFCLK0 = self.northrefclk0,
            i_GTNORTHREFCLK1 = self.northrefclk1,
            i_GTREFCLK0 = self.refclk0,
            i_GTREFCLK1 = self.refclk1,
            i_GTSOUTHREFCLK0 = self.southrefclk0,
            i_GTSOUTHREFCLK1 = self.southrefclk1,
            # QPLL
            o_QPLLDMONITOR = Signal(8), # reserved
            o_QPLLOUTCLK = self.o_clock, # connect to QPLLCLK on GTXE2_CHANNEL
            o_QPLLOUTREFCLK = self.o_refclock, # connect to QPLLREFCLK on CHANNEL
            o_REFCLKOUTMONITOR = Signal(), # refclk mux out
            o_QPLLFBCLKLOST = Signal(),
            o_QPLLLOCK = self.qpll_lock, # async lock signal
            i_QPLLLOCKDETCLK = C(0),
            i_QPLLPD = C(0), # active high QPLL power down
            o_QPLLREFCLKLOST = self.qpll_refclk_lost,
            i_QPLLREFCLKSEL = self.qpllrefclksel,
            i_QPLLRESET = self.qpll_reset,
            # reserved inputs with require tie offs
            i_BGBYPASSB = C(1),
            i_BGMONITORENB = C(1),
            i_BGPDB = C(1),
            i_BGRCALOVRD = C(0b11111, 5),
            i_QPLLRSVD1 = C(0, 16),
            i_QPLLRSVD2 = C(0b11111, 5),
            i_QPLLLOCKEN = C(1),
            i_QPLLOUTRESET = C(0),
            i_PMARSVD = C(0,8),
            i_RCALENB = C(1),
        )
        m.d.comb += self.tx_reset.eq(~self.qpll_lock)
        return m
