# Copyright 2019 - 2020 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth import *

def get_out_fifo_instance(
        dout, din, reset, rden, wren, clki, clko, full, empty,
        almostfull = Signal(),
        almostempty = Signal(),
):
    if len(din) != 48:
        raise Error("din must be 48 bits")
    z4 = Repl(0,4)
    ports = {}
    for i in range(10):
        i1 = 4*i
        if i not in [5,6]:
            ports["i_D{}".format(i)]=Cat(din[i1:i1+4], z4)
            ports["o_Q{}".format(i)]=dout[i1:i1+4]
        else:
            i2 = 4*(i+5) # index of 10, 11 shared with 5, 6
            ports["i_D{}".format(i)]=Cat(din[i1:i1+4], din[i2:i2+4])
            ports["o_Q{}".format(i)]=Cat(dout[i1:i1+4], dout[i2:i2+4])

    return Instance(
        "OUT_FIFO",
        p_ARRAY_MODE="ARRAY_MODE_4_X_4",
        i_RDCLK=ClockSignal(clko),
        i_WRCLK=ClockSignal(clki),
        i_RESET=reset,
        i_RDEN=rden,
        i_WREN=wren,
        o_FULL=full,
        o_EMPTY=empty,
        #o_ALMOSTFULL=almostfull,
        #o_ALMOSTEMPTY=almostempty,
        **ports,
    )

class OUT_FIFO(Elaboratable):
    def __init__(self, din, reset, reset_out, clki='sync', clko='rclk'):
        self.din = din
        self.dout = Signal(len(din))
        self.reset = reset
        self.reset_out = reset_out
        self.clki = clki
        self.clko = clko

    def elaborate(self, platform):
        m = Module()
        empty= Signal(2)
        full=Signal(2)
        rden=Signal()
        wren=Signal()
        max_state = 3
        ostate = Signal(2)
        # wait until the FIFO is half full before enabling reads
        m.d.comb += [
            rden.eq((ostate == max_state) & (empty == 0)),
            wren.eq(~self.reset & (full == 0)),
        ]
        with m.If((empty != 0) | self.reset_out):
            m.d.rclk += ostate.eq(0)
        with m.Elif(ostate != max_state):
            m.d.rclk += ostate.eq(ostate + 1)
        m.submodules.fifo0 = get_out_fifo_instance(
            din=self.din[0:48],
            dout=self.dout[0:48],
            clki=self.clki, clko=self.clko,
            reset=self.reset,
            rden=rden, wren=wren,
            full=full[0], empty=empty[0])
        m.submodules.fifo1 = get_out_fifo_instance(
            din=self.din[48:],
            dout=self.dout[48:],
            clki=self.clki, clko=self.clko,
            reset=self.reset,
            rden=rden, wren=wren,
            full=full[1], empty=empty[1])
        return m

class OUT_FIFO2(Elaboratable):
    def __init__(self, din, reset, reset_out):
        self.din = din
        self.dout = Signal(len(din))
        self.reset = reset
        self.reset_out = reset_out
    def elaborate(self, platform):
        m = Module()
        abits = 4
        iptr = Signal(abits)
        optr = Signal(abits)
        ram = Memory(width=len(self.din), depth=2**abits)
        wrport = m.submodules.wrport = ram.write_port(domain="sync")
        rdport = m.submodules.rdport = ram.read_port (domain="rclk")
        m.d.comb += wrport.en.eq(1)
        m.d.sync += [
            wrport.addr.eq(iptr),
            wrport.data.eq(self.din),
            iptr.eq(Mux(sel=self.reset, val1=0, val0=iptr+1)),
        ]
        m.d.rclk += [
            rdport.addr.eq(optr),
            self.dout.eq(rdport.data),
            optr.eq(Mux(sel=self.reset_out, val1=0, val0=optr+1)),
        ]

        return m
