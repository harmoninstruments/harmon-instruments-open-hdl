`timescale 1 ps/1 ps

// it's a hack, but works unlike the Xilinx simprim
module BUFHCE(output O, input CE, input I);

   reg ce = 1;

   always @ (negedge I)
     ce <= CE;

   assign O=ce&I;

endmodule
