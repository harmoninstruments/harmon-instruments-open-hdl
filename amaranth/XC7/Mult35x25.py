# Copyright 2019-2020 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

import sys
from amaranth import *
from XC7.DSP import DSP48E1

# wide multiply using 2x DSP48E1
# p = ((a * b) + c), 4 clock pipe delay (3 for c to p)
# a is 25 bits, b is 35 bits, p is 65 bits, c is 48 bits
class Mult35x25(Elaboratable):
    def __init__(self, a, b, c=C(0,48), p=None, shift = 0, clk='sys2x'):
        self.i = (a,b,c,shift,clk)
        self.p = Signal(signed(65-shift)) if p is None else p
    def elaborate(self, platform):
        (a,b,c,shift,clk) = self.i
        m = Module()
        dspl = m.submodules.dspl = DSP48E1(
            clk=clk,
            opmode = C(0b0110101, 7), # a*b + c
            a = Cat(a, Repl(a[-1], 5)),
            b = Cat(b[:17], C(0)),
            c = c,
        )
        dsph = m.submodules.dsph = DSP48E1(
            clk=clk,
            AREG=1,
            BREG=2,
            CREG=0,
            cascade_in=dspl.cascade_out,
            opmode = C(0b1010101, 7), # a*b + pcin >> 17
            A_INPUT="CASCADE",
            b=b[17:35],
            cec=C(0),
        )
        if shift < 17:
            lowreg = Signal(17-shift)
            m.d[clk] += lowreg.eq(dspl.o[shift:17])
            m.d.comb += self.p.eq(Cat(lowreg, dsph.o))
        else:
            m.d.comb += self.p.eq(dsph.o[shift-17:])
        return m
