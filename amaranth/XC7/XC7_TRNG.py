# Copyright 2019-2021 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD
# True random number generator for Xilinx 7 Series FPGAs

from amaranth import *
from XC7.IO_FIFO import get_out_fifo_instance
from CDC.CDC import CDC_CE_bus
from amaranth.lib.cdc import FFSynchronizer

class TRNG_core(Elaboratable):
    def __init__(self, clk):
        self.o = Signal()
        self.sum = Signal(12)
        self.clk = clk
        self.o32 = Signal(32)
        self.v = Signal() # o32 valid
    def elaborate(self, platform):
        m = Module()

        carry = Signal(12)
        sumc = Signal(12)
        sr = Signal(32)
        count = Signal(5)

        for i in range(12//4):
            m.submodules["carry4_"+str(i)] = Instance(
                "CARRY4",
                i_DI=Repl(1,4),
                i_S=Repl(1,4), # carry propagate
                i_CI=ClockSignal("rngclk") if i==0 else carry[4*i-1],
                i_CYINIT=C(0),
                o_CO=carry[4*i:4*i+4],
                o_O=sumc[4*i:4*i+4],
            )

        d = self.sum

        m.d[self.clk] += [
            self.sum.eq(sumc),
            self.o.eq(d[0] ^ d[1] ^ d[2] ^ d[3] ^ d[4] ^ d[5] ^
                      d[6] ^ d[7] ^ d[8] ^ d[9] ^ d[10] ^ d[11]),
            sr.eq(Cat(self.o, sr[:31])),
            count.eq(count + 1),
            self.v.eq(count == 0),
        ]

        with m.If(count == 0):
            m.d[self.clk] += self.o32.eq(sr)

        return m

# clockgen is used for fine phase control of the clock 'rngclk'
#

class TRNG(Elaboratable):
    def __init__(self, pio, addr, clockgen):
        self.pio = pio
        self.core = TRNG_core(clk='c100')
        self.clockgen = clockgen
        self.ps = Signal()
        self.psbusy = Signal()
        self.count = Signal(8)
        self.xor = Signal(32)
        self.o32sync = Signal(32)
        self.sumsync = Signal(12)
        pio.add_wvalid(addr + 0x0, self.ps)
        pio.add_ro_reg(addr + 0x0, self.psbusy)
        self.rden = pio.add_ro_reg(addr + 0x4, self.xor)
        pio.add_ro_reg(addr + 0x8, self.o32sync)
        pio.add_ro_reg(addr + 0xC, self.sumsync)
        pio.add_ro_reg(addr + 0x10, self.count)

    def elaborate(self, platform):
        m = Module()

        core = m.submodules.core = self.core

        # bring the data into the 125 MHz domain
        m.submodules.syncsum = FFSynchronizer(
            i=self.core.sum, o=self.sumsync, o_domain='sync', stages=2, max_input_delay=25e-9)

        v = Signal()
        m.submodules.sync = CDC_CE_bus(
            i = self.core.o32,
            cei = self.core.v,
            o = self.o32sync,
            ceo = v,
            cin='c100',
            cout='sync',
            max_input_delay = 10e-9
        )

        m.d.sync += self.count.eq(Mux(self.rden, 0, self.count + (v & (self.count != 255))))

        with m.If(v):
            m.d.sync += self.xor.eq(self.xor ^ self.o32sync)

        # phase shifter
        m.d.sync += self.clockgen.psen.eq(self.ps)
        with m.If(self.ps):
            m.d.sync += self.clockgen.psincdec.eq(self.pio.wdata[0])

        m.d.sync += self.psbusy.eq(self.ps | (self.psbusy & ~self.clockgen.psdone))

        return m
