# Copyright 2019 - 2020 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD
from amaranth import *

def OBUFTDS(os, p, n):
    return Instance("OBUFTDS", i_I=os.o, i_T=os.tq, o_O=p, o_OB=n)

def OBUFT(os, pin):
    return Instance("OBUFTDS", i_I=os.o, i_T=os.tq, o_O=pin)

class OSERDES(Elaboratable):
    def __init__(self, reset, clk, cdiv, i, t=C(0), o=None):

        if len(i) < 2 or len(i) > 8:
            raise Error("i should be an array of 2 to 8 elements")

        self.reset = reset
        self.clk = clk
        self.cdiv = cdiv
        self.i = i
        self.o = Signal() if o is None else o
        self.tq = Signal()
        self.ofb = Signal()
        self.t = t

    def elaborate(self, platform):
        m = Module()

        dbits = {}
        for j in range(8):
            dbits["i_D{}".format(j+1)] = self.i[j] if (j < len(self.i)) else C(0)
        shiftout = Signal(2)
        m.submodules.os = Instance(
            "OSERDESE2",
            p_DATA_RATE_OQ="DDR",
            p_DATA_RATE_TQ="BUF",
            p_DATA_WIDTH=len(self.i),
            p_INIT_OQ=1,
            p_INIT_TQ=0,
            p_SERDES_MODE="MASTER",
            p_SRVAL_OQ=1,
            p_SRVAL_TQ=1,
            p_TBYTE_CTL="FALSE",
            p_TBYTE_SRC="FALSE",
            p_TRISTATE_WIDTH=1,
            o_OFB=self.ofb,
            o_OQ=self.o,
            o_SHIFTOUT1=shiftout[0],
            o_SHIFTOUT2=shiftout[1],
            o_TBYTEOUT=Signal(name="tbyteout"),
            o_TFB=Signal(name="tfb"),
            o_TQ=self.tq,
            i_CLK=ClockSignal(self.clk),
            i_CLKDIV=ClockSignal(self.cdiv),
            i_OCE=C(1),
            i_RST=self.reset,
            i_SHIFTIN1=C(0),
            i_SHIFTIN2=C(0),
            i_T1=self.t,
            i_T2=C(0),
            i_T3=C(0),
            i_T4=C(0),
            i_TBYTEIN=C(0),
            i_TCE=C(1),
            **dbits,
        )
        return m

class ISERDES(Elaboratable):
    def __init__(self, reset, clk, cdiv, i, bitslip=C(0)):
        self.reset = reset
        self.clk = clk
        self.cdiv = cdiv
        self.i = i
        self.bitslip = bitslip
        self.o = Signal(8)
        self.combout = Signal()
    def elaborate(self, platform):
        m = Module()
        dbits = {}
        for j in range(8):
            dbits["o_Q{}".format(j+1)] = self.o[7-j]

        shiftout = Signal(2)

        m.submodules.iser = Instance(
            "ISERDESE2",
            p_DATA_RATE="DDR",
            p_DATA_WIDTH=len(self.o),
            p_INTERFACE_TYPE="NETWORKING",
            p_IOBDELAY="IFD",
            o_O=self.combout,
            o_SHIFTOUT1=shiftout[0],
            o_SHIFTOUT2=shiftout[1],
            i_D=C(0),
            i_DDLY=self.i,
            i_CLK=ClockSignal(self.clk),
            i_CLKB=~ClockSignal(self.clk),
            i_CLKDIV=ClockSignal(self.cdiv),
            i_CLKDIVP=C(0),
            i_CE1=C(1),
            i_CE2=C(1),
            i_RST=self.reset,
            i_OCLK=C(0),
            i_OCLKB=C(0),
            i_BITSLIP=self.bitslip,
            i_SHIFTIN1=C(0),
            i_SHIFTIN2=C(0),
            i_OFB=C(0),
            i_DYNCLKDIVSEL=C(0),
            i_DYNCLKSEL=C(0),
            **dbits,
        )
        return m

# clk = 200 MHz (option for 300), reset is async and required after config
# ready indidicates it has calibrated, can be ignored if desired
def idelayctrl_instance(clk, reset, ready):
    return Instance("IDELAYCTRL", i_REFCLK=ClockSignal(clk), i_RST=reset, o_RDY=ready)

# Get an IDELAYE2 instance
# i is input
# o is output
# val is a 5 bit signal, 78.125 ps increments
# load sets the delay to val
# clk is a clock domain name

def idelay_instance(i, o, val, load, clk="sync", initial=0):
    CntValueOut = Signal(5)
    return Instance(
        "IDELAYE2",
        p_IDELAY_TYPE="VAR_LOAD",
        p_DELAY_SRC="IDATAIN",
        p_IDELAY_VALUE=initial,
        p_HIGH_PERFORMANCE_MODE="FALSE", # power vs jitter trade off
        p_SIGNAL_PATTERN="DATA",
        p_REFCLK_FREQUENCY=200.0, # Clock freq into IDELAYCTRL in MHz
        i_C=ClockSignal(clk),
        i_REGRST=C(0),
        i_LD=load,
        i_CE=C(0),
        i_INC=C(0),
        i_CINVCTRL=C(0),
        i_CNTVALUEIN=val,
        i_IDATAIN=i,
        i_DATAIN=C(0),
        i_LDPIPEEN=C(0),
        o_DATAOUT=o,
        o_CNTVALUEOUT=CntValueOut
    )

def ibufds_diffo_inst(i, o, disable):
    return Instance(
        "IBUFDS_DIFF_OUT_IBUFDISABLE",
        i_I=i.p, i_IB=i.n,
        i_IBUFDISABLE=disable,
        o_O=o[0], o_OB=o[1],
    )

class DualIserdes(Elaboratable):
    def __init__(self, pins, delv, delw, reset, disable=C(0), bitslip=C(0), clk='sys4x', cdiv='sync'):
        self.pins = pins
        self.delv = delv # delay value, 5 bits
        self.delw = delw # delay write, 2 bits
        self.reset = reset
        self.disable = disable
        self.bitslip = bitslip
        self.o = Array(Signal(8) for a in range(2))
        self.clk=clk
        self.cdiv=cdiv

    def elaborate(self, platform):
        m = Module()
        bufo = Signal(2)
        # fails timing if
        m.submodules.ibuf = ibufds_diffo_inst(i=self.pins, o=bufo, disable=C(0))#self.disable)
        delo = Signal(2)
        for i in range(len(delo)):
            m.submodules["idel{}".format(i)] = idelay_instance(
                i=bufo[i], o=delo[i], val=self.delv, load=self.delw[i])
            iser = m.submodules["iser{}".format(i)] = ISERDES(
                reset=self.reset, clk=self.clk, cdiv=self.cdiv, i=delo[i], bitslip=self.bitslip)
            m.d.comb += self.o[i].eq(iser.o)
        return m
