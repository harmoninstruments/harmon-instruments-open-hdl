# Copyright (C) 2014-2020 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

import os.path
import numpy as np
np.random.seed(seed=0)
from amaranth_cocotb import run
from amaranth import *
import cocotb
from cocotb.clock import Clock
from cocotb.triggers import Timer, RisingEdge, ReadOnly, Event
from cocotb.result import ReturnValue
from XC7.Mult35x25 import Mult35x25

def test_xc7_mult35x25():
    m = Module()
    a = Signal(signed(25))
    b = Signal(signed(35))
    c = Signal(signed(48))
    p = Signal(signed(65))
    sync = m.domains.sync = ClockDomain("sync", reset_less=True)
    m.submodules.dut = Mult35x25(a,b,c,p,clk='sync')
    run(m,
        'Mult35x25_test',
        ports=[sync.clk, a,b,c,p],
        vcd_file='dump.vcd',
        verilog_sources=[os.path.expandvars("$XPRIMS/DSP48E1.v")]
    )

def get_rand_signed(bits, count):
    return np.random.randint(-1*2**bits,2**bits-1,count, dtype=np.int64)

pipestages = 3
count = 25
a = get_rand_signed(25-1, count)
b = get_rand_signed(35-1, count)
c = get_rand_signed(48-1, count)

for i in range(10):
    a[i] = 0
    b[i] = 0
    c[i] = 0
a[4] = 0
b[4] = 0
c[4] = 0xCAFE
a[6] = -10923755
b[6] = 4827183763

@cocotb.test()
def run_test(dut):
    testvals = [(0,0,0), (10,20,30), (-1, 1, 0)]
    cocotb.fork(Clock(dut.clk, 4000).start())
    result = []
    yield RisingEdge(dut.clk)
    for i in range(len(a)):
        dut.a = int(a[i])
        dut.b = int(b[i] & (2**35-1))
        dut.c = int(c[i] & (2**48-1))
        yield RisingEdge(dut.clk)
        if(i > 4):
            result.append(dut.p.value.signed_integer)
            expected = a[i-4] * b [i-4] + c[i-3]
            print(a[i-4], b[i-4], c[i-3], result[-1], expected)
            assert expected == result[-1]
