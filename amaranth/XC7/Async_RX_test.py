# Copyright (C) 2021 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth import *
from XC7.Async_RX import *
from amaranth.sim import Simulator, Passive, Settle
import random

def test_Async_RX():
    random.seed(0)
    nbits=30000
    txdata = bin(random.getrandbits(nbits+150))[2:2+nbits]
    rx = Async_RX(pins=None, reset=None, cdiv='sync', sim=True)
    sim = Simulator(rx)
    sim.add_clock(4e-9, domain = 'sync')

    # t is in UI
    # jitter is added then nearest selected
    def get_bit(t):
        return 1 if txdata[int(round(t+random.uniform(-0.15,0.15)))] == '1' else 0

    def get_nibble(i, delay):
        v = 0
        for j in range(4):
            v |= get_bit(0.25+(4*i+j)*(0.9996 + 0.0004*4*i/nbits) -delay/12.8) << j
        return v

    def sim_phy():
        recovered = ''
        for i in range((nbits-50)//4):
            yield Settle()
            # delays in 78.125 ps steps, 12.8 taps = 1 UI @ 1 Gb/s
            delv0 = yield(rx.phy.curdel[0])
            delv1 = yield(rx.phy.curdel[1])
            yield rx.phy.o[0].eq(get_nibble(i, delv0))
            yield rx.phy.o[1].eq(get_nibble(i, delv1))
            odata = yield rx.o
            obits = yield rx.obits
            if obits == 4:
                recovered += "{:04b}".format(odata >> 1)[::-1]
            if obits == 3:
                recovered += "{:03b}".format(odata >> 2)[::-1]
            if obits == 5:
                recovered += "{:05b}".format(odata)[::-1]
            yield

        assert recovered[400:] in txdata

    sim.add_sync_process(sim_phy)
    with sim.write_vcd("dump.vcd"):
        sim.run()
