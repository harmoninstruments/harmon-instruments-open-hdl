# Copyright 2019-2020 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth import *

def idelay_fixed_inst(i, o, initial=0, pattern='DATA'):
    CntValueOut = Signal(5)
    return Instance(
        "IDELAYE2",
        p_IDELAY_TYPE="FIXED",#"VAR_LOAD",
        p_DELAY_SRC="IDATAIN",
        p_IDELAY_VALUE=initial,
        p_HIGH_PERFORMANCE_MODE="FALSE", # power vs jitter trade off
        p_SIGNAL_PATTERN=pattern,
        p_REFCLK_FREQUENCY=200.0, # Clock freq into IDELAYCTRL in MHz
        i_C=C(0),
        i_REGRST=C(0),
        i_LD=C(0),
        i_CE=C(0),
        i_INC=C(0),
        i_CINVCTRL=C(0),
        i_CNTVALUEIN=C(0,5),
        i_IDATAIN=i,
        i_DATAIN=C(0),
        i_LDPIPEEN=C(0),
        o_DATAOUT=o,
        o_CNTVALUEOUT=CntValueOut
    )
