# Copyright 2020 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth import *

# 1 register
# write zero to load the shift register with the device DNA
# reading returns a bit and shifts the register
# read 57 times for entire result
# MSB (56) first
# can't handle back to back reads, but should be no problem reading in a loop

class DNA(Elaboratable):
    def __init__(self, pio, addr, lsb=0):
        self.rv = Signal()
        self.shift_next = pio.add_ro_reg(addr, self.rv, lsb=lsb)
        self.read_next = pio.get_wvalid(addr, lsb=lsb)

    def elaborate(self, platform):
        read = Signal()
        shift = Signal()
        dout = Signal()

        m = Module()
        m.submodules.dna_port = Instance(
            "DNA_PORT",
            i_CLK=ClockSignal(),
            i_DIN=C(0),
            i_READ=read,
            i_SHIFT=shift,
            o_DOUT=dout,
        )

        m.d.sync += [
            shift.eq(self.shift_next),
            read.eq(self.read_next),
            self.rv.eq(dout),
        ]

        return m
