# Copyright 2019-2020 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth import *

class XADC(Elaboratable):
    def __init__(self, pio, addr=0x5):
        self.pio = pio
        self.busy = Signal()
        self.dout = Signal(16)
        self.wxadc = Signal()
        pio.add_ro_reg(addr, Cat(self.dout, self.busy))
        pio.add_wvalid(addr, self.wxadc)

    def elaborate(self, platform):
        m = Module()
        drp_do = Signal(16)
        drp_drdy = Signal()

        with m.If(drp_drdy):
            m.d.sync += self.dout.eq(drp_do)

        m.d.sync += self.busy.eq(self.wxadc | (self.busy & ~drp_drdy))

        m.submodules.xadc_i = Instance(
            "XADC",
            p_SIM_DEVICE='ZYNQ' if 'xc7z' in platform.device else '7SERIES',
            p_INIT_42=0x400,
            i_CONVST=C(0),
            i_CONVSTCLK=C(0),
            i_RESET=C(0),
            i_VN=C(0),
            i_VP=C(0),
            i_VAUXN=Repl(0,16),
            i_VAUXP=Repl(0,16),
            o_DO=drp_do, o_DRDY=drp_drdy,
            i_DADDR=self.pio.wdata[16:23],
            i_DCLK=ClockSignal(),
            i_DEN=self.wxadc,
            i_DI=self.pio.wdata[:16],
            i_DWE=self.pio.wdata[31],
        )
        return m
