# Copyright 2020 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth import *

# write only
# wait after write to ensure only 1 write per 2 bus cycles

class ICAPE2(Elaboratable):
    def __init__(self, pio, addr):
        self.pio = pio
        self.wdata = Signal(32)
        self.wv = Signal()
        self.odata = Signal(32)
        pio.add_wo_reg(addr, self.wdata)
        pio.add_wvalid(addr, self.wv)

    def elaborate(self, platform):
        m = Module()
        wv1 = Signal()
        m.d.sync += wv1.eq(self.wv)
        rev = Signal(32)
        m.d.comb += rev.eq(self.wdata[::-1])

        m.submodules.icap_i = Instance(
            "ICAPE2",
            p_ICAP_WIDTH="X32",
            i_CLK=ClockSignal(),
            i_CSIB=~wv1,
            i_I=Cat(rev[24:32], rev[16:24], rev[8:16], rev[0:8]),
            i_RDWRB=C(0),
            o_O=self.odata,
        )
        return m
