# Copyright 2014 - 2020 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth import *
from amaranth.build import Resource, Subsignal, Pins, Attrs, DiffPairs
from AXI.Stream import AXIStream
from AXI.Bus import AXIBus

class ZynqEMIOI2C():
    def __init__(self, n):
        self.n = n
        self.scl_o = Signal()
        self.scl_i = Signal()
        self.scl_oe = Signal()
        self.sda_o = Signal()
        self.sda_i = Signal()
        self.sda_oe = Signal()
    def get_zynq_ports(self):
        d={}
        for (sig, name) in [
                (self.scl_o, 'SCLO'), (self.scl_i, 'SCLI'), (self.scl_oe, 'SCLTN'),
                (self.sda_o, 'SDAO'), (self.sda_i, 'SDAI'), (self.sda_oe, 'SDATN'),]:
            io = 'i' if name[-1] == 'I' else 'o'
            d["{}_EMIOI2C{}{}".format(io, self.n, name)]=sig
        return d

class Zynq(Elaboratable):
    def __init__(self):
        self.S_AXI_HP = []
        self.S_AXI_GP = []
        self.M_AXI_GP = []
        for i in range(4):
            self.S_AXI_HP.append(AXIBus('HP{}'.format(i), dbits=64, idbits=6))
        for i in range(2):
            self.S_AXI_GP.append(AXIBus('SGP{}'.format(i), dbits=32, idbits=6))
            self.M_AXI_GP.append(AXIBus('MGP{}'.format(i), dbits=32, idbits=12))
        self.irq = Signal(20)
        self.gpio_i = Signal(64)
        self.gpio_o = Signal(64)
        self.gpio_oe = Signal(64)
        self.I2C = []
        for i in range(2):
            self.I2C.append(ZynqEMIOI2C(i))

    def elaborate(self, platform):
        m = Module()
        ports = {}

        m.submodules.zynq = Instance(
            "PS7",
            i_IRQF2P=self.irq,
            i_EMIOGPIOI=self.gpio_i,
            o_EMIOGPIOO=self.gpio_o,
            o_EMIOGPIOTN=self.gpio_oe,
            **self.I2C[0].get_zynq_ports(),
            **self.I2C[1].get_zynq_ports(),
            **self.M_AXI_GP[0].get_zynq_ports("MAXIGP0"),
            **self.M_AXI_GP[1].get_zynq_ports("MAXIGP1"),
            **self.S_AXI_HP[0].get_zynq_ports("SAXIHP0", zynq_is_master=False),
            **self.S_AXI_HP[1].get_zynq_ports("SAXIHP1", zynq_is_master=False),
            **self.S_AXI_HP[2].get_zynq_ports("SAXIHP2", zynq_is_master=False),
            **self.S_AXI_HP[3].get_zynq_ports("SAXIHP3", zynq_is_master=False),
            **ports,
        )
        return m
