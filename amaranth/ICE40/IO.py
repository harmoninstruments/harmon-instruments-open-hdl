# Copyright (C) 2014-2020 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth import *

def get_oreg_ce_inst(ce, i, o, clock='sync'):
    return Instance(
        "SB_IO",
        p_PIN_TYPE=0b010101,
        o_PACKAGE_PIN=o,
        i_CLOCK_ENABLE=ce,
        i_OUTPUT_CLK=ClockSignal(clock),
        i_D_OUT_0=i,
    )

def get_oreg_inst(i, o, clock='sync'):
    return Instance(
        "SB_IO",
        p_PIN_TYPE=0b010101,
        o_PACKAGE_PIN=o,
        i_OUTPUT_CLK=ClockSignal(clock),
        i_D_OUT_0=i,
    )

def get_oddr_inst(i, o, clock='sync'):
    return Instance(
        "SB_IO",
        p_PIN_TYPE=0b010001,
        o_PACKAGE_PIN=o,
        i_OUTPUT_CLK=ClockSignal(clock),
        i_D_OUT_0=i[0],
        i_D_OUT_1=i[1],
    )

def get_iddr_inst(i, o, clock='sync'):
    return Instance(
        "SB_IO",
        p_PIN_TYPE=0b000000,
        i_PACKAGE_PIN=i,
        i_INPUT_CLK=ClockSignal(clock),
        o_D_IN_0=o[0],
        o_D_IN_1=o[1],
    )

def get_ireg_inst(i, o, clock='sync'):
    return Instance(
        "SB_IO",
        p_PIN_TYPE=0b000000,
        i_PACKAGE_PIN=i,
        i_INPUT_CLK=ClockSignal(clock),
        o_D_IN_0=o,
    )
