# Copyright (C) 2022 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth import *

from SPI.flash import SPI_flash
from DSP.CIC import CIC_Decimate
from XCU.primitives import iddr, ff_io

class MCP3461(Elaboratable):
    """
    Interface for Microchip MCP3461 ADC

    uses MDAT raw output, filtering in the FPGA

    the "sync" clock is expected to be 125 MHz

    o ranges from +2 to -2 times CIC gain of R**N

    ovalid asserts at 1/256th of the "sync" clock

    N : number of CIC stages
    log2R : log2(CIC rate change)

    """
    def __init__(self, pio, addr, count, N=4, log2R=3):
        self.pio = pio
        self.addr = addr
        self.count = count
        self.ovalid = Signal()
        self.N = N
        self.log2R=log2R
        self.o = Signal(signed(3 + self.N*self.log2R))

    def elaborate(self, platform):
        m = Module()

        pins = platform.request("clock_adc", 0, dir={"mdat":"-", "mclk":"-"})
        m.submodules.mclk_ff = ff_io(d=self.count[2], q=pins.mclk, clk='sync') # 15.625 MHz

        m.submodules.spi = spi = SPI_flash(
            sck=pins.sck,
            copi=pins.copi,
            cipo=pins.cipo,
            cs=pins.cs,
            wv=self.pio.get_wvalid(self.addr, name="MCP3461 SPI"),
            wdata=self.pio.wdata[:9],
        )
        self.pio.add_ro_reg(self.addr, spi.rdata, name="MCP3461 SPI")

        mdat_iddr = Signal(2)
        m.submodules.iddr = iddr(i=pins.mdat, o=mdat_iddr, clk='sync')

        idata = Signal(2)
        m.d.sync += idata.eq(Cat(mdat_iddr[0], idata[0]))

        idata_edge = Signal(6)
        with m.If(idata == 0b10):
            m.d.sync += idata_edge.eq(self.count[:len(idata_edge)])

        sample_edge = Signal(5)
        self.pio.add_wo_reg(self.addr + 0x10, sample_edge, name="MCP3461 sample point")
        self.pio.add_ro_reg(self.addr + 0x10, idata_edge, name="MCP3461 edge")

        raw = Signal(signed(3)) # delta sigma ADC output, ranges from -2 to 2
        raw_latch = Signal(signed(3))
        raw_valid = Signal()    # asserts at 1/32 of the "sync clock"

        m.d.sync += raw_valid.eq(sample_edge[:5] == self.count[:5])

        with m.If(sample_edge[:3] == self.count[:3]):
            with m.If(sample_edge[3:5] == self.count[3:5]):
                m.d.sync += raw_latch.eq(raw)
                m.d.sync += raw.eq(Mux(idata[0], C(-1,3), C(-2,3)))
            with m.Elif(idata[0]):
                m.d.sync += raw.eq(raw + 1)

        o_ce = Signal()
        m.d.sync += o_ce.eq(self.count[:(5+self.log2R)] == 0)

        m.submodules.cic = cic = CIC_Decimate(
            i=raw_latch, i_ce=raw_valid, o_ce=o_ce, R=2**self.log2R, N=self.N
        )

        m.d.comb += self.o.eq(cic.o)
        m.d.sync += self.ovalid.eq(o_ce)
        self.pio.add_ro_reg(self.addr + 0x20, self.o, name="MCP3461 data")

        return m
