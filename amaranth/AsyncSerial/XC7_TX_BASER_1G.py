# Copyright (C) 2020-2021 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth import *
from AsyncSerial.TXGearbox64b66b_4b import TXGearbox64b66b_4b
from Ethernet.Scrambler10GBASER import Scrambler
from XC7.SERDES import OSERDES, OBUFTDS, OBUFT
from XC7.clockbuffers import bufhce_instance

# common elements that can be shared by transmitters in the same clock region

class XC7_TX_BASER_1G_common(Elaboratable):
    def __init__(
            self,
            reset,
            c250='c2x',
            c500='c4x',
            c250_gated='tx250_gated',
    ):
        self.c250 = c250
        self.c500 = c500
        self.c250_gated = c250_gated
        self.reset = reset
        self.reset2x = Signal()
        self.seq = Signal(6)

    def elaborate(self, platform):
        m = Module()

        # generate sequence, 0 - 32
        seq1 = Signal(6)
        m.d[self.c250] += [
            self.reset2x.eq(self.reset),
            seq1.eq(Mux(seq1[5], 0, seq1 + 1)),
            self.seq.eq(seq1),
        ]

        # create the gated clock
        m.submodules.bufh = bufhce_instance(i=ClockSignal(self.c250), ce=self.o != 32, o=self.c250_gated)

        return m

class XC7_TX_BASER_1G(Elaboratable):
    def __init__(
            self,
            common, # instance of XC7_TX_BASER_1G_common
            pins,
    ):
        self.common = common
        self.pins = pins

    def elaborate(self, platform):
        m = Module()

        sync250 = m.d[self.common.c250]
        sync250g = m.d[self.common.c250_gated]

        # generate sequence, 0 - 32
        d1 = Signal(4)

        with m.Switch(self.common.seq):
            with m.Case(0):
                m.d.comb += d1.eq(0x1)
            with m.Case(1):
                m.d.comb += d1.eq(0xE)
            with m.Case(16):
                m.d.comb += d1.eq(0x1)
            with m.Case(17):
                m.d.comb += d1.eq(0xE)
            with m.Case():
                m.d.comb += d1.eq(0)

        # scramble, pipe stage for seq, control
        m.submodules.scrambler = scrambler = Scrambler(i=d1, en=C(1), clk=self.common.c250_gated)
        c2 = Signal()
        sync250 += [
            c2.eq(seq1[4]),
        ]
        m.submodules.gbox = gbox = TXGearbox64b66b_4b(
            seq=self.common.seq,
            din=scrambler.o,
            control=c2,
            clk=self.common.c250
        )
        m.submodules.oser = os = OSERDES(
            reset=self.common.reset2x,
            clk=self.common.c500,
            cdiv=self.common.c250,
            i=gbox.o,
            t=C(0)
        )
        try:
            m.submodules.obuf = OBUFTDS(os=os, p=self.pins.p, n=self.pins.n)
        except:
            m.submodules.obuf = OBUFT(os=os, pin=self.pins)

        return m
