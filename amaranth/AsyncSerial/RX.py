# Copyright (C) 2020-2021 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD
# 10GBASE-R derived receive chain with 4 bit data path
# intended for use in FPGA to FPGA link with 1000 Mb/s PHY rate

from amaranth import *
from Ethernet.CRC import CRC

# takes a stream of 3, 4 or 5 bit chunks, outputs 6 bit chunks
# i is Async_RX module from XC7.Async_RX, i.o, i.obits
class RX345b_6b(Elaboratable):
    def __init__(self, i):
        self.i = i
        self.bitslip = Signal()
        self.o = Signal(6)
        self.ovalid = Signal()

    def elaborate(self, platform):
        m = Module()
        sr = Signal(10)
        sr_bits = Signal(3)
        sr_bits_next_with_out = Signal(4)
        sr_bits_next = Signal(3)
        ovalid_next = Signal()
        incoming_bits = Signal(3)
        m.d.comb += [
            incoming_bits.eq(self.i.obits - self.bitslip),
            self.o.eq(sr[0:6]),
            sr_bits_next_with_out.eq(sr_bits+incoming_bits),
            ovalid_next.eq(sr_bits_next_with_out >= 6),
            sr_bits_next.eq(Mux(ovalid_next, sr_bits_next_with_out-6, sr_bits_next_with_out)),
        ]
        m.d.sync += [
            self.ovalid.eq(ovalid_next),
            sr_bits.eq(sr_bits_next),
        ]
        for i in range(len(sr)):
            bitsel = Signal(3, name="bitsel{}".format(i))
            m.d.comb += bitsel.eq((i - sr_bits)+(5-incoming_bits))
            with m.If(i >= sr_bits):
                with m.Switch(bitsel):
                    for j in range(5):
                        with m.Case(j):
                            m.d.sync += sr[i].eq(self.i.o[j])
                    with m.Case():
                        m.d.sync += sr[i].eq(0)
            with m.Else():
                if i < 4:
                    with m.If(self.ovalid):
                        m.d.sync += sr[i].eq(sr[i+6])
        return m

# takes a stream of 6 bit chunks, outputs bytes, last, valid
# i is RX345b_6b module, uses o, ovalid
class RX64b66b_6b_8b (Elaboratable):
    def __init__(self, i):
        self.i = i
        self.o = Signal(8)
        self.ocontrol = Signal() # valid at all outputs
        self.olast = Signal()
        self.ovalid = Signal()
        self.ofirst = Signal()
        self.oerror = Signal() # asserted high 1 clock if 00 or 11 header detected

    def elaborate(self, platform):
        m = Module()
        sr = Signal(6)
        state = Signal(4)
        m.d.sync += self.olast.eq(state == 10)
        m.d.sync += self.ofirst.eq(state == 1)
        with m.If(self.i.ovalid):
            with m.If(state == 0):
                m.d.sync += [
                    self.oerror.eq((self.i.o[0] == self.i.o[1])),
                    self.ocontrol.eq(self.i.o[0]), # fixme
                ]
            m.d.sync += state.eq(Mux(state == 10, 0, state + 1))
            with m.Switch(state):
                with m.Case(0):
                    m.d.sync += sr.eq(self.i.o[2:])
                    m.d.sync += self.o.eq(0)
                    m.d.sync += self.ovalid.eq(0)
                b_sr = 4
                for i in range(1,11):
                    with m.Case(i):
                        if b_sr == 0:
                            m.d.sync += sr.eq(self.i.o)
                        elif b_sr == 2:
                            m.d.sync += sr.eq(0)
                        else:
                            m.d.sync += sr.eq(self.i.o[8-b_sr:])
                        m.d.sync += self.o.eq(0 if b_sr == 0 else Cat(sr[0:b_sr],self.i.o[0:8-b_sr]))
                        m.d.sync += self.ovalid.eq(1 if b_sr >= 2 else 0)
                        b_sr = (b_sr + 6) & 0x7
        with m.Else():
            m.d.sync += self.ovalid.eq(0)
        return m

# Strip and check CRCs
# i is RX64b66b_6b_8b output after descrambling
# input data stream is has already been block synchronized and descrambled
# o.error is valid with olast, indicates bad CRC or other error

class Strip_CRC(Elaboratable):
    # first indicates first in block of 8 bytes, not first of packet
    # error indicates packet should be dropped, sampled at last byte
    # control indicates this is the last block of 8 bytes in the packet
    irecord = [("data", 8), ("first", 1), ("valid", 1), ("control", 1)]
    def __init__(self, i=Record(irecord)):
        self.i = i
        self.o = Record([("data", 8), ("last", 1), ("error", 1), ("valid", 1)])
    def elaborate(self, platform):
        ibuf = Record(self.irecord)
        m = Module()
        with m.If(self.i.valid):
            m.d.sync += ibuf.eq(self.i)
        with m.Else():
            m.d.sync += ibuf.valid.eq(0)
        drec = [("data", 8), ("last", 1), ("valid", 1)]
        d1 = Record(drec)
        first_delay = Signal(7)
        crc_last = Signal()
        idata_is_data = Signal()
        m.d.comb += idata_is_data.eq(ibuf.valid & (~ibuf.control | ibuf.first | (first_delay[0:3] != 0)))
        m.d.sync += [
            d1.valid.eq(idata_is_data),
            d1.last.eq(idata_is_data & ibuf.control & first_delay[2]),
            d1.data.eq(ibuf.data),
            crc_last.eq(ibuf.valid & ibuf.control & first_delay[6]),
        ]
        with m.If(ibuf.valid):
            m.d.sync += first_delay.eq(Cat(ibuf.first, first_delay))

        crc_reg = Signal(32, reset=0xFFFFFFFF)
        crc = m.submodules.crc = CRC(crc_prev = crc_reg, i=ibuf.data)

        crc_reset = Signal(reset=1)
        m.d.sync += crc_reset.eq(d1.last)

        with m.If(ibuf.valid):
            with m.If(ibuf.control & first_delay[6]):
                m.d.sync += crc_reg.eq(0xFFFFFFFF)
            with m.Else():
                m.d.sync += crc_reg.eq(crc.o)
        fcs_ok = Signal()
        m.d.sync += fcs_ok.eq(crc.o == 0xDEBB20E3)

        m.d.sync += [
            self.o.last.eq(crc_last),
            self.o.valid.eq((d1.valid & ~d1.last) | crc_last),
        ]
        with m.If(d1.valid):
            m.d.sync += [
                self.o.data.eq(d1.data),
                self.o.error.eq(0),
            ]
        with m.Elif(crc_last):
            m.d.sync += [
                self.o.error.eq(~fcs_ok),
            ]

        return m
