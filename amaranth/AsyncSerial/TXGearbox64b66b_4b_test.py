# Copyright 2020-2021 Harmon Instruments, LLC
from amaranth import *
from amaranth.sim import Simulator, Settle
from random import randint
from AsyncSerial.TXGearbox64b66b_4b import *

def test_TXGearbox64b66b_4b():
    m = Module()
    seq = Signal(6)
    din = Signal(4)
    control = Signal(1)
    gbox = m.submodules.gbox = TXGearbox64b66b_4b(seq=seq, din=din, control=control, clk='sync')
    sim = Simulator(m)
    sim.add_clock(4e-9)

    d = [0xDEADBEEFCAFED00F, 0x1234567855557777]
    c = [1, 0]

    expected = 0x1 | (d[0]<<2) | (0x2<<66) | (d[1] << 68)

    def test_proc():
        out = 0
        for i in range(33):
            if i == 0:
                yield control.eq(c[0])
            elif i == 16:
                yield control.eq(c[1])
            else:
                yield control.eq(0)
            if i < 16:
                yield din.eq(0xF & (d[0] >> (4*i)))
            elif i < 32:
                yield din.eq(0xF & (d[1] >> (4*(i-16))))
            yield seq.eq(i)
            yield
            yield Settle()
            o = yield gbox.o
            e = 0xf & (expected >> (4*i))
            assert o == e

    sim.add_sync_process(test_proc)
    with sim.write_vcd("dump.vcd"):
        sim.run()
