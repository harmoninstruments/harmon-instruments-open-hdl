# Copyright (C) 2020-2021 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth import *
from XC7.Async_RX import Async_RX
from AsyncSerial.RX import RX345b_6b, RX64b66b_6b_8b, Strip_CRC
from Ethernet.BlockSync64b66b import BlockSync64b66b
from Ethernet.Scrambler10GBASER import Descrambler

class XC7_RX_BASER_1G(Elaboratable):
    def __init__(
            self,
            pins,
            reset,
            invert=False,
    ):
        self.reset = reset
        self.pins = pins
        self.invert = invert
        self.lock = Signal()
        self.o = Record([("data", 8), ("last", 1), ("error", 1), ("valid", 1)])
        self.reset2x = Signal()
        self.rxphy = Async_RX(
            pins=self.pins, reset=self.reset2x, clk='c2x', cdiv='sync',
            invert=self.invert
        )

    def elaborate(self, platform):
        m = Module()

        m.d.sync += self.reset2x.eq(self.reset)

        # recover data from the asynchronous stream
        rxphy = m.submodules.rxphy = self.rxphy

        # convert the stream of 3, 4 or 5 bit data to 6 bit
        to_6b = m.submodules.to_6b = RX345b_6b(rxphy)

        # 64b66b decode the 6 bit stream to an 8 bit stream
        decode_64b66b = m.submodules.decode_64b66b = RX64b66b_6b_8b(to_6b)

        # bitslip control
        blocksync = m.submodules.blocksync = BlockSync64b66b(
            header_err=decode_64b66b.oerror,
            header_valid=decode_64b66b.ofirst,
            slip=to_6b.bitslip
        )

        m.d.sync += self.lock.eq(blocksync.lock)

        # descramble, pipe stage for seq, control
        m.submodules.descrambler = descrambler = Descrambler(
            i=decode_64b66b.o, en=decode_64b66b.ovalid
        )

        descrambled = Record([("data", 8), ("first", 1), ("valid", 1), ("control", 1)])
        with m.If(decode_64b66b.ovalid):
            m.d.sync += [
                descrambled.control.eq(decode_64b66b.ocontrol),
                descrambled.first.eq(decode_64b66b.ofirst),
            ]
        m.d.sync += descrambled.valid.eq(decode_64b66b.ovalid)
        m.d.comb += descrambled.data.eq(descrambler.o)

        # Check and remove CRCs
        strip_crc = m.submodules.strip_crc = Strip_CRC(i=descrambled, clk='sync')
        m.d.comb += self.o.eq(strip_crc.o)

        # FIFO to 125 MHz domain

        return m
