# Copyright (C) 2020-2021 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth import *

class TXGearbox64b66b_4b(Elaboratable):
    def __init__(
            self,
            seq, # 6 bits, 0-32, upcount
            din, # 5 bits, valid when seq != 32
            control, # 1 bit, valid when seq = 0, seq = 16
            clk = 'sync',
    ):
        self.seq = seq
        self.din = din
        self.control = control
        self.clk = clk
        self.o = Signal(4)

    def elaborate(self, platform):
        m = Module()
        sync = m.d[self.clk]
        sr = Signal(8)
        m.d.comb += self.o.eq(sr[0:4])
        with m.Switch(self.seq):
            with m.Case(0):
                sync += sr.eq(Cat(self.control, ~self.control, self.din))
            with m.Case(16):
                sync += sr.eq(Cat(sr[4:6], self.control, ~self.control, self.din))
            with m.Case('00----'): # 0-15 we have 2 extra bits in sr
                sync += sr.eq(Cat(sr[4:6], self.din))
            with m.Case(): # 16-31 we have 4 extra bits
                sync += sr.eq(Cat(sr[4:8], self.din))
        return m
