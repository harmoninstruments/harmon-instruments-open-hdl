# Copyright 2020-2021 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth import *
from amaranth.sim import Simulator, Settle, Passive
from struct import pack, unpack
import random, zlib
from AsyncSerial.RX import RX345b_6b, RX64b66b_6b_8b, Strip_CRC

class mock_Async_RX():
    def __init__(self):
        self.o = Signal(5, name='din')
        self.obits = Signal(3, name='din_bits')

def test_RX345b_6b():
    async_rx = mock_Async_RX()
    dut = RX345b_6b(i=async_rx)
    sim = Simulator(dut)
    sim.add_clock(4e-9)
    nbits=600
    random.seed(0)
    txdata = bin(random.getrandbits(nbits+150))[2:2+nbits]

    def sender():
        b = 0
        while b < (nbits-5):
            nb = random.randint(2,5)
            yield async_rx.o.eq(int(txdata[b:b+nb][::-1],2)<<(5-nb))
            yield async_rx.obits.eq(nb)
            yield
            b += nb

    def receiver():
        yield
        b = 0
        while b < (nbits-16):
            yield
            yield Settle()
            ov = yield dut.ovalid
            if ov:
                o = yield dut.o
                s = txdata[b:b+6][::-1]
                expected = int(txdata[b:b+6][::-1],2)
                if o != expected:
                    yield
                assert o == expected
                b+=6

    sim.add_sync_process(sender)
    sim.add_sync_process(receiver)
    with sim.write_vcd("dump.vcd"):
        sim.run()

class mock_RX64b66b_6b_8b():
    def __init__(self):
        self.o = Signal(6, name='din')
        self.ovalid = Signal(1, name='din_v')

def test_RX64b66b_6b_8b():
    rx = mock_RX64b66b_6b_8b()
    dut = RX64b66b_6b_8b(i=rx)
    sim = Simulator(dut)
    sim.add_clock(4e-9)
    random.seed(0)

    testdata = [
        (0xFFFFFFFFFFFFFFFF,2),
        (0xDEADBEEFCAFEF00D,1),
        (0x0123456789ABCDEF,2),
        (0x5555555555555555,3),
        (0xAAAAAAAAAAAAAAAA,0),
        (0xDEADBEEFCAFEF00D,2),
    ]

    def send(d, control):
        d = d<<2 | control
        print(bin(d))
        for i in range(11):
            yield rx.ovalid.eq(0)
            if random.choice([False, False, False, False]):
                yield
            yield rx.ovalid.eq(1)
            yield rx.o.eq(d&0x3F)
            yield
            d = d >> 6

    def sender():
        for (d,c) in testdata:
            yield from send(d,c)

    def get():
        d = 0
        e = 0
        c = 0
        for i in range(8):
            while True:
                v = yield dut.ovalid
                if v:
                    break
                yield
            byte = yield dut.o
            oe = yield dut.oerror
            oc = yield dut.ocontrol
            first = yield dut.ofirst
            if i == 0:
                e = oe
                c = oc
                assert first == 1
            else:
                assert e == oe
                assert c == oc
                assert first == 0
            last = yield dut.olast
            assert last == (i==7)
            d |= byte << (8*i)
            yield

        return (d,c,e)

    def receiver():
        for (d,c) in testdata:
            (od, oc, oe) = yield from get()
            print(hex(d),c,hex(od),oe)
            if c == 3 or c==0:
                assert oe
            else:
                assert ~oe
            assert d == od

    sim.add_sync_process(sender)
    sim.add_sync_process(receiver)
    with sim.write_vcd("dump.vcd"):
        sim.run()

def test_Strip_CRC():
    dut = Strip_CRC()

    maxpacket = bytearray(33)
    maxpacket[0] = 0x55
    for i in range(1,len(maxpacket)):
        maxpacket[i] = i

    skipspot = 0

    def send64(data, control = True, stutter=True):
        yield dut.i.valid.eq(1)
        yield dut.i.control.eq(1 if control else 0)
        yield dut.i.first.eq(1)
        for i in range(8):
            yield dut.i.data.eq(data & 0xFF)
            if stutter:
                yield dut.i.valid.eq(0)
                yield
                yield dut.i.valid.eq(1)
            yield
            yield dut.i.first.eq(0)
            data = data >> 8

    txpacket = []
    rxpacket = []

    def send_packet(data, failcrc=False, stutter=False):
        txpacket.append((data, failcrc))
        crc = zlib.crc32(data)
        if failcrc:
            crc += 1
        rdata = data + pack("I", crc)
        words = len(rdata)//8
        for i in range(words):
            yield from send64(unpack("Q", rdata[8*i:8*i+8])[0], control=(i==(words-1)), stutter=stutter)

    def capture():
        yield Passive()
        packet = bytearray()
        while True:
            yield
            ov = (yield dut.o.valid)
            if ov == 0:
                continue
            od = (yield dut.o.data)
            ol = (yield dut.o.last)
            if not ol:
                packet += pack("B", od)
                continue
            oe = (yield dut.o.error)
            packet += pack("B", od)
            rxpacket.append((packet, oe))
            packet = bytearray()

    def test_proc():
        yield
        for i in range(0,3):
            d = maxpacket[:4+i*8]
            yield from send_packet(d)
            yield from send_packet(d, failcrc=True)
            yield from send_packet(d, stutter=True)
            yield from send_packet(d, failcrc=True, stutter=True)
        yield from send64(0, control=False)
        for i in range(len(txpacket)):
            if txpacket[i][1]: # error
                assert rxpacket[i][1]
            else:
                assert rxpacket[i][0] == txpacket[i][0]
                assert not rxpacket[i][1]
    sim = Simulator(dut)
    sim.add_clock(4e-9)
    sim.add_sync_process(test_proc)
    sim.add_sync_process(capture)
    with sim.write_vcd("dump.vcd"):
        sim.run()
