# Copyright (C) 2022 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD
from amaranth import *
from amaranth.utils import log2_int

"""
cascaded integrator comb filters

References:

1: CIC Filter Introduction
Matthew P. Donadio
https://www.dspguru.com/files/cic.pdf

2: An economical class of digital filters for decimation and interpolation
E. Hogenauer
IEEE Transactions on Acoustics, Speech, and Signal Processing (Volume: 29, Issue: 2, Apr 1981)
DOI: 10.1109/TASSP.1981.1163535

The differential delay, M is fixed to 1 in these filters

"""

class CIC_Decimate(Elaboratable):
    """
    Decimiating CIC filter
    R = rate change ratio
    N = number of stages
    gain is R**N
    """
    def __init__(self, i, i_ce=C(1), o_ce=C(1), R=1, N=1, clk='sync'):
        self.o = Signal(signed(len(i) + (log2_int(R**N))))
        self.args = (i, i_ce, o_ce, N, clk)

    def elaborate(self, platform):
        (i, i_ce, o_ce, N, clk) = self.args
        m = Module()
        integrators = Array(
            Signal(signed(len(self.o)), name='integrate_{}'.format(a)) for a in range(N)
        )
        combs = Array(Signal(signed(len(self.o)), name='comb_{}'.format(a)) for a in range(N))
        combregs = Array(Signal(signed(len(self.o)), name='combreg_{}'.format(a)) for a in range(N))
        with m.If(i_ce):
            m.d[clk] += integrators[0].eq(integrators[0] + i)
            for j in range(1,N):
                m.d[clk] += integrators[j].eq(integrators[j] + integrators[j-1])

        for j in range(1,N):
            m.d[clk] += combs[j].eq(combs[j-1] - combregs[j])
        with m.If(o_ce):
            m.d[clk] += [
                combs[0].eq(integrators[-1] - combregs[0]),
                combregs[0].eq(integrators[-1]),
            ]
            for j in range(1,N):
                m.d[clk] += combregs[j].eq(combs[j-1])

        assert len(self.o) == len(combs[-1])
        m.d.comb += self.o.eq(combs[-1])

        return m

class CIC_Interpolate(Elaboratable):
    """
    Interpolating CIC filter
    R = rate change ratio
    N = number of stages
    gain is R**(N-1)
    """
    def __init__(self, i, i_ce=C(1), o_ce=C(1), R=1, N=1, clk='sync'):
        self.o = Signal(signed(len(i) + (log2_int(R**(N-1)))))
        self.args = (i, i_ce, o_ce, R, N, clk)

    def elaborate(self, platform):
        (i, i_ce, o_ce, R, N, clk) = self.args
        m = Module()

        # Ref 2 (22, 23, 24) for widths
        def comblen(a):
            return len(i) + a + (1 if a != N-1 else 0)
        combs = Array(Signal(signed(comblen(a)), name='comb_{}'.format(a)) for a in range(N))
        combregs = Array(Signal(signed(comblen(a)), name='combreg_{}'.format(a)) for a in range(N))
        # simplifying Ref 2 (22) for R=1, j = a + N + 1
        # integrator widths Ga = 2**(N-(a+1)) * R**a where a = 0 .. N-1
        integrators = Array(
            Signal(
                signed(len(i)+log2_int(2**(N-(a+1))*R**a)),
                name='integrate_{}'.format(a)) for a in range(N)
        )

        with m.If(i_ce):
            m.d[clk] += combregs[0].eq(i)
            m.d[clk] += combs[0].eq(i - combregs[0])
            for j in range(1,N):
                m.d[clk] += combregs[j].eq(combs[j-1])
                m.d[clk] += combs[j].eq(combs[j-1] - combregs[j])
            m.d[clk] += integrators[0].eq(integrators[0] + combs[-1])
        with m.If(o_ce):
            for j in range(1,N):
                m.d[clk] += integrators[j].eq(integrators[j] + integrators[j-1])

        assert len(self.o) == len(integrators[-1])

        m.d.comb += self.o.eq(integrators[-1])

        return m
