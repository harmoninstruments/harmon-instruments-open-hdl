#!/usr/bin/env python3
# Copyright 2020-2022 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

import numpy as np
import scipy
from scipy import signal
import matplotlib.pyplot as plt
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--ad7768', action='store_true', help="set params to AD7768 filter")
parser.add_argument('--PNG', action='store_true', help="save a PNG")
parser.add_argument('--PDF', action='store_true', help="save a PDF")
parser.add_argument('--N', type=int, help="number of CIC stages", default=4)
parser.add_argument('--R', type=int, help="rate change", default=4)
parser.add_argument('--fs', type=float, help="sample rate", default=1.0)
args = parser.parse_args()

R = args.R
N = args.N
fs = args.fs

if args.ad7768:
    R=32
    N=5
    fs=31.25e6/4.0

h1 = np.ones(R)
h = h1
for i in range(N-1):
    h = np.convolve(h, h1)
h *= 1.0/np.sum(h)
(w1,H1) = signal.freqz(h, worN=2048)

fig = plt.figure()
ax = fig.add_subplot(111)
ax.plot(w1*fs/(2.0*np.pi), 20*np.log10(np.abs(H1)))
ax.axis([0,fs/2.0,-120,3])
ax.grid('on')
ax.set_ylabel('Magnitude (dB)')
ax.set_xlabel('Frequency (Hz)')
ax.set_title(f'CIC Filter Frequency Response R={R}, N={N}')
plt.show()
