# Copyright (C) 2022 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth import *

def test_CIC_Decimate():
    from amaranth.sim import Simulator
    from DSP.CIC import CIC_Decimate
    import numpy as np
    idata = Signal(signed(8))
    i_ce = Signal()
    o_ce = Signal()
    dut = CIC_Decimate(i=idata, i_ce=i_ce, o_ce=o_ce, R=4, N=4)

    din = np.concatenate(
        (
            -128 * np.ones(16, dtype=int),
            127 * np.ones(16, dtype=int),
            0 * np.ones(16, dtype=int),
        )
    )

    response_expected = np.convolve(np.ones(4, dtype=int), np.ones(4, dtype=int))
    response_expected = np.convolve(response_expected, response_expected)
    expected = np.convolve(din, response_expected)

    def test_proc():
        for i, d in enumerate(din):
            # test input clock enable
            if i < 6:
                yield i_ce.eq(0)
                yield o_ce.eq(0)
                yield
            d = int(d)
            yield i_ce.eq(1)
            ovalid = (i&3) == 3
            yield o_ce.eq(1 if ovalid else 0)
            yield idata.eq(d)
            yield
            if ovalid:
                o = yield dut.o
                if i >= 8:
                    assert o == expected[i-8]

    sim = Simulator(dut)
    sim.add_clock(8e-9)
    sim.add_sync_process(test_proc)
    sim.run()

def test_CIC_Interpolate():
    from amaranth.sim import Simulator
    from DSP.CIC import CIC_Interpolate
    import numpy as np
    idata = Signal(signed(8))
    i_ce = Signal()
    o_ce = Signal()
    dut = CIC_Interpolate(i=idata, i_ce=i_ce, o_ce=o_ce, R=4, N=4)

    din = np.concatenate(
        (
            -128 * np.ones(4, dtype=int),
            127 * np.ones(4, dtype=int),
            0 * np.ones(4, dtype=int),
        )
    )
    din_stuffed = np.zeros(4*len(din))
    din_stuffed[::4] = din

    response_expected = np.convolve(np.ones(4, dtype=int), np.ones(4, dtype=int))
    response_expected = np.convolve(response_expected, response_expected)
    expected = np.convolve(din_stuffed, response_expected)

    def test_proc():
        for i, d in enumerate(din):
            d = int(d)
            yield idata.eq(d)
            yield o_ce.eq(1)
            for j in range(4):
                if j == 0:
                    yield i_ce.eq(1)
                else:
                    yield i_ce.eq(0)
                yield
                o = yield dut.o
                if i >= 5:
                    assert o == expected[4*(i-5)+j]
                # test o_ce
                if i < 4:
                    yield i_ce.eq(0)
                    yield o_ce.eq(0)
                    yield

    sim = Simulator(dut)
    sim.add_clock(8e-9)
    sim.add_sync_process(test_proc)
    sim.run()
