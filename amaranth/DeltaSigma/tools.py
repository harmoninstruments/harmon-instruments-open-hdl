#!/usr/bin/env python3
# Copyright 2020-2022 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

import numpy as np
import matplotlib.pyplot as plt

def enbw(w):
    """
    return the effective noise bandwidth of a window relative to bin spacing
    see http://www.mathworks.com/help/signal/ref/enbw.html
    """
    energy = np.sum(np.abs(w*w))
    area = np.abs(np.sum(w))**2
    return energy/area

def plot_psd(td, fs, label=''):
    window = np.kaiser(len(td), 20)
    bw = enbw(window)
    print("bw", bw*fs)
    tdw = td*window
    df = np.fft.fft(tdw)/len(tdw)
    f = np.fft.fftfreq(len(tdw), 1e6/fs)
    df = df[4:int(len(df)/2)]
    f = f[4:int(len(f)/2)]
    dfp = np.square(np.abs(df))
    dfp /= np.sum(dfp[:(len(dfp)//2)])
    dfp /= bw*fs
    plt.semilogx(f,10.0*np.log10(dfp), label=label)
    plt.xlabel('Frequency (MHz)')
    plt.ylabel('dBc/Hz')
    plt.title("Dither vs delta sigma")

def dsm_model(coefs, d, scale = 1.0, plot=True):
    rv = np.zeros(len(d))
    err = np.zeros(len(coefs))
    for i in range(len(d)):
        unq = scale*d[i] + np.sum(coefs*err)
        rv[i] = np.floor(unq)
        err[1:len(coefs)] = err[0:len(coefs)-1]
        err[0] = rv[i] - unq
    name = "{}".format(coefs)
    print("{}: min = {}, max = {}".format(name,min(rv),max(rv)))
    if plot:
        plot_psd(rv, 1e9, label=name)
    return rv

if __name__ == "__main__":
    n = np.arange(1024*32)
    t = n*1e-9
    noise = np.random.normal(loc=0,scale=.5,size=len(n))
    print("noise", np.sqrt(np.mean(np.square(noise))))

    d0 = np.sin(t*2*np.pi*123.456789e6)

    plot_psd(noise+d0, 1e9, 'noise')
    plot_psd(np.round(d0), 1e9, 'rounding')

    dsm_model([-1], d0, 17.0)
    dsm_model([-2,1], d0, 17.0)
    dsm_model([-3,3,-1], d0, 17.0)
    dsm_model([-4,6,-4,1], d0, 17.0)
    dsm_model([-5,10,-10,5,-1], d0, 17.0)
    dsm_model([-3,4,-3,1], d0, 17.0)

    plt.legend()
    plt.show()
