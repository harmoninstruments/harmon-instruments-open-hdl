#!/usr/bin/env python3
# Copyright 2015-2022 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth import *

def test_DSM_2():
    from tools import plot_psd, dsm_model
    import numpy as np
    from DSM_2 import DSM_2
    from amaranth.sim import Simulator

    n_stages = 4
    delay = 2*n_stages

    idata = Array(Signal(signed(12+n_stages), name="dsmi{}".format(a)) for a in range(2))
    dut = DSM_2(idata, n_stages = n_stages)

    assert dut.o[0].shape() == signed(12)

    f = 34.5678901234e6
    t = 1e-9
    samples = 128
    scale = 1000
    input_data = np.round(scale*np.cos(np.pi*2*f*np.arange(samples)*t))

    expected = dsm_model([-3,4,-3,1], input_data, scale=1/(2**n_stages), plot=False)

    def dut_test():
        data = np.zeros(samples)
        c = 0
        sumd = 0
        yield
        for i in range(len(data))[::2]:
            yield idata[0].eq(int(input_data[i]))
            yield idata[1].eq(int(input_data[i+1]))
            yield
            data[i] = yield dut.o[0]
            data[i+1] = yield dut.o[1]
        if False:
            import matplotlib.pyplot as plt
            plt.plot(data[delay:], label='data')
            plt.plot(expected, label='expected')
            plt.legend()
            plt.show()
        if np.max(np.abs((data[delay:] - expected[:-delay]))) == 0:
            return
        print("fail")
        print("expected", expected)
        print("result", data)
        assert False

    sim = Simulator(dut)
    sim.add_clock(2e-9, domain = 'sync')
    sim.add_sync_process(dut_test)
    with sim.write_vcd("dump.vcd"):
        sim.run()

if __name__ == "__main__":
    test_DSM_2()
