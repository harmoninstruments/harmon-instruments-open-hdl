#!/usr/bin/env python3
# Copyright 2015-2022 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth import *

def test_DSM():
    from tools import plot_psd, dsm_model
    import numpy as np
    import matplotlib.pyplot as plt
    from DSM import DSM
    from amaranth.sim import Simulator
    idata = Signal(signed(16))
    odata = Signal(signed(8))
    dut = DSM(idata, odata, C(1))

    f = 34.5678901234e6
    t = 2e-9
    samples = 256
    scale = 30000
    isamples = np.round(scale*np.cos(np.pi*2*f*np.arange(samples)*t))

    expected = dsm_model([-4,6,-4,1], isamples, scale=1/256.0, plot=False)
    print(expected)

    def dut_test():
        data = np.zeros(samples)
        c = 0
        sumd = 0
        for i in range(len(data)):
            yield idata.eq(int(isamples[i]))
            yield
            d = yield dut.o
            data[i] = d
        plt.plot(data, label='data')
        plt.plot(expected, label='expected')
        plt.legend()
        #plt.show()
        print(data)
        assert np.max(np.abs((data[1:] - expected[:-1]))) == 0

    sim = Simulator(dut)
    sim.add_clock(2e-9, domain = 'sync')
    sim.add_sync_process(dut_test)
    with sim.write_vcd("dump.vcd"):
        sim.run()

if __name__ == "__main__":
    test_DSM()
