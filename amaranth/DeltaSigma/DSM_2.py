# Copyright 2015-2022 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth import *

class DSM_2_stage(Elaboratable):
    """
    4th order delta sigma modulator, processes 2 samples per clock
    see tools.py
    this is the [-3, 4, -3, 1] modulator

    """

    def __init__(self, i, clk='sync'):
        n = len(i[0])
        self.i = i # array of 2 n bit signed
        self.o = Array(Signal(signed(n - 1), name=f"o{n-1}_{a}") for a in range(2))
        self.clk = clk

    def elaborate(self, platform):
        m = Module()
        coefs = [3, -4, 3, -1, 1]
        # sumval0 = 3err0 + -4err1 + 3err2 - err3 + in[0][0]
        err = Signal(4)
        sumval0 = Signal(range(-5,7))
        with m.Switch(Cat(err, self.i[0][0])):
            for i in range(2**(len(err)+1)):
                with m.Case(i):
                    v = 0
                    for j in range(len(coefs)):
                        if i & (1<<j):
                            v += coefs[j]
                    m.d.comb += sumval0.eq(v)

        # sumval1 = 3sumval0[0] + -4err0 + 3err1 - err2 + in[1][0]
        sumval1 = Signal(range(-5,7))
        with m.Switch(Cat(sumval0[0], err[:-1], self.i[1][0])):
            for i in range(2**(len(err)+1)):
                with m.Case(i):
                    v = 0
                    for j in range(len(coefs)):
                        if i & (1<<j):
                            v += coefs[j]
                    m.d.comb += sumval1.eq(v)

        m.d[self.clk] += [
            self.o[0].eq(self.i[0].shift_right(1)+sumval0.shift_right(1)),
            self.o[1].eq(self.i[1].shift_right(1)+sumval1.shift_right(1)),
            err[0].eq(sumval1[0]),
            err[1].eq(sumval0[0]),
            err[2:].eq(err[:-2]),
        ]
        return m

class DSM_2(Elaboratable):
    """
    Reduce the number of bits in a signal with delta sigma modulation

    The sample rate is twice the clock rate, 2 samples are passed each
    clock on i and o

    i is an array of 2 signed input values
    o is an array of 2 signed output values, n_stages bits smaller than i
    """

    def __init__(self, i, clk='sync', n_stages=6):
        self.stages = []
        for j in range(n_stages):
            self.stages.append(
                DSM_2_stage(
                    i=i if j==0 else self.stages[-1].o,
                    clk=clk
                )
            )
        self.o = self.stages[-1].o

    def elaborate(self, platform):
        m = Module()
        for j, stage in enumerate(self.stages):
            m.submodules[f'dsm_stage_{j}'] = stage
        return m
