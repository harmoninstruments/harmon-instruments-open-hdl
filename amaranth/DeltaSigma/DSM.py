#!/usr/bin/env python3
from amaranth import *

class DSM(Elaboratable):
    """
    Truncate bits with 4th order delta-sigma modulation
    see software/tools/dsm.py for design info
    this is the [-4, 6, -4, 1] modulator
    """
    def __init__(self, i, o, ce=C(1)):
        self.args = (i, ce)
        self.o = o # useful range is reduced by 7 on either side
    def elaborate(self, platform):
        (i, ce) = self.args
        m = Module()
        errsize = len(i) - len(self.o)
        err0 = Signal(errsize)
        err1 = Signal(errsize)
        err2 = Signal(errsize)
        err3 = Signal(errsize)
        sumval = Signal(len(i) + 1)
        # sumval = i + -err3 + 4err2 -6err1 + 4err0
        m.d.comb += sumval.eq(i + (-(err3) + (err2 << 2) + -((err1 << 2) + (err1 << 1)) + (err0 << 2)))
        with m.If(ce):
            m.d.sync += [
                err0.eq(sumval[:errsize]),
                err1.eq(err0),
                err2.eq(err1),
                err3.eq(err2),
                self.o.eq(sumval[errsize:]),
            ]
        return m
