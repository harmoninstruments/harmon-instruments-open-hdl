#!/usr/bin/env python3
# Copyright 2015-2019 Harmon Instruments, LLC
# SPDX-License-Identifier: GPL-3.0-or-later

from amaranth import *

class DSM4_stage(Elaboratable):
    def __init__(self, i):
        self.n = len(i[0])
        self.i = i # array of 4 n bit signed
        self.o = Array(Signal(signed(self.n), name="o{}".format(a)) for a in range(4))
    def elaborate(self, platform):
        q = Array(Signal(signed(4), name="q{}".format(a)) for a in range(4))
        q4 = Signal(4)
        i0b = Signal(4)
        ib = Array(Signal(signed(self.n-1), name="ib{}".format(a)) for a in range(4))
        c = Array(Signal(signed(4), name="q{}".format(a)) for a in range(4))
        m = Module()

        m.d.comb += [
            c[0][0].eq(q[0][0] ^ q[1][0] ^ q[3][0] ^ self.i[0][0]),
            c[1][0].eq(q[2][0] ^ q[0][0] ^ q[3][0] ^ self.i[0][0] ^ self.i[1][0]),
            c[2][0].eq(q[0][0] ^ self.i[0][0] ^ self.i[1][0] ^ self.i[2][0]),
            c[3][0].eq(q[1][0] ^ self.i[1][0] ^ self.i[2][0] ^ self.i[3][0]),
        ]

        for i in range(4):
            m.d.sync += [
                ib[i].eq(self.i[i][1:]),
                i0b[i].eq(i[i][0]),
                q[i][:2].eq(c[i][:2]),
                q4[i].eq(q[i][0]),
                self.o[i].eq(ib[0] + q0[1:4]), # sign extend
            ]
            m.d.comb += [
                q[i][2:4].eq(c[i][2:4]),
            ]
        m.submodules.lutl0 = Instance(
            "LUT5", p_INIT="32'hee8877ee",
            o_O = c[0][1],
            i_I0 = q[0][0], i_I1 = q[1][0], i_I2 = q[2][0], i_I3 = q[3][0], i_I4 = i[0][0])
        m.submodules.lutl1 = Instance(
            "LUT5",
            p_INIT = "32'hee8877ee",
            o_O = c[1][1],
            i_I0 = q[1][0], i_I1 = q[2][0], i_I2 = q[3][0], i_I3 = c[0][0], i_I4 = i[1][0])
        m.submodules.lutl2 = Instance(
            "LUT5",
            p_INIT = "32'hee8877ee",
            o_O = c[2][1],
            i_I0 = q[2][0], i_I1 = q[3][0], i_I2 = c[0][0], i_I3 = c[1][0], i_I4 = i[2][0])
        m.submodules.lutl3 = Instance(
            "LUT5",
            p_INIT = "32'hee8877ee",
            o_O = c[3][1],
            i_I0 = q[3][0], i_I1 = c[0][0], i_I2 = c[1][0], i_I3 = c[2][0],  i_I4 = i[3][0])
        m.submodules.luth0 = Instance(
            "LUT6_2",
            p_INIT = "64'h20b030f22db43cd2",
            o_O6 = c[0][3], o_O5 = c[0][2],
            i_I0 = q4[0], i_I1 = q4[1], i_I2 = q4[2], i_I3 = q4[3], i_I4 = i0b[0], i_I5 = C(1))
        m.submodules.luth1 = Instance(
            "LUT6_2",
            p_INIT = "64'h20b030f22db43cd2",
            o_O6 = c[1][3], o_O5 = c[1][2],
            i_I0 = q4[1], i_I1 = q4[2], i_I2 = q4[3], i_I3 = q[0][0], i_I4 = i0b[1], i_I5 = C(1))
        m.submodules.luth2 = Instance(
            "LUT6_2",
            p_INIT = "64'h20b030f22db43cd2",
            o_O6 = c[2][3], o_O5 = c[2][2],
            i_I0 = q4[2], i_I1 = q4[3], i_I2 = q[0][0], i_I3 = q[1][0], i_I4 = i0b[2], i_I5 = C(1))
        m.submodules.luth3 = Instance(
            "LUT6_2",
            p_INIT = "64'h20b030f22db43cd2",
            o_O6 = c[3][3], o_O5 = c[3][2],
            i_I0 = q4[3], i_I1 = q[0][0], i_I2 = q[1][0], i_I3 = q[2][0], i_I4 = i0b[3], i_I5 = C(1))
        return m

class DSM_4(Elaboratable):
    def __init__(self, i):
        self.i = i # 4x 16 b array
        self.o = Array(Signal(signed(12), name="dsmo{}".format(a)) for a in range(4))
    def elaborate(self, platform):
        m = Module()

        # buffer inputs
        ib0 = Array(Signal(signed(16), name="ib0{}".format(a)) for a in range(4))
        ib1 = Array(Signal(signed(12), name="ib1{}".format(a)) for a in range(4))
        ib2 = Array(Signal(signed(12), name="ib2{}".format(a)) for a in range(4))
        ib3 = Array(Signal(signed(12), name="ib3{}".format(a)) for a in range(4))
        ib4 = Array(Signal(signed(12), name="ib4{}".format(a)) for a in range(4))
        ib5 = Array(Signal(signed(12), name="ib5{}".format(a)) for a in range(4))
        ib6 = Array(Signal(signed(12), name="ib6{}".format(a)) for a in range(4))
        ib7 = Array(Signal(signed(12), name="ib7{}".format(a)) for a in range(4))
        ib8 = Array(Signal(signed(12), name="ib8{}".format(a)) for a in range(4))

        q = []

        for i in range(4):
            q.append(Array(Signal(signed(6-i), name="q{}{}".format(i, a)) for a in range(4)))

        for i in range[4]:
            m.d.sys2x += [
                ib0[i].eq(self.i[i]),
                ib1[i].eq(ib0[i][4:]),
                ib2[i].eq(ib1[i]),
                ib3[i].eq(ib2[i]),
                ib4[i].eq(ib3[i]),
                ib5[i].eq(ib4[i]),
                ib6[i].eq(ib5[i]),
                ib7[i].eq(ib6[i]),
                ib8[i].eq(ib7[i]),
                self.o[i].eq(ib8[i] + q[3][i]),
            ]

        s0 = m.submodules.dsms0 = DSM4_stage(ib0)
        s1 = m.submodules.dsms1 = DSM4_state(s0.o)
        s2 = m.submodules.dsms2 = DSM4_state(s1.o)
        s3 = m.submodules.dsms3 = DSM4_state(s2.o)
        return m
