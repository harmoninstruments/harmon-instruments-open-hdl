from amaranth import Instance, C, ClockSignal

def ff_io(d, q, ce=C(1), clr=C(0), clk='sync'):
    return Instance(
        "FDCE",
        a_IOB="TRUE",
        i_C=ClockSignal(clk),
        i_CE=ce,
        i_CLR=clr,
        i_D=d,
        o_Q=q,
    )

def iobuf(i, o, t, io):
    return Instance(
        "IOBUF",
        i_I=i,
        o_O=o,
        i_T=t,
        io_IO=io
    )

def iddr(i, o, clk='sync'):
    return Instance(
        "IDDRE1",
        p_DDR_CLK_EDGE="SAME_EDGE_PIPELINED",
        p_IS_CB_INVERTED=C(1),
        o_Q1=o[0],
        o_Q2=o[1],
        i_C=ClockSignal(clk),
        i_CB=ClockSignal(clk),
        i_D=i,
        i_R=C(0),
    )
