from amaranth import *

class Sysmon(Elaboratable):
    def __init__(self, pio, addr):
        self.alm = Signal(16)
        self.ot = Signal()
        self.adc_data = Signal(16)
        self.do = Signal(16)
        self.drdy = Signal()
        self.busy = Signal()
        self.channel = Signal(6)
        self.eoc = Signal()
        self.eos = Signal()
        self.muxaddr = Signal(5)

        self.rvalid = pio.add_ro_reg(addr, self.do, lsb=8, ack=self.drdy, name="Sysmon")
        self.pio = pio

    def elaborate(self, platform):
        vp_vn = platform.request("vp_vn", 0, dir='-')
        m = Module()

        # clock divider to produce 5 MHz for the sysmon from 125 MHz
        dclk_div = 25

        m.submodules.sysmon = Instance(
            "SYSMONE4",
            # config regs
            p_INIT_40=C(0,16),
            p_INIT_41=C(0x2F9F,16),
            p_INIT_42=C(dclk_div << 8,16),
            p_INIT_43=C(0x200F,16),
            p_INIT_44=C(0,16),
            p_INIT_45=C(0xA98E,16), # ANALOG_BUS partially documented in UG1087
            # sequence regs
            p_INIT_46=C(0x001F,16), # SEQ_CHANNEL2
            p_INIT_47=C(0x0000,16), # SEQ_AVERAGE2
            p_INIT_48=C(0x7FE0,16), # SEQ_CHANNEL0
            p_INIT_49=C(0,16), # SEQ_CHANNEL1 aux pins
            p_INIT_4A=C(0,16), # SEQ_AVERAGE0
            p_INIT_4B=C(0,16), # SEQ_AVERAGE1
            p_INIT_4C=C(0,16),
            p_INIT_4D=C(0,16),
            p_INIT_4E=C(0,16),
            p_INIT_4F=C(0,16),
            p_COMMON_N_SOURCE=C(0xffff,16),
            p_SIM_DEVICE="ZYNQ_ULTRASCALE" if platform.device == 'xck26' else "ULTRASCALE_PLUS",
            # user voltage monitor
            p_SYSMON_VUSER0_BANK=66,
            p_SYSMON_VUSER0_MONITOR="VCCO",
            p_SYSMON_VUSER1_BANK=224,
            p_SYSMON_VUSER1_MONITOR="AVCC",
            p_SYSMON_VUSER2_BANK=224,
            p_SYSMON_VUSER2_MONITOR="AVTT",
            p_SYSMON_VUSER3_BANK=224,
            p_SYSMON_VUSER3_MONITOR="MGTVCCAUX",
            o_ALM=self.alm,
            o_OT=self.ot,
            o_ADC_DATA=self.adc_data,
            # I2C
            o_I2C_SCLK_TS=Signal(),
            o_I2C_SDA_TS=Signal(),
            o_SMBALERT_TS=Signal(),
            # status
            o_BUSY=self.busy,
            o_CHANNEL=self.channel,
            o_EOC=self.eoc,
            o_EOS=self.eos,
            o_JTAGBUSY=Signal(),
            o_JTAGLOCKED=Signal(),
            o_JTAGMODIFIED=Signal(),
            o_MUXADDR=self.muxaddr,
            i_VAUXN=C(0,16),
            i_VAUXP=C(0,16),
            i_CONVST=C(0),
            i_CONVSTCLK=C(0),
            i_RESET=C(0),
            i_VN=vp_vn.n,
            i_VP=vp_vn.p,
            # DRP
            i_DADDR=self.pio.raddr[:8],
            i_DCLK=ClockSignal(),
            i_DEN=self.rvalid,
            i_DI=C(0,16),
            i_DWE=C(0),
            o_DO=self.do,
            o_DRDY=self.drdy,
            i_I2C_SCLK=C(0),
            i_I2C_SDA=C(0),
        )
        return m
