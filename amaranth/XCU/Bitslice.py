from amaranth import *

class BitsliceControl(Elaboratable):
    def __init__(
            self,
            pll_clk=C(0), # PLL clock input
            ref_clk=C(0), # global clock input
            en_vtc=C(0),  # voltage and temperature compensation
            tbyte_in=C(0xF,4),
            reset=C(0), # async global reset
            riu_addr=C(0,6),
            riu_wr_data = C(0,16),
            riu_wr_en = C(0),
            riu_clk = C(0),
            riu_sel = C(0),
            width = 8,
            serial=False,
            refclk_freq = 500.0, # MHz
            bisc=False,

    ):
        self.width = width
        self.refclk_freq = refclk_freq
        self.reset = reset
        self.rxc_to_bs = Array(Signal(40, name="rxc_to_bs{}".format(a)) for a in range(7))
        self.txc_to_bs = Array(Signal(40, name="txc_to_bs{}".format(a)) for a in range(7))
        self.txtri_to_bs = Signal(40)
        self.rxc_from_bs = Array(Signal(40, name="rxc_from_bs{}".format(a)) for a in range(7))
        self.txc_from_bs = Array(Signal(40, name="txc_from_bs{}".format(a)) for a in range(7))
        self.txtri_from_bs = Signal(40)

        self.vtc_rdy = Signal()
        self.dly_rdy = Signal()
        self.riu_valid = Signal() # last data written has been accepted when high
        self.riu_rd_data = Signal(16)

        self.inst = Instance(
            "BITSLICE_CONTROL",
            p_DIV_MODE="DIV2" if self.width == 4 else "DIV4",
            p_EN_CLK_TO_EXT_NORTH="DISABLE",
            p_EN_CLK_TO_EXT_SOUTH="DISABLE",
            p_EN_DYN_ODLY_MODE="FALSE", # Enable dynamic output delay mode
            p_EN_OTHER_NCLK="FALSE",
            p_EN_OTHER_PCLK="FALSE",
            p_IDLY_VT_TRACK="FALSE",
            p_ODLY_VT_TRACK="FALSE",
            p_QDLY_VT_TRACK="FALSE",
            p_READ_IDLE_COUNT=C(0,6),
            p_REFCLK_SRC="PLLCLK", # PLLCLK, REFCLK (RX only)
            p_ROUNDING_FACTOR=16, # for BISC, 8 - 128
            p_SELF_CALIBRATE= "ENABLE" if bisc else "DISABLE",
            p_SERIAL_MODE="TRUE" if serial else "FALSE",
            p_SIM_DEVICE="ULTRASCALE_PLUS",
            p_TX_GATING="DISABLE",
            o_CLK_TO_EXT_NORTH=Signal(),
            o_CLK_TO_EXT_SOUTH=Signal(),
            o_DLY_RDY=self.dly_rdy,
            o_DYN_DCI=Signal(7),
            o_NCLK_NIBBLE_OUT=Signal(),
            o_PCLK_NIBBLE_OUT=Signal(),
            o_RIU_RD_DATA=self.riu_rd_data,
            o_RIU_VALID=self.riu_valid,
            # 40-bit outputs to bitslices
            o_RX_BIT_CTRL_OUT0=self.rxc_to_bs[0],
            o_RX_BIT_CTRL_OUT1=self.rxc_to_bs[1],
            o_RX_BIT_CTRL_OUT2=self.rxc_to_bs[2],
            o_RX_BIT_CTRL_OUT3=self.rxc_to_bs[3],
            o_RX_BIT_CTRL_OUT4=self.rxc_to_bs[4],
            o_RX_BIT_CTRL_OUT5=self.rxc_to_bs[5],
            o_RX_BIT_CTRL_OUT6=self.rxc_to_bs[6],
            o_TX_BIT_CTRL_OUT0=self.txc_to_bs[0],
            o_TX_BIT_CTRL_OUT1=self.txc_to_bs[1],
            o_TX_BIT_CTRL_OUT2=self.txc_to_bs[2],
            o_TX_BIT_CTRL_OUT3=self.txc_to_bs[3],
            o_TX_BIT_CTRL_OUT4=self.txc_to_bs[4],
            o_TX_BIT_CTRL_OUT5=self.txc_to_bs[5],
            o_TX_BIT_CTRL_OUT6=self.txc_to_bs[6],
            o_TX_BIT_CTRL_OUT_TRI=self.txtri_to_bs,
            o_VTC_RDY=self.vtc_rdy,
            i_CLK_FROM_EXT=C(1,1), # high if unused
            i_EN_VTC=en_vtc,
            i_NCLK_NIBBLE_IN=C(0),
            i_PCLK_NIBBLE_IN=C(0),
            i_PHY_RDCS0=C(0,4), # for MIG only
            i_PHY_RDCS1=C(0,4), # for MIG only
            i_PHY_RDEN=C(0xF,4), # for MIG only
            i_PHY_WRCS0=C(0,4), # for MIG only
            i_PHY_WRCS1=C(0,4), # for MIG only
            i_PLL_CLK=pll_clk,
            i_REFCLK=ref_clk,
            i_RIU_ADDR=riu_addr,
            i_RIU_CLK=riu_clk,
            i_RIU_NIBBLE_SEL=riu_sel,
            i_RIU_WR_DATA=riu_wr_data,
            i_RIU_WR_EN=riu_wr_en,
            i_RST=reset,
            # 40 bit inputs from bitslices
            i_RX_BIT_CTRL_IN0=self.rxc_from_bs[0],
            i_RX_BIT_CTRL_IN1=self.rxc_from_bs[1],
            i_RX_BIT_CTRL_IN2=self.rxc_from_bs[2],
            i_RX_BIT_CTRL_IN3=self.rxc_from_bs[3],
            i_RX_BIT_CTRL_IN4=self.rxc_from_bs[4],
            i_RX_BIT_CTRL_IN5=self.rxc_from_bs[5],
            i_RX_BIT_CTRL_IN6=self.rxc_from_bs[6],
            i_TBYTE_IN=tbyte_in,
            # 40 bit inputs from bitslices
            i_TX_BIT_CTRL_IN0=self.txc_from_bs[0],
            i_TX_BIT_CTRL_IN1=self.txc_from_bs[1],
            i_TX_BIT_CTRL_IN2=self.txc_from_bs[2],
            i_TX_BIT_CTRL_IN3=self.txc_from_bs[3],
            i_TX_BIT_CTRL_IN4=self.txc_from_bs[4],
            i_TX_BIT_CTRL_IN5=self.txc_from_bs[5],
            i_TX_BIT_CTRL_IN6=self.txc_from_bs[6],
            i_TX_BIT_CTRL_IN_TRI=self.txtri_from_bs,
        )

    def elaborate(self, platform):
        m = Module()
        m.submodules.bitslice_control = self.inst
        return m
"""
        "RIU_OR",
        p_SIM_DEVICE="ULTRASCALE_PLUS",
        o_RIU_RD_DATA=RIU_RD_DATA,           # 16-bit output: RIU data bus to the controller
        o_RIU_RD_VALID=RIU_RD_VALID,
        i_RIU_RD_DATA_LOW=RIU_RD_DATA_LOW, # 16-bit
        i_RIU_RD_DATA_UPP=RIU_RD_DATA_UPP, # 16-bit
        i_RIU_RD_VALID_LOW=RIU_RD_VALID_LOW,
        i_RIU_RD_VALID_UPP=RIU_RD_VALID_UPP,
"""

def get_tx_bitslice(
        control,
        index,
        clk=C(0),
        cntvaluein=C(0,9),
        load=C(0), # load the delay
        d=C(0,8), # data is transmitted LSB first
        en_vtc=C(0),
        o=Signal(), # output to buffer
        t = C(0), # tristate in
):
    return Instance(
        "TX_BITSLICE",
        p_SIM_DEVICE="ULTRASCALE_PLUS",
        p_DATA_WIDTH=control.width,
        p_DELAY_FORMAT="COUNT", # COUNT or TIME
        p_DELAY_TYPE="VAR_LOAD", # FIXED, VARIABLE, VAR_LOAD
        p_DELAY_VALUE=0,
        p_ENABLE_PRE_EMPHASIS="FALSE",
        p_INIT=C(0,1), # initial output state
        p_OUTPUT_PHASE_90="FALSE",
        p_REFCLK_FREQUENCY=control.refclk_freq,
        p_TBYTE_CTL="T", # T or TBYTE_IN
        p_UPDATE_MODE="ASYNC", # delay update ASYNC, MANUAL, SYNC
        o_CNTVALUEOUT=Signal(9), # delay value
        o_O=o,
        o_RX_BIT_CTRL_OUT=control.rxc_from_bs[index],
        o_TX_BIT_CTRL_OUT=control.txc_from_bs[index],
        o_T_OUT=Signal(),
        i_CE=C(0), # for odelay
        i_CLK=clk, # for odelay
        i_CNTVALUEIN=cntvaluein,
        i_D=d,
        i_EN_VTC=en_vtc,
        i_INC=C(0),
        i_LOAD=load,
        i_RST=control.reset, # async reset
        i_RST_DLY=control.reset, # sync de-assert
        i_RX_BIT_CTRL_IN=control.rxc_to_bs[index],
        i_T=t,           # from device logic
        i_TBYTE_IN=C(0), # from TX_BITSLICE_TRI
        i_TX_BIT_CTRL_IN=control.txc_to_bs[index],
    )

def get_rx_bitslice(
        control,
        index,
        clk=C(0),
        cntvaluein=C(0,9),
        load=C(0), # load the delay
        load_ext=C(0),
        d=C(0,8),
        en_vtc=C(0),
        q=Signal(8), # output data
        fifo_empty = Signal(),
        fifo_rd_clk = Signal(),
        fifo_rd_en = Signal(),
        datain=Signal(),
        cascade="FALSE",
        data_type="DATA", # CLOCK, DATA, DATA_AND_CLOCK, SERIAL
        delay_format = "COUNT", # COUNT or TIME
):
    return Instance(
        "RX_BITSLICE",
        p_CASCADE=cascade,
        p_DATA_TYPE=data_type,
        p_DATA_WIDTH=control.width,
        p_DELAY_FORMAT=delay_format,
        p_DELAY_TYPE="VAR_LOAD", # FIXED, VARIABLE, VAR_LOAD
        p_DELAY_VALUE=0, # Input delay value setting in ps
        p_DELAY_VALUE_EXT=0, # Value of the extended input delay value in ps
        p_FIFO_SYNC_MODE="FALSE", # Always set to FALSE. TRUE is reserved for later use.
        p_REFCLK_FREQUENCY=control.refclk_freq,
        p_SIM_DEVICE="ULTRASCALE_PLUS",
        p_UPDATE_MODE="ASYNC",          # delay will take effect ASYNC, MANUAL, SYNC
        p_UPDATE_MODE_EXT="ASYNC",       # extended input delay will take effect ASYNC, MANUAL, SYNC
        o_CNTVALUEOUT=Signal(9),
        o_CNTVALUEOUT_EXT=Signal(9),
        o_FIFO_EMPTY=fifo_empty,
        o_FIFO_WRCLK_OUT=Signal(), #currently unsupported, do not connect
        o_Q=q,
        o_RX_BIT_CTRL_OUT=control.rxc_to_bs[index],
        o_TX_BIT_CTRL_OUT=control.txc_to_bs[index],
        i_CE=C(0),
        i_CE_EXT=C(0),
        i_CLK=clk,
        i_CLK_EXT=clk,
        i_CNTVALUEIN=cntvaluein,
        i_CNTVALUEIN_EXT=cntvaluein,
        i_DATAIN=datain,
        i_EN_VTC=C(0),
        i_EN_VTC_EXT=C(0),
        i_FIFO_RD_CLK=fifo_rd_clk,
        i_FIFO_RD_EN=fiof_rd_en,
        i_INC=C(0),
        i_INC_EXT=C(0),
        i_LOAD=load,
        i_LOAD_EXT=load_ext,
        i_RST=control.reset,
        i_RST_DLY=control.reset,
        i_RST_DLY_EXT=control.reset,
        i_RX_BIT_CTRL_IN=control.rxc_from_bs[index],
        i_TX_BIT_CTRL_IN=control.txc_from_bs[index],
   )

def get_rxtx_bitslice(
        control,
        index,
        clk=C(0),
        cntvaluein=C(0,9),
        rx_load=C(0), # load the delay
        tx_load=C(0),
        d=C(0,8),
        datain=C(0),
        o=Signal(), # serialized output
        t_out=Signal(),
        en_vtc=C(0),
        q=Signal(8), # output data
        t = C(0),
        fifo_empty = Signal(),
        fifo_rd_clk = Signal(),
        fifo_rd_en = Signal(),
        cascade="FALSE",
        data_type="DATA", # CLOCK, DATA, DATA_AND_CLOCK, SERIAL
        delay_format="COUNT", # COUNT or TIME
        wrclk_out=Signal(), # reserved
        loopback=False,
        rx_delay = 0,
        tx_delay = 0,
):
    return Instance(
        "RXTX_BITSLICE",
        p_ENABLE_PRE_EMPHASIS="FALSE",
        p_FIFO_SYNC_MODE="FALSE", # reserved
        p_INIT=C(1,1),
        p_RX_DATA_TYPE=data_type,
        p_RX_DATA_WIDTH=control.width,
        p_RX_DELAY_FORMAT=delay_format,
        p_RX_DELAY_TYPE="VAR_LOAD", # FIXED, VARIABLE, VAR_LOAD
        p_RX_DELAY_VALUE=rx_delay,
        p_RX_REFCLK_FREQUENCY=control.refclk_freq,
        p_RX_UPDATE_MODE="ASYNC",
        p_SIM_DEVICE="ULTRASCALE_PLUS",
        p_TBYTE_CTL="T", # T or TBYTE_IN
        p_TX_DATA_WIDTH=control.width,
        p_TX_DELAY_FORMAT=delay_format,
        p_TX_DELAY_TYPE="VAR_LOAD", # FIXED, VARIABLE, VAR_LOAD
        p_TX_DELAY_VALUE=tx_delay,
        p_TX_OUTPUT_PHASE_90="FALSE",
        p_TX_REFCLK_FREQUENCY=control.refclk_freq,
        p_TX_UPDATE_MODE="ASYNC",
        p_LOOPBACK="TRUE" if loopback else "FALSE",
        o_FIFO_EMPTY=fifo_empty,
        o_FIFO_WRCLK_OUT=wrclk_out,
        o_O=o,
        o_Q=q,
        o_RX_BIT_CTRL_OUT=control.rxc_from_bs[index],
        o_RX_CNTVALUEOUT=Signal(9),
        o_TX_BIT_CTRL_OUT=control.txc_from_bs[index],
        o_TX_CNTVALUEOUT=Signal(9),
        o_T_OUT=t_out,
        i_D=d,
        i_DATAIN=datain,
        i_FIFO_RD_CLK=fifo_rd_clk,
        i_FIFO_RD_EN=fifo_rd_en,
        i_RX_BIT_CTRL_IN=control.rxc_to_bs[index],
        i_RX_CE=C(0),
        i_RX_CLK=clk,
        i_RX_CNTVALUEIN=cntvaluein,
        i_RX_EN_VTC=en_vtc,
        i_RX_INC=C(0),
        i_RX_LOAD=rx_load,
        i_RX_RST=control.reset,
        i_RX_RST_DLY=control.reset,
        i_T=t,
        i_TBYTE_IN=C(0),
        i_TX_BIT_CTRL_IN=control.txc_to_bs[index],
        i_TX_CE=C(0),
        i_TX_CLK=clk,
        i_TX_CNTVALUEIN=cntvaluein,
        i_TX_EN_VTC=en_vtc,
        i_TX_INC=C(0),
        i_TX_LOAD=tx_load,
        i_TX_RST=control.reset,
        i_TX_RST_DLY=control.reset,
    )
