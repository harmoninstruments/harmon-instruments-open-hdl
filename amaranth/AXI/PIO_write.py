# AXI3 to a simple local bus
# ignores AXI3 WID
# Copyright 2019-2022 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth import *
from amaranth.utils import log2_int

class AXI_PIO_write(Elaboratable):
    def __init__(self, axi, local):
        self.axi = axi
        self.local = local # this is an AXI stream with addr, data fields

    def elaborate(self, platform):
        m = Module()

        l = self.local
        w = self.axi.w
        aw = self.axi.aw
        b = self.axi.b

        trimlsb = log2_int(len(w.data)//8)

        state = Signal(2)

        m.d.comb += [
            aw.ready.eq(state==0),
            l.data.eq(w.data),
            l.valid.eq(w.valid & (state == 1)),
            w.ready.eq((state == 1) & l.ready),
            b.id.eq(aw.id),
            b.resp.eq(0),
        ]

        m.d[self.axi.clk] += [
            b.valid.eq((w.accept & w.last) | (b.valid & ~b.ready)),
        ]

        with m.If(state == 0):
            m.d[self.axi.clk] += state.eq(aw.valid)
            m.d[self.axi.clk] += l.addr.eq(aw.addr[trimlsb:len(l.addr)+trimlsb])
        with m.Elif(state == 1):
            with m.If(w.accept & w.last):
                m.d[self.axi.clk] += state.eq(2)
            with m.If(w.accept):
                m.d[self.axi.clk] += l.addr.eq(l.addr + 1)
        with m.Else():
            with m.If(b.accept):
                m.d[self.axi.clk] += state.eq(0)

        return m
