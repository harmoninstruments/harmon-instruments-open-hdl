# Copyright 2014 - 2022 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD
from amaranth import *
from amaranth.utils import log2_int

# AXI to a simple local bus
# low address bits are removed

class AXI_PIO_read(Elaboratable):
    def __init__(self, axi, a, d, v, r):
        self.axi = axi
        self.rdata = d # response data
        self.raddr = a # address with low (sub word size) bits clipped off
        self.rvalid = v # indicates address is valid, asserts one cycle for each read
        self.rready = r # indicates response data valid, assert one cycle

    def elaborate(self, platform):
        m = Module()

        ar = self.axi.ar
        r = self.axi.r

        # our output address is not a byte address but a multiple of
        # the bus width, so trim the low bits off
        alow = log2_int(len(r.data) // 8)

        lenbits = len(ar.len)
        endstate = (2**lenbits) - 1

        state = Signal(lenbits)
        idle = Signal()

        m.d.comb += [
            idle.eq(state == 0),
            ar.ready.eq(idle),
            r.resp.eq(0),
        ]

        with m.If(idle):
            with m.If(ar.valid):
                m.d[self.axi.clk] += [
                    r.id.eq(ar.id),
                    self.raddr.eq(ar.addr[alow:alow+len(self.raddr)]),
                    state.eq(endstate - ar.len),
                    self.rvalid.eq(1),
                ]
        with m.Else():
            m.d[self.axi.clk] += [
                self.raddr.eq(self.raddr + (r.ready & r.valid)),
                self.rvalid.eq(r.ready & r.valid & (state != endstate)),
                state.eq(state + (r.ready & r.valid)),
                r.last.eq(state == endstate),
            ]
            with m.If(self.rready):
                m.d[self.axi.clk] += [
                    r.data.eq(self.rdata),
                    r.valid.eq(1),
                ]
            with m.Elif(r.ready & r.valid):
                m.d[self.axi.clk] += r.valid.eq(0)

        return m
