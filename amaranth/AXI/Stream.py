# Copyright 2014 - 2019 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth import *

class dotdict(dict):
    """dot.notation access to dictionary attributes"""
    __getattr__ = dict.get
    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__

class AXIStream():
    def __init__(self, name, u, d=[], sim_queue=None, sim_alwaysready=False):
        self.name = name
        # upstream driven signals
        self.u = dotdict({"valid":Signal(name=name+'valid', reset=0)})
        self.u_raw = u
        # downstream driven signals
        self.d = dotdict({"ready":Signal(name=name+'ready', reset=0)})
        for n,bits in u:
            self.u[n] = Signal(bits, reset=0, name=name+n)
        for n,bits in d:
            self.d[n] = Signal(bits, reset=0, name=name+n)
        self.accept = self.valid & self.ready
        self.sim_queue = sim_queue
        self.sim_alwaysready = False

    def __getattr__(self, attr):
        if attr == 'ready':
            return self.d[attr]
        return self.u[attr]

    # returns all upstream driven signals concatenated
    def get_upstream_concat(self):
        sigs = []
        for key in sorted(self.u.keys()):
            if key == 'valid':
                continue
            sigs.append(self.u[key])
        return Cat(sigs)

    def get_zynq_ports(self, name, zynq_is_upstream=True, uppercase=False):
        ports = {}
        pre_u = 'o_' if zynq_is_upstream else 'i_'
        pre_d = 'i_' if zynq_is_upstream else 'o_'
        for u in self.u:
            if uppercase:
                u = u.upper()
            ports[pre_u+name+u] = self.u[u]
        for d in self.d:
            if uppercase:
                d = d.upper()
            ports[pre_d+name+d] = self.d[d]
        return ports

    def sim_writer(self):
        from amaranth.sim import Simulator, Passive
        while True:
            yield Passive()
            try:
                v = self.sim_queue.get(False)
                for key in v:
                    yield self.u[key].eq(v[key])
                yield self.valid.eq(1)
                yield
                while (yield self.ready) != 1:
                    yield
            except GeneratorExit:
                return
            except:
                yield self.valid.eq(0)
                yield

    def sim_reader(self):
        from amaranth.sim import Simulator, Passive
        import random
        yield Passive()
        while True:
            ready = True
            if not self.sim_alwaysready:
                ready = random.choice((False,True,True))
                yield self.ready.eq(ready)
            yield
            valid = yield self.u['valid']
            if not (ready and valid):
                continue
            d = {}
            for key in self.u:
                d[key] = yield self.u[key]
            self.sim_queue.put(d)
