# Copyright 2019-2021 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth import *
import random

# tausme2.ps / boost/random/linear_feedback_shift.hpp
class LFSR(Elaboratable):
    def __init__(self, w, k, q, s, resetval=None):
        if resetval is None:
            resetval = random.randint(999, 2**w-1)
        self.o = Signal(w, reset=resetval)
        self.nextval = Cat(
            self.o[k-(s+q):w-q] ^ self.o[k-s:], self.o[w-k:w-s])
    def elaborate(self, platform):
        m = Module()
        m.d.sync += self.o.eq(self.nextval)
        return m

# Tausme2.ps Table 2 first line
class PsuedoRandomBitSeqence64(Elaboratable):
    def __init__(self):
        self.o = Signal(64)
    def elaborate(self, platform):
        m = Module()
        g1 = m.submodules.gen1 = LFSR(64, 63,  5, 24)
        g2 = m.submodules.gen2 = LFSR(64, 58, 19, 13)
        g3 = m.submodules.gen3 = LFSR(64, 55, 24,  7)
        m.d.sync += self.o.eq(g1.o ^ g2.o ^ g3.o)
        return m

class PseudoRandomBitSequence(Elaboratable):
    def __init__(self):
        self.nbits = 0
        self.requests = []
        self.elaborated = False
    def get(self, bits):
        assert(self.elaborated==False)
        self.nbits += bits
        v = Signal(bits)
        self.requests.append(v)
        return v
    def elaborate(self, platform):
        m = Module()
        print("PRBS: {} bits".format(self.nbits))
        while (self.nbits & 63) != 0:
            self.nbits += 1
        urnd = Signal(self.nbits)
        for i in range(self.nbits//64):
            sm = m.submodules["prbs_"+str(i)] = PsuedoRandomBitSeqence64()
            m.d.comb += urnd[64*i:64*(i+1)].eq(sm.o)
        bit = 0
        for r in self.requests:
            m.d.comb += r.eq(urnd[bit:bit+len(r)])
            bit += len(r)
        self.elaborated = True
        return m
