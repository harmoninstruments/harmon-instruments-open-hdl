# Copyright 2019-2021 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

from amaranth import *
from Random.PseudoRandomBitSequence import *

# derived from boost/random/linear_feedback_shift.hpp
class Model():
    def __init__(self, reset=999):
        self.value = reset
    def update_linear_feedback_shift(self, w, k, q, s):
        wordmask = (1<<w)-1
        b = (((self.value << q) ^ self.value) & wordmask) >> (k-s)
        mask = (wordmask << (w-k)) & wordmask
        self.value = wordmask & (((self.value & mask) << s) ^ b)
        return self.value

def test_prbs():
    dut = LFSR(64, 63,  5, 24, resetval = 999)
    model = Model(999)
    from amaranth.sim import Simulator, Settle
    sim = Simulator(dut)
    sim.add_clock(4e-9)

    def test_proc():
        for i in range(40):
            o = yield dut.o
            expected = model.value
            model.update_linear_feedback_shift(64, 63, 5, 24)
            print("val{} = {:016x} {:016x}".format(i, o, expected))
            if i>3:
                assert o == expected
            yield

    sim.add_sync_process(test_proc)
    ports = [dut.o]
    with sim.write_vcd("dump.vcd", "dump.gtkw", traces=ports):
        sim.run()
