# Copyright 2020 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

import os
from amaranth import *
from PIO.Generic import PIO_Generic

# a is instr (RO)
# b is data (RW)
class BlockRams(Elaboratable):
    def __init__(self, cpu):
        self.cpu = cpu
        self.a_rdata = Signal(32)
        self.b_rdata = Signal(32)
    def elaborate(self, platform):
        cpu = self.cpu
        m = Module()
        init = {} # block ram initialization
        if cpu.programfile is not None:
            with open(cpu.programfile, 'rb') as f:
                d = f.read()
            assert len(d) < 4096
            for i in range(128):
                v = 0
                for j in range(32):
                    n = 32*i+j
                    if n >= len(d):
                        break
                    v1 = int(d[32*i+j]) & 0xFF
                    v |= v1 << (8*j)
                if v != 0:
                    init["p_INIT_{:02X}".format(i)] = C(v,256)

        address_out_of_range = Signal()
        m.d.comb += address_out_of_range.eq(cpu.dbus_addr[28:] != 0)
        mask_gated = Signal(4)
        m.d.comb += mask_gated.eq(Mux(address_out_of_range, 0, cpu.dbus_mask))

        m.submodules.rom = Instance(
            "RAMB36E1",
            p_DOA_REG=0,
            p_DOB_REG=0,
            p_RAM_MODE="TDP",
            p_READ_WIDTH_A=36,
            p_READ_WIDTH_B=36,
            p_WRITE_WIDTH_A=0,
            p_WRITE_WIDTH_B=36,
            p_SIM_DEVICE="7SERIES",
            **init,
            o_CASCADEOUTA=Signal(),
            o_CASCADEOUTB=Signal(),
            o_DBITERR=Signal(),
            o_ECCPARITY=Signal(8),
            o_RDADDRECC=Signal(9),
            o_SBITERR=Signal(),
            # out
            o_DOADO=self.a_rdata,
            o_DOPADOP=Signal(4, name='dopa'),
            o_DOBDO=self.b_rdata,
            o_DOPBDOP=Signal(4, name='dopb'),
            # unused inputs
            i_CASCADEINA=C(0),
            i_CASCADEINB=C(0),
            i_INJECTDBITERR=C(0),
            i_INJECTSBITERR=C(0),
            # port A inputs
            i_ADDRARDADDR=Cat(C(0,5), cpu.ibus_addr[2:12], C(0)),
            i_CLKARDCLK=ClockSignal(cpu.clk),
            i_ENARDEN=cpu.ibus_valid,
            i_REGCEAREGCE=C(0),
            i_RSTRAMARSTRAM=C(0),
            i_RSTREGARSTREG=C(0),
            i_WEA=C(0,4),
            i_DIADI=C(0,32),
            i_DIPADIP=C(0,4),
            # port B inputs
            i_ADDRBWRADDR=Cat(C(0,5), cpu.dbus_addr[2:12], C(0)),
            i_CLKBWRCLK=ClockSignal(cpu.clk),
            i_ENBWREN=cpu.dbus_cmd_valid,
            i_REGCEB=C(0),
            i_RSTRAMB=address_out_of_range,
            i_RSTREGB=C(0),
            i_WEBWE=Cat(mask_gated,C(0,4)),
            i_DIBDI=cpu.dbus_cmd_data,
            i_DIPBDIP=C(0,4),
        )
        return m

class RiscV(PIO_Generic):
    def __init__(self, reset, clk='sync', programfile=None, rdata_or=C(0)):
        self.base_init(name="RV", abits=30)
        self.programfile=programfile
        self.reset = reset
        self.clk = clk
        self.rdata_or = rdata_or
        self.wmask = Signal(4)
        # out from CPU
        self.ibus_valid = Signal()
        self.ibus_addr = Signal(32)
        self.dbus_cmd_valid = Signal()
        self.dbus_addr = Signal(32)
        self.dbus_mask = Signal(4)
        self.dbus_cmd_data = Signal(32)
        self.dbus_cmd_wr = Signal()

    def elaborate(self, platform):
        m = Module()

        # in from us
        ibus_rsp_valid = Signal()

        dbus_payload_size = Signal(2)
        dbus_rsp_valid = Signal()

        ram = m.submodules.ram = BlockRams(cpu = self)

        m.d[self.clk] += [
            ibus_rsp_valid.eq(self.ibus_valid),
            dbus_rsp_valid.eq(self.dbus_cmd_valid),
        ]

        # payload_size: 0 = 1 byte, 1 = 2 bytes default: 4 bytes
        with m.If(self.dbus_cmd_wr & self.dbus_cmd_valid):
            with m.Switch(dbus_payload_size):
                with m.Case(0): # 1 byte
                    m.d.comb += self.dbus_mask.eq(1 << self.dbus_addr[0:2])
                with m.Case(1): # 2 bytes
                    m.d.comb += self.dbus_mask.eq(3 << self.dbus_addr[0:2])
                with m.Case(): # 4 bytes
                    m.d.comb += self.dbus_mask.eq(0xF)
        with m.Else():
            m.d.comb += self.dbus_mask.eq(0)

        rvalid_prev = Signal()
        m.d[self.clk] += rvalid_prev.eq(self.rvalid)

        ram_prev = Signal()
        m.d[self.clk] += ram_prev.eq(self.rvalid & (self.dbus_addr[28:32] == 0))

        m.submodules.cpu = Instance(
            "VexRiscv",
            # ibus
            o_iBus_cmd_valid = self.ibus_valid,
            i_iBus_cmd_ready = C(1), # we don't ever need to stall
            o_iBus_cmd_payload_pc = self.ibus_addr,
            i_iBus_rsp_valid = ibus_rsp_valid,
            i_iBus_rsp_payload_error = C(0), # we just alias if it's out of range
            i_iBus_rsp_payload_inst = ram.a_rdata,
            # dbus
            o_dBus_cmd_valid = self.dbus_cmd_valid,
            i_dBus_cmd_ready = C(1),
            o_dBus_cmd_payload_wr = self.dbus_cmd_wr,
            o_dBus_cmd_payload_address = self.dbus_addr,
            o_dBus_cmd_payload_data = self.dbus_cmd_data,
            o_dBus_cmd_payload_size = dbus_payload_size, # 2
            i_dBus_rsp_ready = C(1),
            i_dBus_rsp_error = C(0),
            i_dBus_rsp_data = ram.b_rdata | self.rdata_or | self.rdata,
            i_clk = ClockSignal(self.clk),
            i_reset = self.reset,
        )

        m.d.comb += [
            self.raddr.eq(self.dbus_addr[2:32]),
            self.rvalid.eq(self.dbus_cmd_valid & ~self.dbus_cmd_wr),
        ]

        m.d.sync += [
            self.waddr.eq(self.dbus_addr[2:32]),
            self.wvalid.eq(self.dbus_cmd_wr & self.dbus_cmd_valid),
            self.wdata.eq(self.dbus_cmd_data),
            self.wmask.eq(self.dbus_mask),
        ]

        m = self.base_elaborate(m)

        return m
