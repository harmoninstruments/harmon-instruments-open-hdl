// This is free and unencumbered software released into the public domain.
//
// Anyone is free to copy, modify, publish, use, compile, sell, or
// distribute this software, either in source code form or as a compiled
// binary, for any purpose, commercial or non-commercial, and by any
// means.

#include "firmware.h"

extern "C" void main(void)
{
        *((volatile uint32_t *)0x10000010) = 9;
        *((volatile uint32_t *)0x10000010) = 11;
        __sync_synchronize();
        //print_str("hello world1\n");
        (*((volatile uint32_t *)0x10000010))++;
        __sync_synchronize();
        __sync_synchronize();
        print_str("hello world2\n");
        print_hex(0xCAFE, 4);
        print_hex(*(uint32_t *) 0x10000004, 8);
        print_str("\n");
        print_str("hello world3\n");
        stats();
        print_str("hello world4\n");
        while(1);
}
