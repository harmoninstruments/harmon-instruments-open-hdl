# Copyright (C) 2014-2020 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

import os.path
from amaranth_cocotb import run
from amaranth import *

import cocotb
from cocotb.clock import Clock
from cocotb.triggers import Timer, RisingEdge, ReadOnly, Event
from cocotb.result import ReturnValue

from Memory.SDP import *

def test_memory_sdp_8_32_generic(primitive = 'generic'):
    m = Module()
    dut = m.submodules.dut = Memory_SDP(wbits=8, rbits=32, primitive=primitive, reset_out=True)
    sync = m.domains.sync = ClockDomain("sync", reset_less=True)
    run(m,
        'Memory.SDP_test',
        ports=[sync.clk, dut.wvalid, dut.wdata, dut.waddr, dut.rvalid, dut.rdata, dut.raddr],
        vcd_file='dump.vcd',
        verilog_sources=[os.path.expandvars("$XPRIMS/RAMB36E1.v"),]
    )

def test_memory_sdp_8_32_ramb36e1():
    test_memory_sdp_8_32_generic('BRAMB36E1')

@cocotb.coroutine
def read(dut, addr):
    dut.raddr = addr
    dut.rvalid = 1
    yield RisingEdge(dut.clk)
    dut.rvalid = 0
    yield RisingEdge(dut.clk)
    rv = int(dut.rdata)
    print("read", hex(rv), 'at', addr)
    raise ReturnValue(rv)

@cocotb.coroutine
def write(dut, addr, data):
    dut.waddr = addr
    dut.wdata = data
    dut.wvalid = 1
    yield RisingEdge(dut.clk)
    dut.wvalid = 0

@cocotb.test()
def run_test(dut):
    dut.clk = 0
    dut.wvalid = 0
    dut.rvalid = 0
    yield Timer(4000)
    cocotb.fork(Clock(dut.clk, 8000).start())
    yield RisingEdge(dut.clk)
    for i in range(10):
        yield RisingEdge(dut.clk)
    yield write(dut, 3, 0xDE)
    yield write(dut, 0, 0xFF)
    rv = yield read(dut,0)
    assert rv == 0xDE0000FF
    rv = yield read(dut,3)
    assert rv == 0
