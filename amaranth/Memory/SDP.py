# Copyright (C) 2020 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD
# A simple dual port async memory wrapper with wrappers for FPGA primitives

from amaranth import *
import math

# reg: if False, 1 cycle latency, 2 cycles if true
# reset_out: if True, output is only non-zero for 1 clock after each read, else keeps last read value
# does not currently support byte enables

class Memory_SDP(Elaboratable):
    def __init__(self, wbits, rbits, wclk='sync', rclk='sync', primitive='generic',
                 reset_out=False, reg=False):
        self.primitive = primitive # generic or XC7
        self.wclk = wclk
        self.wvalid = Signal()
        self.waddr = Signal(self._abits(wbits))
        self.wdata = Signal(wbits)
        self.rclk = rclk
        self.rvalid = Signal()
        self.raddr = Signal(self._abits(rbits))
        self.rdata = Signal(rbits)
        self.reset_out = reset_out
        self.reg = reg
        if wbits > rbits:
            assert (wbits % rbits) == 0
            assert False # support for this case not yet implemented
        else:
            assert (rbits % wbits) == 0

    def _abits(self, dbits, size=32768):
        # on most FPGAs, multiples of 9 bits
        if dbits >= 9:
            size = (size * 9) // 8
        return int(math.log2(size/dbits))
    def _round_up_bits(self, bits):
        if bits < 8:
            return bits
        n=9
        while True:
            if n >= bits:
                return n
            n *= 2

    def elaborate_RAMB36E1(self, platform):
        m = Module()
        wdata = Signal(72)
        rdata = Signal(72)
        m.d.comb += [
            wdata.eq(self.wdata),
            self.rdata.eq(rdata[:len(self.rdata)]),
        ]
        en_next = Signal(reset=1)
        if self.reset_out:
            m.d[self.rclk] += en_next.eq(self.rvalid)
        # SDP: A is read, B is write
        m.submodules.ram = Instance(
            "RAMB36E1",
            p_RAM_MODE="SDP" if max(len(self.wdata),len(self.rdata)) > 36 else "TDP",
            p_READ_WIDTH_A=self._round_up_bits(len(self.rdata)),
            p_READ_WIDTH_B=0,
            p_WRITE_WIDTH_A=0,
            p_WRITE_WIDTH_B=self._round_up_bits(len(self.wdata)),
            p_SIM_DEVICE="7SERIES",
            o_CASCADEOUTA=Signal(), o_CASCADEOUTB=Signal(),
            o_DBITERR=Signal(), o_ECCPARITY=Signal(8), o_RDADDRECC=Signal(9),
            o_SBITERR=Signal(),
            # out
            o_DOADO=rdata[0:32],
            o_DOPADOP=rdata[32:36],
            o_DOBDO=rdata[36:68],
            o_DOPBDOP=rdata[68:72],
            # unused inputs
            i_CASCADEINA=C(0), i_CASCADEINB=C(0),
            i_INJECTDBITERR=C(0), i_INJECTSBITERR=C(0),
            # port A inputs
            i_ADDRARDADDR=Cat(C(0,15-len(self.raddr)), self.raddr, C(0)),
            i_CLKARDCLK=ClockSignal(self.rclk),
            i_ENARDEN=en_next | self.rvalid if self.reset_out else self.rvalid,
            i_REGCEAREGCE=C(0),
            i_RSTRAMARSTRAM=~self.rvalid if self.reset_out else C(0),
            i_RSTREGARSTREG=C(0),
            i_WEA=C(0,4),
            i_DIADI=wdata[36:68],
            i_DIPADIP=wdata[68:72],
            # port B inputs
            i_ADDRBWRADDR=Cat(C(0,15-len(self.waddr)), self.waddr, C(0)),
            i_CLKBWRCLK=ClockSignal(self.wclk),
            i_ENBWREN=self.wvalid,
            i_REGCEB=C(0),
            i_RSTRAMB=C(0),
            i_RSTREGB=C(0),
            i_WEBWE=C(0xFF,8),
            i_DIBDI=wdata[0:32],
            i_DIPBDIP=wdata[32:36],
        )
        return m
    def elaborate(self, platform):
        if 'RAMB36E1' in self.primitive:
            return self.elaborate_RAMB36E1(platform)
        # generic
        m = Module()
        bits = max(len(self.wdata), len(self.rdata))
        ram = Memory(
            width=bits, depth=2**max(len(self.raddr), len(self.raddr)))
        wrport = m.submodules.wrport = ram.write_port(
            domain=self.wclk, granularity=min(len(self.wdata),len(self.rdata)))
        rdport = m.submodules.rdport = ram.read_port (domain=self.rclk, transparent=False)
        m.d.comb += wrport.data.eq(Repl(self.wdata, bits//len(self.wdata)))
        if len(self.wdata) == len(self.rdata):
            m.d.comb += wrport.en.eq(self.wvalid)
            m.d.comb += wrport.addr.eq(self.waddr)
        elif len(self.wdata) * 4 == len(self.rdata):
            m.d.comb += wrport.addr.eq(self.waddr[2:])
            with m.If(self.wvalid):
                with m.Switch(self.waddr[:2]):
                    for j in range(4):
                        with m.Case(j):
                            m.d.comb += wrport.en.eq(1 << j)
            with m.Else():
                m.d.comb += wrport.en.eq(0)
        else:
            assert False # not implemented

        rvalid_prev = Signal()
        m.d.sync += rvalid_prev.eq(self.rvalid)
        m.d.comb += [
            rdport.addr.eq(self.raddr),
            rdport.en.eq(self.rvalid),
            self.rdata.eq(Mux(rvalid_prev, rdport.data, 0)),
        ]
        return m
