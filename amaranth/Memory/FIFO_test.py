# Copyright (C) 2020 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD

# unfortunately, we can't test FIFO36E1, FIFO18E1 OUT_FIFO and IN_FIFO
# as the simulation primitives are encrypted

from amaranth import *
from amaranth.sim import Simulator, Passive

def test_fifo(iclk=8e-9, oclk=3e-9):
    from Memory.FIFO import FIFO
    from AXI.Stream import AXIStream
    reset = Signal(reset=1)
    istream = AXIStream('fifo_i', [('data', 8), ('last', 1)])
    dut = FIFO(i=istream, reset=reset, depth=16, iclk='iclk', oclk='oclk')
    sim = Simulator(dut)
    sim.add_clock(iclk, domain='iclk')
    sim.add_clock(oclk, domain='oclk')

    def write_proc():
        yield Passive()
        for i in range(256):
            yield dut.i.valid.eq(1)
            yield dut.i.data.eq(i)
            yield dut.i.last.eq(1 if i == 15 else 0)
            while True:
                yield
                ready = yield dut.i.ready
                if ready:
                    break
            yield dut.i.valid.eq(0)
            if i == 4:
                for j in range(4):
                    yield

    def read_proc():
        for i in range(50):
            if(i == 3):
                yield dut.o.ready.eq(0)
                for j in range(5):
                    yield
            # check gaps
            yield dut.o.ready.eq(1)
            while True:
                yield
                valid = yield dut.o.valid
                if valid:
                    break
            yield dut.o.ready.eq(0)
            data = yield dut.o.data
            last = yield dut.o.last
            assert data == i
            assert last == (1 if i == 15 else 0)

    sim.add_sync_process(write_proc, domain='iclk')
    sim.add_sync_process(read_proc, domain='oclk')
    with sim.write_vcd("dump.vcd"):
        sim.run()

def test_fifo_fast_write():
    test_fifo(iclk=1e-9, oclk=8e-9)
