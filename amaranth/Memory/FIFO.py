# Copyright (C) 2020 Harmon Instruments, LLC
# SPDX-License-Identifier: BSD-2-Clause-NetBSD
# An AXI stream to AXI stream FIFO with wrappers for FPGA primitives

from amaranth import *
from amaranth.utils import log2_int
from AXI.Stream import AXIStream
from CDC.CDC import CDC_Gray

class FIFO(Elaboratable):
    def __init__(
            self,
            i, # input data
            iclk='sync', # input clock name
            oclk='sync', # output clock name
            primitive='generic', # or FIFO36E1, FIFO18E1, OUT_FIFO
            depth=32,
            max_delay=None,
            clockname_in=None,
            synchronous=False,
            reset=C(0)
    ):
        self.primitive = primitive
        self.reset = reset
        self.i = i
        self.iclk = iclk
        self.o = AXIStream('fifo_o', i.u_raw)
        self.oclk = oclk
        self.empty = Signal()
        self.full = Signal()
        self.almost_full = Signal()
        self.almost_empty = Signal()
        self.icat = self.i.get_upstream_concat()
        self.ocat = self.o.get_upstream_concat()
        self.max_delay = max_delay
        self.synchronous = synchronous
        self.abits = log2_int(depth)
        self.iptr = Signal(self.abits)
        self.optr = Signal(self.abits)
        self.count_i = Signal(self.abits)
        self.count_o = Signal(self.abits)
        self.clockname_in = clockname_in

    def _round_up_bits(self, bits):
        widths = [4, 9, 18, 36, 72]
        for width in widths:
            if bits <= width:
                return width
        assert False # too wide

    def elaborate(self, platform):
        if 'FIFO36E1' in self.primitive:
            return self.elaborate_FIFO36E1(platform)
        if 'FIFO18E1' in self.primitive:
            return self.elaborate_FIFO18E1(platform)
        if 'OUT_FIFO' in self.primitive:
            return self.elaborate_OUT_FIFO(platform)
        # generic is default
        m = Module()

        ram = Memory(width=len(self.icat), depth=2**self.abits)
        wrport = m.submodules.wrport = ram.write_port(domain=self.iclk)
        rdport = m.submodules.rdport = ram.read_port(domain=self.oclk, transparent=False)
        if self.clockname_in is not None:
            rdport.data.attrs["hi.false_path"] = self.clockname_in

        if self.synchronous:
            iptr_oclk = self.iptr
            optr_iclk = self.optr
        else:
            m.submodules.sync_io = CDC_Gray(i=self.iptr, cin=self.iclk, cout=self.oclk,
                                            max_input_delay=self.max_delay)
            m.submodules.sync_oi = CDC_Gray(i=self.optr, cin=self.oclk, cout=self.iclk,
                                            max_input_delay=self.max_delay)
            iptr_oclk = m.submodules.sync_io.o
            optr_iclk = m.submodules.sync_oi.o

        # input clocked signals
        write = Signal()
        m.d.comb += [
            self.count_i.eq(self.iptr - optr_iclk),
            write.eq(self.i.valid & ~self.full),
            self.full.eq(self.iptr == (optr_iclk - 1)[:self.abits]),
            self.i.ready.eq(~self.full),
        ]

        m.d[self.iclk] += [
            self.iptr.eq(self.iptr + write),
            self.almost_full.eq(self.count_i[-2:] == 3), # fill level exceeds 75 %
            wrport.addr.eq(self.iptr),
            wrport.data.eq(self.icat),
            wrport.en.eq(self.i.valid),
        ]

        # output clocked signals
        read = Signal()
        with m.If(read):
            m.d[self.oclk] += [
                self.o.valid.eq(1),
                self.optr.eq(self.optr + 1),
            ]
        with m.Elif(self.o.ready):
            m.d[self.oclk] += self.o.valid.eq(0)

        m.d.comb += [
            self.count_o.eq(iptr_oclk - self.optr),
            self.empty.eq(self.optr == iptr_oclk),
            read.eq((~self.o.valid | self.o.ready) & ~self.empty),
            self.ocat.eq(rdport.data),
            rdport.addr.eq(self.optr),
            rdport.en.eq(read),
        ]
        return m

    def elaborate_OUT_FIFO(self, platform):
        assert len(self.icat) <= 48
        m = Module()
        wdata = Signal(48)
        rdata = Signal(48)
        m.d.comb += [
            self.i.ready.eq(~self.full),
            wdata.eq(self.icat),
            self.ocat.eq(rdata[:len(self.ocat)]),
        ]
        read = ~self.empty & ~self.reset & (self.o.ready | ~self.o.valid)
        m.d[self.oclk] += self.o.valid.eq(read | (self.o.valid & ~self.o.ready))
        ports = {}
        for i in range(10):
            i1 = 4*i
            if i not in [5,6]:
                ports["i_D{}".format(i)]=Cat(wdata[i1:i1+4], Repl(0,4))
                ports["o_Q{}".format(i)]=rdata[i1:i1+4]
            else:
                i2 = 4*(i+5) # index of 10, 11 shared with 5, 6
                ports["i_D{}".format(i)]=Cat(wdata[i1:i1+4], wdata[i2:i2+4])
                ports["o_Q{}".format(i)]=Cat(rdata[i1:i1+4], rdata[i2:i2+4])

        m.submodules.fifo_inst = Instance(
            "OUT_FIFO",
            p_ARRAY_MODE="ARRAY_MODE_4_X_4",
            p_ALMOST_EMPTY_VALUE=2,
            p_ALMOST_FULL_VALUE=2,
            i_RDCLK=ClockSignal(self.oclk),
            i_WRCLK=ClockSignal(self.iclk),
            i_RESET=self.reset,
            i_WREN=self.i.valid & ~self.full & ~self.reset,
            i_RDEN=read,
            o_FULL=self.full,
            o_EMPTY=self.empty,
            o_ALMOSTFULL=self.almost_full,
            o_ALMOSTEMPTY=self.almost_empty,
            **ports,
        )
        return m

    def elaborate_FIFO36E1(self, platform):
        m = Module()
        wdata = Signal(72)
        rdata = Signal(72)
        assert len(self.icat) <= 72
        m.d.comb += [
            wdata.eq(self.icat),
            self.ocat.eq(rdata[:len(self.ocat)]),
            self.i.ready.eq(~self.full),
            self.o.valid.eq(~self.empty),
        ]
        m.submodules.fifo = Instance(
            'FIFO36E1',
            p_ALMOST_EMPTY_OFFSET=0x00F, # 13b
            p_ALMOST_FULL_OFFSET=0x80, # 13b
            p_DATA_WIDTH=self._round_up_bits(len(self.icat)),
            p_DO_REG=1, # must be 1 for dual clock
            p_FIRST_WORD_FALL_THROUGH="TRUE",
            o_ALMOSTEMPTY=self.almost_empty,
            o_ALMOSTFULL=self.almost_full,
            o_DO=Cat(rdata[0:32],rdata[36:68]),
            o_DOP=Cat(rdata[32:36],rdata[68:72]),
            o_EMPTY=self.empty,
            o_FULL=self.full,
            o_RDCOUNT=Signal(13, name='rdcount'),
            o_RDERR=Signal(1, name='rderr'),
            o_WRCOUNT=Signal(13, name='wrcount'),
            o_WRERR=Signal(1, name='wrerr'),
            i_DI=Cat(wdata[0:32],wdata[36:68]),
            i_DIP=Cat(wdata[32:36],wdata[68:72]),
            i_RDCLK=ClockSignal(self.oclk),
            i_RDEN=self.o.ready & ~self.empty,
            i_RST=self.reset,
            i_RSTREG=C(0),
            i_REGCE=C(1),
            i_WRCLK=ClockSignal(self.iclk),
            i_WREN=self.i.valid & ~self.full,
        )
        return m

    def elaborate_FIFO18E1(self, platform):
        m = Module()
        wdata = Signal(36)
        rdata = Signal(36)
        assert len(self.icat) <= 36
        m.d.comb += [
            wdata.eq(self.icat),
            self.ocat.eq(rdata[:len(self.ocat)]),
            self.i.ready.eq(~self.full),
            self.o.valid.eq(~self.empty),
        ]
        m.submodules.fifo = Instance(
            'FIFO18E1',
            p_ALMOST_EMPTY_OFFSET=0x00F, # 13b
            p_ALMOST_FULL_OFFSET=0x80, # 13b
            p_DATA_WIDTH=self._round_up_bits(len(self.icat)),
            p_DO_REG=1, # must be 1 for dual clock
            p_FIRST_WORD_FALL_THROUGH="TRUE",
            o_ALMOSTEMPTY=self.almost_empty,
            o_ALMOSTFULL=self.almost_full,
            o_DO=Cat(rdata[0:16],rdata[18:34]),
            o_DOP=Cat(rdata[16:18],rdata[34:36]),
            o_EMPTY=self.empty,
            o_FULL=self.full,
            o_RDCOUNT=Signal(12, name='rdcount'),
            o_RDERR=Signal(1, name='rderr'),
            o_WRCOUNT=Signal(12, name='wrcount'),
            o_WRERR=Signal(1, name='wrerr'),
            i_DI=Cat(wdata[0:16],wdata[18:34]),
            i_DIP=Cat(wdata[16:18],wdata[34:36]),
            i_RDCLK=ClockSignal(self.oclk),
            i_RDEN=self.o.ready & ~self.empty,
            i_RST=self.reset,
            i_RSTREG=C(0),
            i_REGCE=C(1),
            i_WRCLK=ClockSignal(self.iclk),
            i_WREN=self.i.valid & ~self.full,
        )
        return m
