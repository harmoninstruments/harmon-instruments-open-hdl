# Harmon Instruments Open HDL

Open source HDL from Harmon Instruments, most is in [Amaranth HDL](https://github.com/amaranth-lang/), formerly nmigen.

# License
 - same license as Amaranth HDL for all Harmon Instruments code
 - VexRiscv is MIT license